/*
 * libfannypack.c
 *
 *  Created on: Dec. 19, 2020
 *      Author: otavio
 */

#define CONVERT_VERSION(x)			((((uint32_t)x.major) << 24) | (((uint32_t)x.minor) << 16) | (((uint32_t)x.revision) << 8) | x.user)
#define CHUNK						16384

#include "libfannypack.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <zlib.h>

#include "aes.h"

uint8_t g_AESKey[16];
uint8_t g_IV[16];

static int countEntries(const char *path, uint32_t *totalSize){
	FILE_DIR_STRUCT theDir;
	FILE_DIR_ENTRY_STRUCT entry;
	FILE_STAT_STRUCT stats;
	char subDirPath[512];
	int entryCount = 0;

	if(!FILE_OPEN_DIR(theDir, path))
		return 0;

	while(FILE_READ_DIR(theDir, entry)){
		if((strcmp(FILE_ENTRY_NAME(entry), ".") == 0) || (strcmp(FILE_ENTRY_NAME(entry), "..") == 0))
			continue;	// skip parent and current placeholders

		sprintf(subDirPath, "%s/%s", path, FILE_ENTRY_NAME(entry));
		if(FILE_IS_DIR(entry)){
			entryCount++;
			entryCount += countEntries(subDirPath, totalSize);
		}else{
			FILE_STAT(subDirPath, stats);
			*totalSize += FILE_GET_SIZE(stats);
			entryCount++;
		}
	}

	return entryCount;
}

static uint8_t *storeEntries(const char *leading, const char *path, uint8_t *buffer, fannypack_archiver_entry_t *archEntries, uint32_t *initialOffset, uint32_t *countEntries){
	FILE_DIR_STRUCT theDir;
	FILE_DIR_ENTRY_STRUCT entry;
	char subDirPath[512];
	int currentBlock, totalEntries = 0;
	uint32_t subDirEntries;
	FILE_FILE_STRUCT fp;

	if(path[0] != '\0')
		sprintf(subDirPath, "%s/%s", leading, path);
	else
		strcpy(subDirPath, leading);

	if(!FILE_OPEN_DIR(theDir, subDirPath))
		return 0;

	while(FILE_READ_DIR(theDir, entry)){
		if((strcmp(FILE_ENTRY_NAME(entry), ".") == 0) || (strcmp(FILE_ENTRY_NAME(entry), "..") == 0))
			continue;	// skip parent and current placeholders

		if(path[0] != '\0')
			sprintf(subDirPath, "%s/%s", path, FILE_ENTRY_NAME(entry)); // fix pathing
		else
			strcpy(subDirPath, FILE_ENTRY_NAME(entry));

		if(FILE_IS_DIR(entry)){
			strcpy(archEntries->ARCHIVER_PATH, subDirPath);
			archEntries->ARCHIVER_TYPE = FANNYPACK_ENTRY_DIR;
			archEntries->ARCHIVER_SIZE = 0;
			archEntries->ARCHIVER_OFFSET = 0xFFFFFFFFU;
			archEntries++;
			(*countEntries)++;

			subDirEntries = 0;
			buffer = storeEntries(leading, subDirPath, buffer, archEntries, initialOffset, &subDirEntries);
			archEntries += subDirEntries;
			(*countEntries) += subDirEntries;
		}else{
			strcpy(archEntries->ARCHIVER_PATH, subDirPath);
			sprintf(subDirPath, "%s/%s/%s", leading, path, FILE_ENTRY_NAME(entry));

			if(FILE_OPEN_READ(fp, subDirPath)){
				archEntries->ARCHIVER_TYPE = FANNYPACK_ENTRY_FILE;
				archEntries->ARCHIVER_OFFSET = *initialOffset;
				archEntries->ARCHIVER_SIZE = 0;

				do{
					FILE_DATA_READ(currentBlock, buffer, CONF_MAX_CHUNK, fp);
					buffer += currentBlock;
					*initialOffset += currentBlock;
					archEntries->ARCHIVER_SIZE += currentBlock;
					if(strcmp(archEntries->ARCHIVER_PATH,"Debug/friesNight.elf") == 0)
						__asm("nop");
				}while(currentBlock > 0);

				archEntries++;
				(*countEntries)++;
				FILE_CLOSE(fp);
			}
		}
	}

	return buffer;
}

#ifdef CONF_TARGET_LINUX
void GenerateRandomIV(uint8_t *to, unsigned int length){
//	FILE *rnd = fopen("/dev/random", "r");
//	fread(to, 1, length, rnd);
//	fclose(rnd);
	for(uint8_t idx = 0; idx < length; idx++)
		to[idx] = (uint8_t)rand();
}
#else
__attribute__((weak)) void GenerateRandomIV(uint8_t *to, unsigned int length){
	for(uint8_t idx = 0; idx < length; idx++)
		to[idx] = (uint8_t)rand();
}
#endif

fannypack_version_t FANNYPACK_ParseVersion(char *v){
	fannypack_version_t result;
	int scanResult;

	result.user = 0;
	scanResult = sscanf(v, "%d.%d-%d", &(result.major), &(result.minor), &(result.revision));
	if(scanResult != 3){
		result.major = 0;
		result.minor = 0;
		result.revision = 0;
	}

	return result;
}

uint8_t *FANNYPACK_CreateDataArchiver(const char *path, const char *option, uint32_t *length){
	uint8_t *result;
	uint32_t entryCount, totalFileSize, initialOffset;

	fannypack_archiver_entry_t *archiverEntries;
	fannypack_archiver_header_t header;

	totalFileSize = 0;
	entryCount = countEntries(path, &totalFileSize);
	if(entryCount == 0) // empty
		return NULL;

	header.ARCHIVER_MAGIC			= CONF_ARCHIVER_MAGIC;
	header.ARCHIVER_ENTRIES_COUNT	= entryCount;
	header.ARCHIVER_DATA_BEGIN		= (CONF_ARCHIVER_ENTRY_LENGTH * entryCount);
	header.RESERVED					= 0;

	// This is just to manipulate the entries
	archiverEntries = (fannypack_archiver_entry_t *)MEMORY_MALLOC((CONF_ARCHIVER_ENTRY_LENGTH * entryCount));
	// Allocate space for all folder structure, data and header
	result = (uint8_t *)MEMORY_MALLOC((sizeof(fannypack_archiver_entry_t) * entryCount) + totalFileSize + CONF_ARCHIVER_HEADER_LENGTH);

	initialOffset = 0;
	storeEntries(path, "", (result + (CONF_ARCHIVER_ENTRY_LENGTH * entryCount) + CONF_ARCHIVER_HEADER_LENGTH), archiverEntries, &initialOffset, &entryCount);

	memcpy(result, &header, CONF_ARCHIVER_HEADER_LENGTH);
	memcpy((result + CONF_ARCHIVER_HEADER_LENGTH), archiverEntries, (CONF_ARCHIVER_ENTRY_LENGTH * header.ARCHIVER_ENTRIES_COUNT));
	*length = ((CONF_ARCHIVER_ENTRY_LENGTH * header.ARCHIVER_ENTRIES_COUNT) + totalFileSize + CONF_ARCHIVER_HEADER_LENGTH);

	free(archiverEntries);
	return result;
}

int FANNYPACK_WriteArchiverEntries(const char *destination, uint8_t *data){
	int lastSLashIdx, dstIdx = 0;
	FILE_DIR_STRUCT pathDir;
	fannypack_archiver_header_t *header;
	fannypack_archiver_entry_t *entries;
	uint8_t *fileData;
	char fileName[64];
	FILE_FILE_STRUCT currentFile;
	uint32_t writenSize, dataLeft, writenChunk = 0;

	// strip last folder from dest. to check if path is valid
	lastSLashIdx = -1;
	while(destination[dstIdx] != '\0'){
		if(destination[dstIdx] == '/')
			lastSLashIdx = dstIdx;

		dstIdx++;
	}

	if(lastSLashIdx > 0){
		strcpy(fileName, destination);
		fileName[lastSLashIdx] = '\0';
		if(!FILE_OPEN_DIR(pathDir, fileName))
			return FANNYPACK_ERROR_INVALID_PATH;
		FILE_CLOSE_DIR(pathDir);
	}

	// return original path
	if(!FILE_OPEN_DIR(pathDir, destination)){
		if(FILE_CREATE_DIR(destination) != 0) // All access to owner and group, read/execute for others
			return FANNYPACK_ERROR_EACCES;
	}
	FILE_CLOSE_DIR(pathDir);

	header		= (fannypack_archiver_header_t *)data;
	entries		= (fannypack_archiver_entry_t *)(data + CONF_ARCHIVER_HEADER_LENGTH);
	fileData	= (data + (CONF_ARCHIVER_ENTRY_LENGTH * header->ARCHIVER_ENTRIES_COUNT) + CONF_ARCHIVER_HEADER_LENGTH);

	for(int eIdx = 0; eIdx < header->ARCHIVER_ENTRIES_COUNT; eIdx++){
		sprintf(fileName, "%s/%s", destination, entries[eIdx].ARCHIVER_PATH);
		if(entries[eIdx].ARCHIVER_TYPE == FANNYPACK_ENTRY_DIR){
			// create directory
			if(FILE_CREATE_DIR(fileName) != 0)
				return FANNYPACK_ERROR_EACCES;
		}else{
			if(FILE_OPEN_WRITE(currentFile, fileName)){
				writenSize = 0;
				dataLeft = entries[eIdx].ARCHIVER_SIZE;

				do{
					if(dataLeft > CONF_MAX_CHUNK)
						FILE_DATA_WRITE(writenChunk, (fileData + entries[eIdx].ARCHIVER_OFFSET + writenSize), CONF_MAX_CHUNK, currentFile);
					else
						FILE_DATA_WRITE(writenChunk, (fileData + entries[eIdx].ARCHIVER_OFFSET + writenSize), dataLeft, currentFile);

					if(writenChunk < 0)
						return FANNYPACK_ERROR_FILE_CREATION;

					writenSize	+= writenChunk;
					dataLeft	-= writenChunk;
				}while(dataLeft);

				if(writenSize != entries[eIdx].ARCHIVER_SIZE){
					// delete file
					FILE_DELETE(fileName);
					return FANNYPACK_ERROR_FILE_SIZE_MISMATCH;
				}

				FILE_FLUSH(currentFile);
				FILE_CLOSE(currentFile);
			}
		}
	}

	// All files written, yay!
	return FANNYPACK_ERROR_SUCCESS;
}

int FANNYPACK_ChunksWriteArchiverEntries(const char *source, const char *destination, uint32_t maxChunks){
	int lastSLashIdx, dstIdx = 0;
	FILE_DIR_STRUCT pathDir;
	fannypack_archiver_header_t header;
	fannypack_archiver_entry_t *entries;
	uint8_t *fileData;
	char fileName[64];
	FILE_FILE_STRUCT currentFile;
	FILE_FILE_STRUCT sourceData;
	uint32_t dataLeft, entriesPerTime, entriesPerTimeSize, entriesLocation, dataLocation, entryCount, writenChunk = 0;
	int writenSize, dataRead;

	// strip last folder from dest. to check if path is valid
	lastSLashIdx = -1;
	while(destination[dstIdx] != '\0'){
		if(destination[dstIdx] == '/')
			lastSLashIdx = dstIdx;

		dstIdx++;
	}

	if(maxChunks < CONF_ARCHIVER_HEADER_LENGTH)
		return FANNYPACK_ERROR_CHUNK_TOO_SMALL;	// what's that? a chunk for ants

	if(lastSLashIdx > 0){ //TODO: fix this for root pack
		strcpy(fileName, destination);
		fileName[lastSLashIdx] = '\0';
		if(!FILE_OPEN_DIR(pathDir, fileName))
			return FANNYPACK_ERROR_INVALID_PATH;
		FILE_CLOSE_DIR(pathDir);
	}

	// return original path
	if(!FILE_OPEN_DIR(pathDir, destination)){ // TODO: fix the check for new folder
		if(FILE_CREATE_DIR(destination) != 0) // All access to owner and group, read/execute for others
			return FANNYPACK_ERROR_EACCES;
	}
	FILE_CLOSE_DIR(pathDir);

	if(!FILE_OPEN_READ(sourceData, source))
		return FANNYPACK_ERROR_EACCES;

	FILE_DATA_READ(dataRead, &header, CONF_ARCHIVER_HEADER_LENGTH, sourceData);
	if(dataRead != CONF_ARCHIVER_HEADER_LENGTH){
		FILE_CLOSE(sourceData);
		return FANNYPACK_ERROR_FILE_READING;
	}

	if(header.ARCHIVER_MAGIC != CONF_ARCHIVER_MAGIC){
		FILE_CLOSE(sourceData);
		return FANNYPACK_ERROR_NOT_ARCHIVER;
	}

	if(maxChunks < (CONF_ARCHIVER_ENTRY_LENGTH * header.ARCHIVER_ENTRIES_COUNT)){
		entriesPerTimeSize = maxChunks & CONF_ARCHIVER_ENTRY_MASK;
		entriesPerTime = entriesPerTimeSize >> CONF_ARCHIVER_ENTRY_DIVIDER;
	}else{
		entriesPerTimeSize = (CONF_ARCHIVER_ENTRY_LENGTH * header.ARCHIVER_ENTRIES_COUNT);
	}

	entries	= (fannypack_archiver_entry_t *)MEMORY_MALLOC(entriesPerTimeSize);
	fileData = (uint8_t *)MEMORY_MALLOC(maxChunks);

	entryCount = 0;
	entriesLocation = CONF_ARCHIVER_HEADER_LENGTH;
	dataLocation = (CONF_ARCHIVER_HEADER_LENGTH + (CONF_ARCHIVER_ENTRY_LENGTH * header.ARCHIVER_ENTRIES_COUNT));
//	for(uint32_t entryCount = entriesPerTime; entryCount < header.ARCHIVER_ENTRIES_COUNT; entryCount += entriesPerTime){
	while(entryCount < header.ARCHIVER_ENTRIES_COUNT){
		FILE_REWIND(sourceData, entriesLocation);
		if((header.ARCHIVER_ENTRIES_COUNT - entryCount) < entriesPerTime){
			entriesPerTime = (header.ARCHIVER_ENTRIES_COUNT - entryCount);
			entriesPerTimeSize = CONF_ARCHIVER_ENTRY_LENGTH * entriesPerTime;
		}

		FILE_DATA_READ(dataRead, entries, entriesPerTimeSize, sourceData);
		if(dataRead != entriesPerTimeSize){
			MEMORY_FREE(fileData);
			MEMORY_FREE(entries);
			FILE_CLOSE(sourceData);
			return FANNYPACK_ERROR_FILE_READING;
		}
		entriesLocation += entriesPerTimeSize;
		entryCount += entriesPerTime;

		for(int eIdx = 0; eIdx < entriesPerTime; eIdx++){
			sprintf(fileName, "%s/%s", destination, entries[eIdx].ARCHIVER_PATH);
			if(entries[eIdx].ARCHIVER_TYPE == FANNYPACK_ENTRY_DIR){
				// create directory
				if(FILE_CREATE_DIR(fileName) != 0)
					return FANNYPACK_ERROR_EACCES;
			}else{
				if(FILE_OPEN_WRITE(currentFile, fileName)){
					FILE_REWIND(sourceData, (dataLocation + entries[eIdx].ARCHIVER_OFFSET));

					writenSize = 0;
					dataLeft = entries[eIdx].ARCHIVER_SIZE;

					do{
						if(dataLeft > maxChunks)
							FILE_DATA_READ(dataRead, fileData, maxChunks, sourceData);
						else
							FILE_DATA_READ(dataRead, fileData, dataLeft, sourceData);

						if(dataRead < 0){
							FILE_CLOSE(currentFile);
							FILE_CLOSE(sourceData);
							FILE_DELETE(fileName);
							MEMORY_FREE(fileData);
							MEMORY_FREE(entries);

							return FANNYPACK_ERROR_FILE_READING;
						}

						if(dataRead){
							FILE_DATA_WRITE(writenChunk, fileData, dataRead, currentFile);

							if(writenChunk != dataRead){
								FILE_CLOSE(currentFile);
								FILE_CLOSE(sourceData);
								FILE_DELETE(fileName);
								MEMORY_FREE(fileData);
								MEMORY_FREE(entries);

								return FANNYPACK_ERROR_FILE_WRITING;
							}

							writenSize	+= dataRead;
							dataLeft	-= dataRead;
						}else{
							__asm("nop");
						}
					}while(dataLeft);

					if(writenSize != entries[eIdx].ARCHIVER_SIZE){
						// delete file
						FILE_CLOSE(currentFile);
						FILE_CLOSE(sourceData);
						FILE_DELETE(fileName);
						MEMORY_FREE(fileData);
						MEMORY_FREE(entries);

						return FANNYPACK_ERROR_FILE_SIZE_MISMATCH;
					}

					FILE_FLUSH(currentFile);
					FILE_CLOSE(currentFile);
				}
			}
		}
	}

	// All files written, yay!
	FILE_CLOSE(sourceData);
	MEMORY_FREE(fileData);
	MEMORY_FREE(entries);
	return FANNYPACK_ERROR_SUCCESS;
}

int FANNYPACK_GuessPackageSizeFromHeader(uint8_t *buffer, uint32_t length){
	fannypack_header_strap_t *header;

	if(length < CONF_FANNYPACK_STRAP_HEADER_LENGTH)
		return FANNYPACK_ERROR_CHUNK_TOO_SMALL;

	header = (fannypack_header_strap_t *)buffer;
	return (header->DATA_TRANSMIT_LENGTH + CONF_FANNYPACK_STRAP_HEADER_LENGTH);
}

int FANNYPACK_CheckIfValidPackage(const char *path){
	// TODO: check the CRC
	FILE_FILE_STRUCT headerFile;
	fannypack_header_strap_t header;
	fannypack_archiver_header_t archiver;
	int dataRead;

	if(!FILE_OPEN_READ(headerFile, path))
		return FANNYPACK_ERROR_EACCES;

	FILE_DATA_READ(dataRead, &header, CONF_FANNYPACK_STRAP_HEADER_LENGTH, headerFile);
	if(dataRead != CONF_FANNYPACK_STRAP_HEADER_LENGTH)
		return FANNYPACK_ERROR_FILE_READING;

	if(header.MAGIC != CONF_FANNYPACK_MAGIC)
		return FANNYPACK_ERROR_SUCCESS;
	else
		return FANNYPACK_ERROR_WRONG_DATA;
}

int FANNYPACK_CreatePackageStrapHeader(uint8_t *UID, fannypack_version_t version, fannypack_crypt_t cryptMethod, uint8_t *data, unsigned int dataLength, uint8_t **result){
	fannypack_header_strap_t header;
	uint8_t *dataResult;
	uint8_t *zLibWork;
	uint32_t trailingZeroes;

	struct AES_ctx ctx;

	z_stream zs;
	int zResult;

	if(!data)
		return FANNYPACK_ERROR_WRONG_DATA;

	header.MAGIC = CONF_FANNYPACK_MAGIC;
	header.HEADER_TYPE = FANNYPACK_HEADER_STRAP;
	header.LIB_VERSION = CONF_LIB_VERSION;
	if(UID)
		memcpy(header.PACKAGE_UID, UID, CONF_HEADER_UID_LENGTH);
	else
		bzero(header.PACKAGE_UID, CONF_HEADER_UID_LENGTH);

	header.DATA_FULL_LENGTH = dataLength;
	header.VERSION = CONVERT_VERSION(version);

	if(cryptMethod > CONF_CRYPT_METHODS)
		return FANNYPACK_ERROR_ILLEGAL_CRYPT_METHOD;

	header.CRYPT_METHOD = cryptMethod;

	// Create data result
//	dataResult = (uint8_t *)MEMORY_MALLOC(CONF_FANNYPACK_STRAP_HEADER_LENGTH);
	dataResult = (uint8_t *)MEMORY_MALLOC((header.DATA_FULL_LENGTH + CONF_FANNYPACK_STRAP_HEADER_LENGTH + CONF_FANNYPACK_COMPRESS_LEEWAY));
//	if(!dataResult)
//		return FANNYPACK_ERROR_MEM_ERROR;

	// Compress the data
	zs.zalloc	 = Z_NULL;
	zs.zfree	 = Z_NULL;
	zs.opaque	 = Z_NULL;
	// Setup data pointers
	zs.avail_in  = dataLength;
	zs.next_in	 = data;
	zs.next_out  = (dataResult + CONF_FANNYPACK_STRAP_HEADER_LENGTH);

	zResult = deflateInit(&zs, CONF_COMPRESS_LEVEL);
	if(zResult != Z_OK){
		MEMORY_FREE(dataResult);
		return FANNYPACK_ERROR_COMPRESSING;
	}

//	do{
		zs.avail_out = (header.DATA_FULL_LENGTH + 256);
		zResult = deflate(&zs, Z_FINISH);
//	}while(zs.avail_out == 0);

	zResult = deflateEnd(&zs);
	if(zResult != Z_OK){
		MEMORY_FREE(dataResult);
		return FANNYPACK_ERROR_COMPRESSING;
	}

	// copy the header and return
	header.DATA_COMPRESSED_LENGTH = zs.total_out;

	if(header.DATA_COMPRESSED_LENGTH & 0x0000000FU){
		// we gonna need some zeroes to match AES block size
		header.DATA_TRANSMIT_LENGTH = (header.DATA_COMPRESSED_LENGTH & 0xFFFFFFF0U) + AES_BLOCKLEN;
		trailingZeroes = header.DATA_TRANSMIT_LENGTH - header.DATA_COMPRESSED_LENGTH;
		bzero((dataResult + CONF_FANNYPACK_STRAP_HEADER_LENGTH + header.DATA_COMPRESSED_LENGTH), trailingZeroes);
	}else{
		header.DATA_TRANSMIT_LENGTH = header.DATA_COMPRESSED_LENGTH;
	}

	// Initiate an IV for the encryption
	GenerateRandomIV(g_IV, CONF_HEADER_IV_LENGTH);
	memcpy(header.CRYPT_IV, g_IV, CONF_HEADER_IV_LENGTH);

	AES_init_ctx_iv(&ctx, g_AESKey, g_IV);
	AES_CBC_encrypt_buffer(&ctx, (dataResult + CONF_FANNYPACK_STRAP_HEADER_LENGTH), header.DATA_TRANSMIT_LENGTH);

	memcpy(dataResult, &header, CONF_FANNYPACK_STRAP_HEADER_LENGTH);

	*result = dataResult;
	return (zs.total_out + trailingZeroes + CONF_FANNYPACK_STRAP_HEADER_LENGTH);
}

int FANNYPACK_CreatePackageNormalHeader(uint8_t *UID, fannypack_version_t version, fannypack_crypt_t cryptMethod, uint8_t *description, uint8_t extUser, uint8_t *data, unsigned int dataLength, uint8_t **result){
	return FANNYPACK_ERROR_NOT_IMPLEMENTED;
}

uint8_t *FANNYPACK_ExtractData(uint8_t *input, unsigned int *length){
	uint8_t *result;
	uint8_t *compressedData;
	fannypack_header_strap_t *header = (fannypack_header_strap_t *)input;

	struct AES_ctx ctx;

	z_stream zs;
	int zResult;

	if(header->MAGIC != CONF_FANNYPACK_MAGIC)
		return NULL;

	if(header->HEADER_TYPE == FANNYPACK_HEADER_STRAP){
		compressedData = (input + CONF_FANNYPACK_STRAP_HEADER_LENGTH);
	}else if(header->HEADER_TYPE == FANNYPACK_HEADER_NORMAL){
		compressedData = (input + CONF_FANNYPACK_NORMAL_HEADER_LENGTH);
	}else{
		return NULL;
	}

	AES_init_ctx_iv(&ctx, g_AESKey, header->CRYPT_IV);
	AES_CBC_decrypt_buffer(&ctx, compressedData, header->DATA_TRANSMIT_LENGTH);

	*length = 0;
	result = (uint8_t *)MEMORY_MALLOC(header->DATA_FULL_LENGTH);

	zs.zalloc    = Z_NULL;
	zs.zfree     = Z_NULL;
	zs.opaque    = Z_NULL;
	// Setup data pointers
	zs.total_in  = 0;
	zs.avail_in  = header->DATA_COMPRESSED_LENGTH;
	zs.next_in   = compressedData;
	zs.total_out = 0;
	zs.avail_out = header->DATA_FULL_LENGTH;
	zs.next_out  = result;

	zResult = inflateInit(&zs);
	if(zResult != Z_OK){
		MEMORY_FREE(result);
		return NULL;
	}

	do{
	zResult = inflate(&zs, Z_NO_FLUSH);
	if(zResult < Z_OK){
		MEMORY_FREE(result);
		return NULL;
	}
	}while(zResult != Z_STREAM_END);

	inflateEnd(&zs);

	*length = zs.total_out;
	return result;
}

int FANNYPACK_ChunksExtractData(char *dstPath, char *srcPath, uint32_t maxChunks){
	uint8_t *result;
	uint8_t *compressedData;
	fannypack_header_strap_t header;
	FILE_FILE_STRUCT source;
	FILE_FILE_STRUCT destination;
	uint32_t resultLength, compressedDataBegin, have;
	int dataRead, dataWritten, dataToInflate;

	struct AES_ctx ctx;

	z_stream zs;
	int zResult;

	if(maxChunks < CONF_FANNYPACK_STRAP_HEADER_LENGTH) // we need at least the size of strap header
		return FANNYPACK_ERROR_CHUNK_TOO_SMALL;

	// make chunks 16bytes multiple
	maxChunks &= 0xFFFFFFF0U;

	// head header
	if(!FILE_OPEN_READ(source, srcPath))
		return FANNYPACK_ERROR_EACCES;

	header.MAGIC = 0;
	FILE_DATA_READ(dataRead, &header, CONF_FANNYPACK_STRAP_HEADER_LENGTH, source);
	if(dataRead != CONF_FANNYPACK_STRAP_HEADER_LENGTH)
		return FANNYPACK_ERROR_FILE_READING;

	if(header.MAGIC != CONF_FANNYPACK_MAGIC){
		FILE_CLOSE(source);
		return FANNYPACK_ERROR_WRONG_DATA;
	}

	if(header.HEADER_TYPE == FANNYPACK_HEADER_STRAP){
		compressedDataBegin = CONF_FANNYPACK_STRAP_HEADER_LENGTH;
	}else if(header.HEADER_TYPE == FANNYPACK_HEADER_NORMAL){
		compressedDataBegin = CONF_FANNYPACK_NORMAL_HEADER_LENGTH;
	}else{
		FILE_CLOSE(source);
		return FANNYPACK_ERROR_WRONG_DATA;
	}

	if(!FILE_OPEN_WRITE(destination, dstPath)){
		FILE_CLOSE(source);
		return FANNYPACK_ERROR_EACCES;
	}

	compressedData = (uint8_t *)MEMORY_MALLOC(maxChunks);
	result = (uint8_t *)MEMORY_MALLOC(maxChunks);
	AES_init_ctx_iv(&ctx, g_AESKey, header.CRYPT_IV);

	zs.zalloc    = Z_NULL;
	zs.zfree     = Z_NULL;
	zs.opaque    = Z_NULL;
	// Setup data pointers
	zs.total_in  = 0;
	zs.avail_in  = maxChunks;
	zs.next_in   = compressedData;
	zs.total_out = 0;
	zs.avail_out = maxChunks;
	zs.next_out  = result;

	zResult = inflateInit(&zs);
	if(zResult != Z_OK){
		MEMORY_FREE(compressedData);
		MEMORY_FREE(result);

		FILE_CLOSE(source);
		FILE_CLOSE(destination);
		return FANNYPACK_ERROR_INIT_ZLIB;
	}

	resultLength = 0;
	for(uint32_t offset = 0; offset < header.DATA_TRANSMIT_LENGTH; offset += dataRead){
		if((offset + maxChunks) < header.DATA_TRANSMIT_LENGTH){
			FILE_DATA_READ(dataRead, compressedData, maxChunks, source);

			dataToInflate = maxChunks;
		}else{
			FILE_DATA_READ(dataRead, compressedData, (header.DATA_TRANSMIT_LENGTH - offset), source);

			// data might have trailing zeros, don't include those in inflation
			dataToInflate = (header.DATA_COMPRESSED_LENGTH - offset);
		}

		if(dataRead < 0){
			inflateEnd(&zs);
			MEMORY_FREE(compressedData);
			MEMORY_FREE(result);

			FILE_CLOSE(source);
			FILE_CLOSE(destination);
			return FANNYPACK_ERROR_WRONG_DATA;
		}
		AES_CBC_decrypt_buffer(&ctx, compressedData, dataRead);
		if(dataRead > 0){
			zs.avail_in = dataToInflate;
			zs.next_in = compressedData;

			do {
				zs.avail_out = maxChunks;
				zs.next_out = result;
				zResult = inflate(&zs, Z_NO_FLUSH);
				if(zResult == Z_STREAM_ERROR){
					inflateEnd(&zs);
					MEMORY_FREE(compressedData);
					MEMORY_FREE(result);

					FILE_CLOSE(source);
					FILE_CLOSE(destination);
					return FANNYPACK_ERROR_COMPRESSING;
				}
				switch(zResult){
				case Z_NEED_DICT:
		                zResult = Z_DATA_ERROR;	// fall through
		                /* no break */
				case Z_DATA_ERROR:
				case Z_MEM_ERROR:
					inflateEnd(&zs);
					MEMORY_FREE(compressedData);
					MEMORY_FREE(result);

					FILE_CLOSE(source);
					FILE_CLOSE(destination);
					return FANNYPACK_ERROR_COMPRESSING;
				}

				have = (maxChunks - zs.avail_out);
				resultLength += have;

				FILE_DATA_WRITE(dataWritten, result, have, destination);
				if (dataWritten != have) {
					inflateEnd(&zs);
					MEMORY_FREE(compressedData);
					MEMORY_FREE(result);

					FILE_CLOSE(source);
					FILE_CLOSE(destination);
					return FANNYPACK_ERROR_COMPRESSING;
				}

			} while (zs.avail_out == 0);
		}	// end if dataRead
	}		// end for

	inflateEnd(&zs);
	MEMORY_FREE(compressedData);
	MEMORY_FREE(result);

	FILE_FLUSH(destination);
	FILE_CLOSE(source);
	FILE_CLOSE(destination);

	return resultLength;
}

void FANNYPACK_DefineKey(uint8_t *key){
	memcpy(g_AESKey, key, 16);
}
