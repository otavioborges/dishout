/*
 * libfannypack.h
 *
 *  Created on: Dec 19, 2020
 *      Author: otavio
 */

#ifndef INC_LIBFANNYPACK_H_
#define INC_LIBFANNYPACK_H_

#define FANNYPACK_UDP_SERVER_PORT                       28369U
#define FANNYPACK_UDP_CLIENT_PORT                       28393U
#define FANNYPACK_TCP_SERVER_PORT                       10283U

#define FANNYPACK_TCP_TOTAL_LENGTH                      1024U
#define FANNYPACK_TCP_DATA_LENGTH                       (FANNYPACK_TCP_TOTAL_LENGTH - (6 * 4))	// header of TCP package is 6 words

#include <stdint.h>
#include "fannypack_conf.h"

typedef enum{
	FANNYPACK_HEADER_NORMAL	= 0x00,
	FANNYPACK_HEADER_STRAP	= 0x01
}fannypack_header_type_t;

typedef enum{
	FANNYPACK_CRYPT_NONE	= 0x00,
	FANNYPACK_CRYPT_AES128	= 0x01
}fannypack_crypt_t;

typedef struct{
	uint32_t MAGIC;										// It's magical! Fannypack package identifier
	uint8_t HEADER_TYPE;								// Indicates the format of the header, 0x00 for normal, 0x01 for strap, 0x02-0xFF is reserved
	uint32_t LIB_VERSION;								// The version of the fannypack library that created the package in the format MM.mm.Rev.Arch
	uint8_t PACKAGE_UID[CONF_HEADER_UID_LENGTH];		// Somewhat unique identifier for this package, can be randomly generated
	uint8_t USER_DEFINED1[CONF_HEADER_USER1_LENGTH];
	uint16_t DATA_CHUNK_CRC;							// CRC for the compressed data chunk
	uint32_t DATA_TRANSMIT_LENGTH;						// Encryption might require trailing zeros, the encrypted data might be slightly larger than compressed data
	uint32_t DATA_COMPRESSED_LENGTH;					// The length of the compressed data chunk
	uint32_t DATA_FULL_LENGTH;							// Data length when uncompressed
	uint32_t VERSION;									// For update control, version of the application on the data chunk, on the format MM.mm.Rev.User
	uint8_t CRYPT_METHOD;								// Encryption method used, use section *Encryption Methods* for possible methods
	uint8_t CRYPT_IV[CONF_HEADER_IV_LENGTH];			// Initialization vector for AES cypher
}fannypack_header_strap_t;

typedef struct{
	uint32_t TIMESTAMP;									// Unix Timestamp of the package creation date
	uint8_t DESCRIPTION[CONF_HEADER_DESC_LENGTH];		// String with the description of the package
	uint8_t USER_DEFINED2[CONF_HEADER_USER2_LENGTH];	// Application-dependent bytes, extended from USER_DEFINED1=
	uint8_t RESERVED[CONF_HEADER_RESERVED_LENGTH];		// Reserved for future use, all zeores
}fannypack_header_additional_t;

typedef union{
	fannypack_header_strap_t strap;
	fannypack_header_additional_t add;
}fannypack_header_normal_t;

typedef struct{
	uint8_t major;
	uint8_t minor;
	uint8_t revision;
	uint8_t user;
}fannypack_version_t;

typedef enum{
	FANNYPACK_ENTRY_FILE	= 0x00,
	FANNYPACK_ENTRY_LINK	= 0x01,
	FANNYPACK_ENTRY_DIR		= 0x02
}fannypack_entry_type_t;

typedef struct{
	char ARCHIVER_PATH[119];
	uint8_t ARCHIVER_TYPE;
	uint32_t ARCHIVER_SIZE;
	uint32_t ARCHIVER_OFFSET;
}fannypack_archiver_entry_t;

typedef struct{
	uint32_t ARCHIVER_MAGIC;							// Tells us that the data in the package is archiver structure
	uint32_t ARCHIVER_ENTRIES_COUNT;					// The number of entries in the package
	uint32_t ARCHIVER_DATA_BEGIN;						// The offset to access file data
	uint32_t RESERVED;									// Reserved for future use
}fannypack_archiver_header_t;

typedef enum{
	FANNYPACK_UDP_REQUEST        = 0x01,
	FANNYPACK_UDP_REPLY          = 0x02,
	FANNYPACK_UDP_WRONG_PROTOCOL = 0x03                // We only work with IPv4
}fannypack_udp_type;

typedef enum{
	FANNYPACK_UDP_WE_ARE_CLIENT = 0x01,
	FANNYPACK_UDP_WE_ARE_SERVER = 0x02,
	FANNYPACK_UDP_WE_ARE_IDLE   = 0x03
}fannypack_udp_reply_type;

typedef enum{
	FANNYPACK_TCP_REPLY_SUCCESS = 0x00000001U,
	FANNYPACK_TCP_REPLY_FAIL    = 0xFFFFFFFFU
}fannypack_tcp_reply_t;

struct __attribute__((__packed__)) udp_enquiry_data{
	uint8_t queryType;
	uint32_t howsAsking;
	uint32_t howsReplying;
	uint8_t replyType;
};

struct __attribute__((__packed__)) tcp_package_data{
	uint32_t totalSize;                        // Total fannypack length being sent
	uint32_t transactionID;                    // The package transmission data, use a random and keep it. Can't be 0
	uint32_t remainningData;                   // How much data is left to send (including this package)
	uint32_t packageOffset;                    // Offset of this data in relation to the full fannypack package
	uint32_t packageLength;                    // The length of the data being send
	uint32_t reserved;                         // Always zero
	uint8_t data[FANNYPACK_TCP_DATA_LENGTH];   // The data
};

struct __attribute__((__packed__)) tcp_reply{
	uint32_t magicCookie;                      // Just to ensure the message is what expected uses CONF_FANNYPACK_MAGIC
	uint32_t transactionID;                    // The transaction being replied
	uint32_t replyCode;                        // The response from the client uses fannypack_tcp_reply_t
};

// If out of a Linux environment this should be implemented by the user for a true random
void GenerateRandomIV(uint8_t *to, unsigned int length);

fannypack_version_t FANNYPACK_ParseVersion(char *v);
uint8_t *FANNYPACK_CreateDataArchiver(const char *path, const char *option, uint32_t *length);
int FANNYPACK_WriteArchiverEntries(const char *destination, uint8_t *data);
int FANNYPACK_ChunksWriteArchiverEntries(const char *source, const char *destination, uint32_t maxChunks);

int FANNYPACK_GuessPackageSizeFromHeader(uint8_t *buffer, uint32_t length);
int FANNYPACK_CheckIfValidPackage(const char *path);
int FANNYPACK_CreatePackageStrapHeader(uint8_t *UID, fannypack_version_t version, fannypack_crypt_t cryptMethod, uint8_t *data, unsigned int dataLength, uint8_t **result);
int FANNYPACK_CreatePackageNormalHeader(uint8_t *UID, fannypack_version_t version, fannypack_crypt_t cryptMethod, uint8_t *description, uint8_t extUser, uint8_t *data, unsigned int dataLength, uint8_t **result);
uint8_t *FANNYPACK_ExtractData(uint8_t *data, unsigned int *length);
int FANNYPACK_ChunksExtractData(char *dstPath, char *srcPath, uint32_t maxChunks);
void FANNYPACK_DefineKey(uint8_t *key);

#endif /* INC_LIBFANNYPACK_H_ */
