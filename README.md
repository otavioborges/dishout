# Dish Out - Managing your household

Dish out is the software for STM32F746NG-Discovery based RFID control server. It will allow you to create fields and purses on
a NXP MiFARE RFID card, so you can blackmail your kids into doing chores or gift your partner with some "Get out of Dishes Free" card.

You need Eclipse IDE to program/debug the software

## Hardware Requirements

This software is based on the following hardware

 * STM32F746G-Discovery Board;
 * MFRC522 RFID breakout, connected to SPI2 arduino expansion header.

## Documentation

Some interesting reading materials for this project:

 * [STM32F74xxx Reference Manual](https://www.st.com/resource/en/reference_manual/dm00124865-stm32f75xxx-and-stm32f74xxx-advanced-armbased-32bit-mcus-stmicroelectronics.pdf);
 * [STM32F756xx Datahseet](https://www.st.com/resource/en/datasheet/stm32f756bg.pdf);
 * [STM32F746G-Discovery Schematics](https://www.st.com/resource/en/schematic_pack/mb1191-f746ngh6-c01_schematic.pdf);
 * [MFRC522 Datasheet](https://www.nxp.com/docs/en/data-sheet/MFRC522.pdf);
 * [MiFARE Card Datasheet](https://www.nxp.com/docs/en/data-sheet/MF1S50YYX_V1.pdf);
 * [FreeRTOS (Open Source RTOS)](https://www.freertos.org/);
 * [Cyclone TCP (TCP/IP Stack?)](https://www.oryx-embedded.com/products/CycloneTCP).
