/*
 * gui.c
 *
 *  Created on: Jan. 29, 2021
 *      Author: otavio
 */

#include "gui.h"
#include "priorities.h"
#include "lcd.h"

#include "ff.h"

#include "FreeRTOS.h"
#include "semphr.h"

static SemaphoreHandle_t g_lcdAccess;
static TaskHandle_t g_guiHandler;
static void guiThread(void *argument);

result_t gui_init(void){
	g_lcdAccess = xSemaphoreCreateMutex();

	if(xTaskCreate(guiThread, "GUI", GUI_THREAD_STACK_SIZE, NULL, PRIORITY_TASK_GUI, &g_guiHandler) != pdPASS)
		return RESULT_ERROR_TASK_CREATE;

	return RESULT_OK;
}

void guiThread(void *argument){

}
