/*
 * gpio.c
 *
 *  Created on: Nov. 15, 2020
 *      Author: otavio
 */

#include "gpio.h"

void GPIO_Clear(GPIO_TypeDef *port, uint8_t pin){
	port->MODER &= ~(0x0003 << (pin << 1));
	port->OTYPER &= ~(0x0001 << pin);
	port->OSPEEDR &= ~(0x0003 << (pin << 1));
	port->PUPDR &= ~(0x0003 << (pin << 1));
	if(pin < 8)
		port->AFR[0] &= ~(0x000F << (pin << 2));
	else
		port->AFR[1] &= ~(0x000F << ((pin - 8) << 2));
}

void GPIO_SetOutput(GPIO_TypeDef *port, uint8_t pin, gpio_type_t type, gpio_speed_t speed, gpio_pull_t pull){
	port->MODER |= (0x0001 << (pin << 1));
	port->OTYPER |= (type << pin);
	port->OSPEEDR |= (speed << (pin << 1));
	port->PUPDR |= (pull << (pin << 1));
}

void GPIO_SetInput(GPIO_TypeDef *port, uint8_t pin, gpio_speed_t speed, gpio_pull_t pull){
	port->OSPEEDR |= (speed << (pin << 1));
	port->PUPDR |= (pull << (pin << 1));
}

void GPIO_SetAF(GPIO_TypeDef *port, uint8_t pin, uint8_t alternate, gpio_type_t type, gpio_speed_t speed, gpio_pull_t pull){
	port->MODER |= (0x0002 << (pin << 1));
	port->OTYPER |= (type << pin);
	port->OSPEEDR |= (speed << (pin << 1));
	port->PUPDR |= (pull << (pin << 1));
	if(pin < 8)
		port->AFR[0] |= ((alternate & 0x0F) << (pin << 2));
	else
		port->AFR[1] |= ((alternate & 0x0F) << ((pin - 8) << 2));
}

void GPIO_SetAnalog(GPIO_TypeDef *port, uint8_t pin, gpio_speed_t speed){
	port->MODER |= (0x0003 << (pin << 1));
	port->OSPEEDR |= (speed << (pin << 1));
}

void GPIO_Reset(GPIO_TypeDef *port, uint8_t pin){
	port->BSRR = 0x01 << ((pin & 0xF) + 16);
}

void GPIO_Set(GPIO_TypeDef *port, uint8_t pin){
	port->BSRR = 0x01 << (pin & 0xF);
}

uint8_t GPIO_Read(GPIO_TypeDef *port, uint8_t pin){
	return (uint8_t)((port->IDR >> pin) & 0x01);
}
