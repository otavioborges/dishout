/*
 * lcd.c
 *
 *  Created on: Jan. 28, 2021
 *      Author: otavio
 */

#define HFP   32
#define HSYNC 40
#define HBP   54

#define VFP   2
#define VSYNC 9
#define VBP   12

#define ACTIVE_LAYER_1		0
#define ACTIVE_LAYER_2		1

#define LAYER_COUNT         2

// Memory space for layers
#define LCD_LINE_LENGTH		480
#define LCD_MEMORY_LAYER_1	0xC0000000
#define LCD_MEMORY_LAYER_2	0xC003FC00 // end at 0xC007F800

#define ACTIVE_W (HSYNC + LCD_WIDTH + HBP - 1)
#define ACTIVE_H (VSYNC + LCD_HEIGHT + VBP - 1)

#define TOTAL_WIDTH  (HSYNC + HBP + LCD_WIDTH + HFP - 1)
#define TOTAL_HEIGHT (VSYNC + VBP + LCD_HEIGHT + VFP - 1)

#define LCD_PORTH_AF4_MASK       0x0180U

#define LCD_PORTE_AF14_MASK      0x0010U
#define LCD_PORTI_AF14_MASK      0xC600U
#define LCD_PORTJ_AF14_MASK      0xEFFFU
#define LCD_PORTK_AF14_MASK      0x00F7U

#include "lcd.h"
#include "port.h"
#include "util.h"
#include "fonts.h"
#include "sdram.h"

static uint16_t *frameBuffer[LAYER_COUNT];

color_t lcd_colorFromARGB(uint32_t argb){
	return (color_t)((argb & 0x001FU) | ((argb >> 3) & 0x07E0U) | ((argb >> 5) & 0xF800));
}

result_t lcd_init(void){
	result_t res;
	// config memory for layer 1
	res = sdram_initMultArray((void **)frameBuffer, (sizeof(uint16_t) * LCD_WIDTH * LCD_HEIGHT), LAYER_COUNT);
	ASSERT_RESULT(res);

	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN | RCC_AHB1ENR_GPIOGEN | RCC_AHB1ENR_GPIOHEN |
			RCC_AHB1ENR_GPIOIEN | RCC_AHB1ENR_GPIOJEN | RCC_AHB1ENR_GPIOKEN;

	GPIO_Clear(GPIOE, 4);
	GPIO_Clear(GPIOG, 12);
	GPIO_Clear(GPIOI, 12);
	GPIO_Clear(GPIOK, 3);

	PORT_Clear(GPIOE, LCD_PORTE_AF14_MASK);
	PORT_Clear(GPIOH, LCD_PORTH_AF4_MASK);
	PORT_Clear(GPIOI, LCD_PORTI_AF14_MASK);
	PORT_Clear(GPIOJ, LCD_PORTJ_AF14_MASK);
	PORT_Clear(GPIOK, LCD_PORTK_AF14_MASK);

	// LCD Power
	GPIO_SetAF(GPIOE, 4, 14, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	GPIO_SetAF(GPIOG, 12, 9, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	GPIO_SetOutput(GPIOI, 12, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_LOW, GPIO_PULL_NO_PULL);
	GPIO_SetOutput(GPIOK, 3, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_LOW, GPIO_PULL_NO_PULL);

	PORT_SetAF(GPIOE, LCD_PORTE_AF14_MASK, 14U, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	PORT_SetAF(GPIOH, LCD_PORTH_AF4_MASK, 4U, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	PORT_SetAF(GPIOI, LCD_PORTI_AF14_MASK, 14U, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	PORT_SetAF(GPIOJ, LCD_PORTJ_AF14_MASK, 14U, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	PORT_SetAF(GPIOK, LCD_PORTK_AF14_MASK, 14U, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);

	RCC->APB2ENR |= RCC_APB2ENR_LTDCEN;

	LTDC->SSCR = (((HSYNC -1) << 16U) | (VSYNC - 1));
	LTDC->BPCR = (((HBP -1) << 16U) | (VBP - 1));
	LTDC->AWCR = ((ACTIVE_W << 16U) | ACTIVE_H);
	LTDC->TWCR = ((TOTAL_WIDTH << 16U) | TOTAL_HEIGHT);
	LTDC->BCCR = 0;

	LTDC_Layer1->WHPCR = HBP | ((HBP + LCD_WIDTH - 1) << 16);
	LTDC_Layer1->WVPCR = VBP | ((VBP + LCD_HEIGHT - 1) << 16);
	LTDC_Layer1->PFCR = 2; // RGB565
	LTDC_Layer1->CACR = 0xFF;
	LTDC_Layer1->DCCR = 0xFF00FF00;
	LTDC_Layer1->BFCR = 0x606; // using PAxCA for both blending, page 536 of reference
	LTDC_Layer1->CFBAR = (uint32_t)(frameBuffer[0]);
	LTDC_Layer1->CFBLR = ((LCD_WIDTH * 2) << 16) | ((LCD_WIDTH * 2) + 3);
	LTDC_Layer1->CFBLNR = 272;

	//	LTDC_Layer2->WHPCR = HBP | ((HBP + LCD_WIDTH - 1) << 16);
	//	LTDC_Layer2->WVPCR = VBP | ((VBP + LCD_HEIGHT - 1) << 16);
	//	LTDC_Layer2->PFCR = 2; // RGB565
	//	LTDC_Layer2->CACR = 0xFF;
	//	LTDC_Layer2->DCCR = 0xFF00FF00;
	//	LTDC_Layer2->BFCR = 0x606; // using PAxCA for both blending, page 536 of reference
	//	LTDC_Layer2->CFBAR = (uint32_t)(frameBuffer[1]);
	//	LTDC_Layer2->CFBLR = ((LCD_WIDTH * 2) << 16) | ((LCD_WIDTH * 2) + 3);
	//	LTDC_Layer2->CFBLNR = 272;

	GPIOI->BSRR = GPIO_BSRR_BS_12;

	LTDC_Layer1->CR = LTDC_LxCR_LEN;
	LTDC->SRCR = LTDC_SRCR_IMR;
	LTDC->GCR = LTDC_GCR_LTDCEN;	// enable it!

	return RESULT_OK;
}

void lcd_backlight(uint8_t bclt){
	if(bclt)
		GPIOK->BSRR = GPIO_BSRR_BS_3;
	else
		GPIOK->BSRR = GPIO_BSRR_BR_3;
}

void lcd_rectangle(color_t color, uint16_t x, uint16_t y, uint16_t width, uint16_t height){
	for(uint16_t posX = x; posX < (width + x); posX++){
		for(uint16_t posY = y; posY < (height + y); posY++){
			if(frameBuffer[0][(posY * LCD_LINE_LENGTH) + posX] != color)
				frameBuffer[0][(posY * LCD_LINE_LENGTH) + posX] = color;
		}
	}
}

void lcd_printText(char *text, color_t color, color_t backColor, uint16_t x, uint16_t y){
	// font must be dislocated 0x20
	uint8_t *character;
	uint8_t idx = 0;
	uint16_t posX = x;
	uint16_t posY = y;

	while(text[idx] != '\0'){
		if(idx > 29)
			break;
		if(text[idx] == '\n')
			break;

		character = (uint8_t *)(Font16_Table + ((text[idx] - 0x20) * 32));
		for(uint8_t offset = 0; offset < 32; offset++){
			posX = x + (idx * 16) + ((offset & 0x01) << 3);
			posY = y + (offset >> 1);

			for(uint8_t bit = 0; bit < 8; bit++){
				if((character[offset] >> (7 -bit)) & 0x01){
					if(frameBuffer[0][(posY * LCD_LINE_LENGTH) + posX] != color)
						frameBuffer[0][(posY * LCD_LINE_LENGTH) + posX] = color;
				}

				posX++;
			}
		}
		idx++;
	}
}

void lcd_paint(color_t color){
	for(uint16_t line = 0; line < 272; line++){
		for(uint16_t col = 0; col < 480; col++){
			if(frameBuffer[0][(line * LCD_LINE_LENGTH) + col] != color)
				frameBuffer[0][(line * LCD_LINE_LENGTH) + col] = color;
		}
	}
}
