/*
 * dish_out.c
 *
 *  Created on: Jan. 5, 2021
 *      Author: otavio
 */

#define ASSERT_FINAL(x)             if(x != RESULT_OK) ErrorHandler(x)
#define SD_RAM_BUFFER_LENGTH        (512 * 1024) //512KB for SD data buffer

#define DISH_OUT_SERVER_NONE        0
#define DISH_OUT_SERVER_LOCAL       1
#define DISH_OUT_SERVER_REMOTE      2
#define DISH_OUT_FANNYPACK_PACKAGE  "fannypackData"

#include "stm32f7xx.h"
#include "util.h"
#include "rmii.h"
#include "sdram.h"
#include "sd.h"
#include "httpData.h"
#include "priorities.h"
#include "tcpIpDriver.h"
#include "terminal.h"
#include "cardFormat.h"
#include "cardData.h"
#include "rfid.h"
#include "lcd.h"

#include "FreeRTOS.h"
#include "task.h"

#include "ff.h"

#include "libfannypack.h"

#include "lwip/netif.h"
#include "lwip/dhcp.h"
#include "lwip/tcpip.h"

#include "cJSON.h"

// (1UL << __NVIC_PRIO_BITS) - 1UL priority settings

static uint8_t *g_sdRamDataHelper = NULL;
static uint8_t g_sdBuffer[SD_CARD_DEFAULT_SECTOR_SIZE];

struct netif g_network;
static ip4_addr_t g_ourIP = {0};
static ip4_addr_t g_ourNetmask = {0};
static ip4_addr_t g_ourGateway = {0};

static TaskHandle_t g_mainTask;
//static TaskHandle_t g_cardTask;

static void mainThread(void *argument);
//static void cardThread(void *argument);


#include "gpio.h"
void main(void){
	result_t opResult;

//	SCB_EnableICache();
//	SCB_EnableDCache();
	SCB_DisableICache();

	CoreInit();
	sdram_init();

	lcd_init();
	lcd_backlight(1);
	lcd_paint(COLOR_BLACK);

	terminal_init();
	terminal_print("System initiated, jumping to main thread\n");

	opResult = sdram_initArray((void **)&g_sdRamDataHelper, SD_RAM_BUFFER_LENGTH);
	ASSERT_FINAL(opResult);

	// Avoid priority issues with FreeRTOS
	NVIC_SetPriorityGrouping(3);

	if(xTaskCreate(mainThread, "MAIN", 1024U, NULL, PRIORITY_TASK_MAIN, &g_mainTask) != pdPASS)
		ErrorHandler(RESULT_ERROR_TASK_CREATE);

	vTaskStartScheduler();

	// Should not arrive here, in that case reboot
	NVIC_SystemReset();
}

void mainThread(void *argument){
	FATFS filesystem;
	FRESULT fatResult;
	FILINFO fileNfo;
	uint8_t serverStatus, wwwStatus = DISH_OUT_SERVER_NONE;
	uint32_t dataServerIP;
	result_t opResult;
	cJSON_Hooks hooks = {pvPortMalloc, vPortFree};

	int server;
	uint32_t connectedClients = 0;

	struct http_client_data clients[HTTPDATA_MAX_CONNECTIONS];
	struct http_client_data *nextClient = clients;
	TaskHandle_t clientTasks[HTTPDATA_MAX_CONNECTIONS];
	socklen_t clientAddrLength;

	cJSON_InitHooks(&hooks);

	tcpip_init(NULL, NULL);

	// Prepare and let RFID ready to go
	RFID_Init();
	RFID_PowerUp();

	terminal_print("Waiting for DHCP release...\n");
	netif_add(&g_network, &g_ourIP, &g_ourNetmask, &g_ourGateway, NULL, tcpIpDriver_init, netif_input);
	netif_set_default(&g_network);
	netif_set_up(&g_network);
	dhcp_start(&g_network);

	while(!dhcp_supplied_address(&g_network))
		vTaskDelay(10);

	terminal_print("System connected with IP \'%s\'\n", ip4addr_ntoa(&(g_network.ip_addr)));
	f_mount(&filesystem, SD_CARD_VOLUME_NAME, 1);
	if(filesystem.fs_type != FS_FAT12){
		fatResult = f_mkfs(SD_CARD_VOLUME_NAME, 0, g_sdBuffer, SD_CARD_DEFAULT_SECTOR_SIZE);
		if(fatResult != FR_OK)
			ErrorHandler(fatResult);
	}

	terminal_print("Reading card formats saved on file\n");
	opResult = cardFormat_init();
	ASSERT_FINAL(opResult);

	terminal_print("Filesystem mounted, waiting for webdata server\n");
	opResult = httpdata_init(clients);
	ASSERT_FINAL(opResult);

	// Verify if data on disk is valid
	fatResult = f_stat(HTTPDATA_DEFAULT_INDEX, &fileNfo);
	if(fatResult == FR_OK)
		wwwStatus = DISH_OUT_SERVER_LOCAL;

	if(wwwStatus == DISH_OUT_SERVER_NONE) // no files on local SD, try to reach server forever
		opResult = httpData_isDownloadServerOn(HTTPDATA_INFINITE_RETRIES, &serverStatus, &dataServerIP, g_network.ip_addr.addr);
	else
		opResult = httpData_isDownloadServerOn(4, &serverStatus, &dataServerIP, g_network.ip_addr.addr); // try a couple of times to see if update is in order

	// Check if operation went well
	ASSERT_FINAL(opResult);

	if(!serverStatus && (wwwStatus == DISH_OUT_SERVER_NONE)) // no server, no data. FAIL
		ErrorHandler(RESULT_ERROR_INVALID_STATE);

	// Download the data
	if(serverStatus){
		terminal_print("Webdata server found, downloading new data\n");
		opResult = httpData_downloadServer(dataServerIP, 4, g_sdRamDataHelper, SD_RAM_BUFFER_LENGTH);
		ASSERT_FINAL(opResult);
	}

	// HTTP data is available, open socket to listen to HTTP requests
	terminal_print("Initiating webserver...\n");
	opResult = httpData_initiateServer(&server);
	for(;;){
		if(!nextClient){ // no clients available, let's wait a bit
			vTaskDelay(2500); // wait a bit to search for a new client handler

			for(uint8_t c = 0; c < HTTPDATA_MAX_CONNECTIONS; c++){
				if(!clients[c].isConnected){
					// this client is available
					nextClient = (clients + c);
					break;
				}
			}
		}

		xSemaphoreTake(nextClient->clientMutex, portMAX_DELAY);
		terminal_print("Waiting for clients\n");
		nextClient->clientSock = lwip_accept(server, (struct sockaddr *)&(nextClient->clientAddr), &clientAddrLength);
		if(clientAddrLength != 16){
			// no IPv4, no candy
			terminal_print("Non-IPv4 client connected, dropping...\n");
			nextClient->isConnected = 0;
			httpdata_safeSocketClose(&(nextClient->clientSock));
		}
		if(nextClient->clientSock > 0){
			// valid client launch thread
			terminal_print("Client \'%s\' is connected, waiting for requests\n", ip4addr_ntoa((ip4_addr_t*)&(nextClient->clientAddr.sin_addr)));
			nextClient->isConnected = 1;
			if(xTaskCreate(httpdata_clientThread, "CLIENT", HTTPDATA_CLIENT_THREAD_STACK, (void *)nextClient, nextClient->clientPriority, &(clientTasks[nextClient->clientIdx])) != pdPASS){
				nextClient->isConnected = 0;
				httpdata_safeSocketClose(&(nextClient->clientSock));

			}
		}

		xSemaphoreGive(nextClient->clientMutex);

		// get the next client handler available
		nextClient = NULL;
		for(uint8_t c = 0; c < HTTPDATA_MAX_CONNECTIONS; c++){
			if(!clients[c].isConnected){
				// this client is available
				nextClient = (clients + c);
				break;
			}
		}
	} // Main task loop, never exit this place

	vTaskDelete(NULL);
}
