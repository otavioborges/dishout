/*
 * util.c
 *
 *  Created on: Jan. 5, 2021
 *      Author: otavio
 */

#define UTIL_TIM7_PRESCALER       1
#define UTIL_TIM7_FREQ            2000U
#define UTIL_TIM7_TIMEOUT         0xFFFFFFFFU
#define UTIL_GET_CURRENT_COUNT()  (uint32_t)(TIM2->CNT)

#define UTIL_TIM14_PRESCALER       1
#define UTIL_TIM14_FREQ            2000U
#define UTIL_TIM14_TIMEOUT         0xFFFFU

#define UTIL_RANDOM_POOL_LENGTH   32U
#define UTIL_RANDOM_POOL_MASK     0x1F

#include "util.h"
#include "priorities.h"
#include "stm32f7xx.h"

#include "FreeRTOS.h"
#include "semphr.h"

#include <stdlib.h>
#include <string.h>

static uint64_t g_uptimeCounter = 0;

static volatile uint8_t g_randomPoolCount = 0;
static volatile uint8_t g_randomInput = 0;
static uint8_t g_randomOuput = 0;
static uint32_t g_randomPool[UTIL_RANDOM_POOL_LENGTH];

static SemaphoreHandle_t g_copyingMutex = (SemaphoreHandle_t)0;
static TaskHandle_t g_currentCopyingTask = (TaskHandle_t)0;
static uint32_t g_copyingResult = 0;

/********************************************************
 * Default Error Handlers
 */
void NMI_Handler(void){
	ErrorHandler(RESULT_ERROR_NMI);
}
void HardFault_Handler(void){
	ErrorHandler(RESULT_ERROR_HARDFAULT);
}
void MemManage_Handler(void){
	ErrorHandler(RESULT_ERROR_MEMMANG);
}
void BusFault_Handler(void){
	ErrorHandler(RESULT_ERROR_BUSFAULT);
}
void UsageFault_Handler(void){
	ErrorHandler(RESULT_ERROR_USAGEFAULT);
}
// End ofDefault Error Handlers

//void DMA2_Stream1_IRQHandler(void){
//
//}

void TIM2_IRQHandler(void){
	g_uptimeCounter += 1; // each IRQ happens after 2147483647,5ms or 25 days
	TIM2->SR = 0;
}

void RNG_IRQHandler(void){
	g_randomPool[g_randomInput++] = RNG->DR;
	g_randomInput &= UTIL_RANDOM_POOL_MASK;
	if(++g_randomPoolCount >= UTIL_RANDOM_POOL_LENGTH) // disable this IRQ
		RNG->CR &= ~RNG_CR_RNGEN;
}

void DMA2_Stream1_IRQHandler(void){
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if(DMA2->LISR & DMA_LISR_TCIF1){
		g_copyingResult = 1;
	}else if(DMA2->LISR & (DMA_LISR_TEIF1 | DMA_LISR_DMEIF1)){
		g_copyingResult = 0;
	}

	DMA2->LIFCR = 0xF40;
	if(g_currentCopyingTask){
		vTaskNotifyGiveFromISR(g_currentCopyingTask, &xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);

		g_currentCopyingTask = 0;
	}

	xHigherPriorityTaskWoken = pdFALSE;
	xSemaphoreGiveFromISR(g_copyingMutex, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

result_t CoreInit(void){
	// Shift SYSCLK to HSI
	if(RCC->CFGR & RCC_CFGR_SWS){
		RCC->CR |= RCC_CR_HSION;
		while((RCC->CR & RCC_CR_HSIRDY) == 0)
			__asm("nop");

		// Swith SW (maintain only RTC configuration)
		RCC->CFGR &= RCC_CFGR_RTCPRE;
		while((RCC->CFGR & RCC_CFGR_SWS))
			__asm("nop");
	}

	RCC->CR &= ~(RCC_CR_PLLON | RCC_CR_HSEON);
	RCC->CR |= RCC_CR_HSEBYP;

	// Turn HSE clock input on
	RCC->CR |= RCC_CR_HSEON;
	while((RCC->CR & RCC_CR_HSEON) == 0)
		__asm("nop");

	// Turn PLL on and set it to 216MHz
	RCC->PLLCFGR = ((9 << RCC_PLLCFGR_PLLM_Pos) | RCC_PLLCFGR_PLLSRC | (432 << RCC_PLLCFGR_PLLN_Pos) | (25 << RCC_PLLCFGR_PLLM_Pos));
	RCC->CR |= RCC_CR_PLLON;
	while((RCC->CR & RCC_CR_PLLRDY) == 0)
		__asm("nop");

	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CR1 |= PWR_CR1_ODEN;
	while((PWR->CSR1 & PWR_CSR1_ODRDY) == 0)
		__asm("nop");

	PWR->CR1 |= PWR_CR1_ODSWEN;
	while((PWR->CSR1 & PWR_CSR1_ODSWRDY) == 0)
		__asm("nop");

	// PLL is ready, set APB1~2 accordingly
	RCC->CFGR |= ((0b100 << RCC_CFGR_PPRE2_Pos) | (0b101 << RCC_CFGR_PPRE1_Pos));

	FLASH->ACR &= ~FLASH_ACR_LATENCY;
	FLASH->ACR |= FLASH_ACR_LATENCY_7WS;

	// Shift HCLK to use PLL
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	while((RCC->CFGR & RCC_CFGR_SWS_PLL) != RCC_CFGR_SWS_PLL)
		__asm("nop");

	// System is set up as 216MHz
	SystemCoreClockUpdate();

	// Configure PLLSAI1 to be used in LCD. Final CLK 9MHz
	RCC->CR &= ~RCC_CR_PLLSAION;

	RCC->PLLSAICFGR = ((4 << RCC_PLLSAICFGR_PLLSAIR_Pos) | (2 << RCC_PLLSAICFGR_PLLSAIP_Pos) |
			(144U << RCC_PLLSAICFGR_PLLSAIN_Pos));
	RCC->DCKCFGR1 &= ~RCC_DCKCFGR1_PLLSAIDIVR;
	RCC->DCKCFGR1 |= (1 << RCC_DCKCFGR1_PLLSAIDIVR_Pos);

	RCC->CR |= RCC_CR_PLLSAION;
	while((RCC->CR & RCC_CR_PLLSAIRDY) == 0)
		__asm("nop");

	// Initiate the delay free-running clock
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

	TIM2->CR1 = TIM_CR1_URS;
	TIM2->CR2 = 0;
	TIM2->DIER = TIM_DIER_UIE;
	TIM2->PSC = (uint16_t)(((SystemCoreClock >> UTIL_TIM7_PRESCALER) / UTIL_TIM7_FREQ) - 1);
	TIM2->ARR = UTIL_TIM7_TIMEOUT;

	NVIC_SetPriority(TIM2_IRQn, PRIORITY_IRQ_DELAY);
	NVIC_EnableIRQ(TIM2_IRQn);
	TIM2->CR1 |= TIM_CR1_CEN;
	TIM2->CNT = 0;
	TIM2->EGR = TIM_EGR_UG; // force update of counter
	TIM2->SR = 0;

	// Initiate TIM14 for timeout tasks
	RCC->APB1ENR |= RCC_APB1ENR_TIM14EN;

	// Use 48MHz clock to drive SDMMC
	RCC->DCKCFGR2 &= RCC_DCKCFGR2_SDMMC1SEL;

	TIM14->CR1 = TIM_CR1_OPM; // timeout counter only count once and trigger the timeout
	TIM14->DIER = 0;
	TIM14->PSC = (uint16_t)(((SystemCoreClock >> UTIL_TIM14_PRESCALER) / UTIL_TIM14_FREQ) - 1);

	// Start clock for memory copying shabang
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	NVIC_EnableIRQ(DMA2_Stream1_IRQn);
	NVIC_SetPriority(DMA2_Stream1_IRQn, PRIORITY_IRQ_MEM_COPY);

	// Initiate Random generation
	RCC->AHB2ENR |= RCC_AHB2ENR_RNGEN;
	NVIC_SetPriority(RNG_IRQn, PRIORITY_IRQ_RANDOM);
	NVIC_EnableIRQ(RNG_IRQn);

	RNG->CR = (RNG_CR_IE | RNG_CR_RNGEN);

	return RESULT_OK;
}

uint32_t getRandom(void){
	if(--g_randomPoolCount < (UTIL_RANDOM_POOL_LENGTH >> 1)) // Reactivate random generation if half the pool is used
		RNG->CR |= RNG_CR_RNGEN;

	g_randomOuput &= UTIL_RANDOM_POOL_MASK;
	return g_randomPool[g_randomOuput++];
}

void delay(uint32_t ms){
	ms <<= 1; // tick is in 0.5ms
	ms += UTIL_GET_CURRENT_COUNT();

	while(ms < UTIL_GET_CURRENT_COUNT()) // wait for overflow
		__asm("nop");

	while(ms > UTIL_GET_CURRENT_COUNT())
		__asm("nop");
}

result_t waitUntilFlag(const volatile uint32_t *reg, uint32_t flag, uint16_t maxWait, uint8_t exitWhenZero){
	TIM14->ARR = (maxWait << 1);
	TIM14->EGR = TIM_EGR_UG;
	TIM14->CR1 |= TIM_CR1_CEN;

	if(exitWhenZero){
		while((*reg & flag)){
			if(!(TIM14->CR1 & TIM_CR1_CEN)) // timeout
				return RESULT_ERROR_TIMEOUT;
		}
	}else{
		while((*reg & flag) == 0){
			if(!(TIM14->CR1 & TIM_CR1_CEN)) // timeout
				return RESULT_ERROR_TIMEOUT;
		}
	}

	// Flag has been SET/CLEARED as expected, return is OK
	return RESULT_OK;
}

uint32_t getUptime(void){
	return (UTIL_GET_CURRENT_COUNT() >> 1);
}
uint64_t getLongUptime(void){
	return (((g_uptimeCounter << 32) | UTIL_GET_CURRENT_COUNT()) >> 1);
}

uint32_t copyMem(void *dest, const void *src, uint32_t length){
	if(!g_copyingMutex){
		g_copyingMutex = xSemaphoreCreateBinary();
		xSemaphoreGive(g_copyingMutex);
	}

	if(length == 0)
		return 0;

	__DSB();
	xSemaphoreTake(g_copyingMutex, portMAX_DELAY);
	vPortEnterCritical();
	DMA2_Stream1->CR = ((3 << DMA_SxCR_CHSEL_Pos) | DMA_SxCR_MINC | DMA_SxCR_PINC | DMA_SxCR_DIR_1);// | DMA_SxCR_TCIE);
	DMA2_Stream1->NDTR = length;
	DMA2_Stream1->PAR = (uint32_t)src;
	DMA2_Stream1->M0AR = (uint32_t)dest;

	DMA2->LIFCR = 0xF40;
	DMA2_Stream1->CR |= DMA_SxCR_EN;

	while(DMA2_Stream1->CR & DMA_SxCR_EN)
		__asm("nop");

	vPortExitCritical();
	xSemaphoreGive(g_copyingMutex);
	return length;
}

//uint32_t copyMemNonBlocking(void *dest, const void *src, uint32_t length){
//	if(!g_copyingMutex){
//		g_copyingMutex = xSemaphoreCreateBinary();
//		xSemaphoreGive(g_copyingMutex);
//	}
//
//	if(length == 0)
//		return 0;
//
//	if(DMA2_Stream1->CR & DMA_SxCR_EN) // DMA ongoing
//		return 0;
//
//	__DSB();
//	xSemaphoreTake(g_copyingMutex, portMAX_DELAY);
//	vPortEnterCritical();
//	DMA2_Stream1->CR = ((3 << DMA_SxCR_CHSEL_Pos) | DMA_SxCR_MINC | DMA_SxCR_PINC | DMA_SxCR_DIR_1 | DMA_SxCR_TCIE | DMA_SxCR_DMEIE | DMA_SxCR_TEIE);
//	DMA2_Stream1->NDTR = length;
//	DMA2_Stream1->PAR = (uint32_t)src;
//	DMA2_Stream1->M0AR = (uint32_t)dest;
//
//	DMA2->LIFCR = 0xF40;
//
//	g_currentCopyingTask = xGetCurrentTaskHandle();
//	DMA2_Stream1->CR |= DMA_SxCR_EN;
//
//	vPortExitCritical();
//	ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
//
//	if(g_copyingResult)
//		return length;
//	else
//		return 0;
//}

void ErrorHandler(result_t errorCode){
	struct hardFaultDetails *errorPointer;

	__asm volatile
	(
			" tst lr, #4                                                \n\t"
			" ite eq                                                    \n\t"
			" mrseq r0, msp                                             \n\t"
			" mrsne r0, psp                                             \n\t"
			" mov %0, r0                                                \n\t"
			: "=r"(errorPointer));

	__asm("bkpt");
}

char *fitSize(uint32_t size){
	char power = '\0';
	uint32_t value, remainder;
	static char result[32];

	if(size == 0){
		result[0] = '0';
		result[1] = '\0';
		return result;
	}

	remainder = 0;
	if(size > 1024U){
		remainder = size & 0x3FFU;
		size >>= 10U;
		power = 'k';
	}

	if(size > 1024U){
		remainder = size & 0x3FFU;
		size >>= 10U;
		power = 'M';
	}

	if(size > 1024U){
		remainder = size & 0x3FFU;
		size >>= 10U;
		power = 'G';
	}

	itoa(size, result, 10);
	value = strlen(result);
	if(remainder){
		result[value++] = '.';
		itoa(remainder, (result + value), 10);
		value = strlen(result);
	}

	result[value++] = power;
	result[value] = '\0'; // terminate the string after the power
	return result;
}
