/*
 * httpData.c
 *
 *  Created on: Jan. 8, 2021
 *      Author: otavio
 */

#define HTTPDATA_TEMP_FANNYPACK_DATA    "tempFannypack"
#define HTTPDATA_TEMP_ARCHIVER_DATA     "tempArchiver"
#define HTTPDATA_TEMP_LCD_FILE_DATA     "lcdFannypack"
#define HTTPDATA_DATA_CHUNK_SIZE        128U

#define HTTPDATA_RESP_OK_IDX            3U
#define HTTPDATA_RESP_BAD_REQ_IDX       17U
#define HTTPDATA_RESP_UNAUTH_IDX        18U
#define HTTPDATA_RESP_NOT_FOUND         21U

#define HTTPDATA_CONTENT_HTML_IDX       0U
#define HTTPDATA_CONTENT_JPEG_IDX       1U
#define HTTPDATA_CONTENT_ICON_IDX       2U
#define HTTPDATA_CONTENT_JS_IDX         3U
#define HTTPDATA_CONTENT_CSS_IDX        4U
#define HTTPDATA_CONTENT_PLAIN_IDX      5U
#define HTTPDATA_CONTENT_PNG_IDX        6U

#define HTTPDATA_METHOD_VAR_BEGIN       "<%%f"
#define HTTPDATA_METHOD_VAR_END         "%%>"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "httpData.h"
#include "libfannypack.h"
#include "priorities.h"
#include "sdram.h"
#include "terminal.h"
#include "cardFormat.h"
#include "cJSON.h"
#include "rfid.h"
#include "cardData.h"

#include "ff.h"

static struct udp_enquiry_data g_enqRequest, g_enqReply;
static struct tcp_package_data g_tcpData;
static struct tcp_reply g_tcpReply;
struct http_client_buffer g_clientBuffers[HTTPDATA_MAX_CONNECTIONS];
static char g_defaultHeaderKill[] = "\r\nContent-Type: text/html\r\n\r\n";
static char g_defaultHeaderKeepAlive[] = "\r\nConnection: keep-alive\r\nContent-Length: ";
static char g_defaultCommandHeader[] = "\r\nContent-Type: text/json\r\n\r\n";
static char g_contentStr[] = "\r\nContent-Type: ";
static char g_static400[] = "<html><title>400 BAD REQUEST</title><body><h1>400<br/>BAD REQUEST</h1></body></html>\n";
static char g_static404[] = "<html><title>404 NOT FOUND</title><body><h1>404<br/>NOT FOUND</h1></body></html>\n";
static char g_static401[] = "<html><title>401 BAD REQUEST</title><body><h1>401<br/>BAD REQUEST</h1></body></html>\n";
static char g_doubleBreak[5] = "\r\n\r\n";
static char g_lastFile[128];
static uint8_t *g_largeFileBuffer;
static SemaphoreHandle_t g_largeBufferMutex;

static struct card_format *g_responseFormat;
static struct card_sector_data g_responseCardData[CARD_DATA_SECTOR_COUNT];

static char *g_contentType[7] = {
		"text/html",
		"image/jpeg",
		"image/icon",
		"text/javascript",
		"text/css",
		"text/plain",
		"image/png"
};

struct http_response_code g_responseCodes[] = {
	{100U, "100 Continue"},
	{101U, "101 Switching Protocols"},
	{103U, "103 Early Hints"},
	{200U, "200 OK"},
	{201U, "201 Created"},
	{202U, "202 Accepted"},
	{203U, "203 Non-Authoritative Information"},
	{204U, "204 No Content"},
	{205U, "205 Reset Content"},
	{206U, "206 Partial Content"},
	{300U, "300 Multiple Choices"},
	{301U, "301 Moved Permanently"},
	{302U, "302 Found"},
	{303U, "303 See Other"},
	{304U, "304 Not Modified"},
	{307U, "307 Temporary Redirect"},
	{308U, "308 Permanent Redirect"},
	{400U, "400 Bad Request"},
	{401U, "401 Unauthorized"},
	{402U, "402 Payment Required"},
	{403U, "403 Forbidden"},
	{404U, "404 Not Found"},
	{405U, "405 Method Not Allowed"},
	{406U, "406 Not Acceptable"},
	{407U, "407 Proxy Authentication Required"},
	{408U, "408 Request Timeout"},
	{409U, "409 Conflict"},
	{410U, "410 Gone"},
	{411U, "411 Length Required"},
	{412U, "412 Precondition Failed"},
	{413U, "413 Payload Too Large"},
	{414U, "414 URI Too Long"},
	{415U, "415 Unsupported Media Type"},
	{416U, "416 Range Not Satisfiable"},
	{417U, "417 Expectation Failed"},
	{418U, "418 I\'m a teapot"},
	{422U, "422 Unprocessable Entity"},
	{425U, "425 Too Early"},
	{426U, "426 Upgrade Required"},
	{428U, "428 Precondition Required"},
	{429U, "429 Too Many Requests"},
	{431U, "431 Request Header Fields Too Large"},
	{451U, "451 Unavailable For Legal Reasons"},
	{500U, "500 Internal Server Error"},
	{501U, "501 Not Implemented"},
	{502U, "502 Bad Gateway"},
	{503U, "503 Service Unavailable"},
	{504U, "504 Gateway Timeout"},
	{505U, "505 HTTP Version Not Supported"},
	{506U, "506 Variant Also Negotiates"},
	{507U, "507 Insufficient Storage"},
	{508U, "508 Loop Detected"},
	{510U, "510 Not Extended"},
	{511U, "511 Network Authentication Required"}
};

static char *g_validCommands[12] = {
		"getFormats",
		"addFormat",
		"clearFormats",
		"getFields",
		"writeCard",
		"writeStatus",
		"readCard",
		"readStatus",
		"updateCard",
		"sendLCD",
		"sendLCDFinal",
		NULL
};

static uint8_t g_defaultKey[] = {0x66, 0xB4, 0x7A, 0x27, 0x7B, 0x41, 0x73, 0xFF, 0x71, 0xBD, 0xC6, 0x2D, 0x0B, 0x04, 0xFE, 0x2B};
static result_t g_cardOperation = RESULT_ERROR_NO_OPERATION;

static int httpdata_parseInput(uint8_t *data, uint32_t length, uint8_t *response, uint32_t *dataLeft, uint32_t *offset);
static char *httpdata_getExtension(char *fileName);
static uint32_t httpdata_getLargeFile(char *path, uint8_t *buffer, uint32_t offset, uint32_t maxLength);
static FRESULT delete_node(TCHAR* path, UINT sz_buff, FILINFO* fno);
static int httpdata_executePUT(char *command, char *resultData, char *request);
static void cardOpCallback(card_operation_t op, result_t res, struct card_sector_data *data, struct card_format *format);

result_t httpdata_init(struct http_client_data *clients){
	result_t opResult;

	opResult = sdram_initArray((void **)&g_largeFileBuffer, HTTPDATA_LARGE_FILE_BUFFER_LENGTH);
	ASSERT_RESULT(opResult);
	g_largeFileBuffer[0] = '\0';

	g_largeBufferMutex = xSemaphoreCreateMutex();

	for(uint8_t c = 0; c < HTTPDATA_MAX_CONNECTIONS; c++){
		clients[c].isConnected = 0;
		clients[c].clientIdx = c;
		clients[c].procResult = RESULT_OK;
		clients[c].clientMutex = xSemaphoreCreateMutex();
		clients[c].clientPriority = PRIORITY_TASK_CLIENTS + c;

		opResult = sdram_initArray((void **)&g_clientBuffers[c].rxData, HTTPDATA_BUFFER_LENGTH);
		ASSERT_RESULT(opResult);

		opResult = sdram_initArray((void **)&g_clientBuffers[c].txData, HTTPDATA_BUFFER_LENGTH);
		ASSERT_RESULT(opResult);

		g_clientBuffers[c].rxLength = 0;
		g_clientBuffers[c].txLength = 0;
	}

	return RESULT_OK;
}

void httpdata_safeSocketClose(int *s){
	lwip_close(*s);
}

result_t httpData_isDownloadServerOn(uint8_t retries, uint8_t *serverStatus, uint32_t *serverIP, uint32_t ourIP){
	int udpSocket;
	struct timeval timeout;
	struct sockaddr_in serverAddr, clientAddr;
	int dataTransmit;

	*serverStatus = 0;
	*serverIP = 0;

	udpSocket = lwip_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(udpSocket < 0)
		return RESULT_ERROR_SOCKET_CREATION;

	timeout.tv_sec = 1;
	timeout.tv_usec = 0;
	if(lwip_setsockopt(udpSocket, SOL_SOCKET, SO_RCVTIMEO, (void *)&timeout, sizeof(struct timeval)) < 0){
		httpdata_safeSocketClose(&udpSocket);
		return RESULT_ERROR_SOCKET_CREATION;
	}

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = lwip_htons(FANNYPACK_UDP_SERVER_PORT);
	serverAddr.sin_addr.s_addr = INADDR_BROADCAST; // Set as broadcast

	clientAddr.sin_family = AF_INET;
	clientAddr.sin_port = lwip_htons(FANNYPACK_UDP_CLIENT_PORT);
	clientAddr.sin_addr.s_addr = INADDR_ANY;

	if(lwip_bind(udpSocket, (struct sockaddr *)&clientAddr, sizeof(struct sockaddr_in)) < 0)
		return RESULT_ERROR_SOCKET_CREATION;

	g_enqRequest.queryType = FANNYPACK_UDP_REQUEST;
	g_enqRequest.howsAsking = ourIP; // don't care
	g_enqRequest.howsReplying = 0;
	g_enqRequest.replyType = 0;

	do{
		dataTransmit = lwip_sendto(udpSocket, (void *)&g_enqRequest, sizeof(struct udp_enquiry_data), 0, (struct sockaddr *)&serverAddr, sizeof(struct sockaddr_in));

		dataTransmit = lwip_recvfrom(udpSocket, (void *)&g_enqReply, sizeof(struct udp_enquiry_data), 0, NULL, NULL);
		if(dataTransmit >= sizeof(struct udp_enquiry_data)){
			// This is what we want
			if(g_enqReply.queryType == FANNYPACK_UDP_REPLY){
				if(g_enqReply.replyType == FANNYPACK_UDP_WE_ARE_SERVER){
					// The server is up and GOOOOOD to go
					*serverIP = g_enqReply.howsReplying;
					serverAddr.sin_addr.s_addr = g_enqReply.howsReplying;
					*serverStatus = 1;

					break; // force end of loop, we got it!
				}
			}
		}

		vTaskDelay(100); // wait a bit to avoid errors
		if(retries != HTTPDATA_INFINITE_RETRIES)
			retries--;
	}while(retries); // Run until server is available locally or to download

	httpdata_safeSocketClose(&udpSocket);

	return RESULT_OK;
}

result_t httpData_downloadServer(uint32_t serverIP, uint8_t retries, uint8_t *dataBuffer, uint32_t bufferLength){
	int tcpSocket;
	struct sockaddr_in serverAddr;
	struct timeval timeout;
	int dataResult;
	uint8_t currentRetry;
	char directoryName[128];

	FRESULT fatResult;
	FIL workFile;

	uint8_t successChunk;
	uint32_t totalLength, expectedID, dataOnRam, dataOnDisk;

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = serverIP;
	serverAddr.sin_port = lwip_htons(FANNYPACK_TCP_SERVER_PORT);

	tcpSocket = lwip_socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(tcpSocket < 0)
		return RESULT_ERROR_SOCKET_CREATION;

	// Increase TCP timeout
	timeout.tv_sec = 30;
	timeout.tv_usec = 0;
	if(lwip_setsockopt(tcpSocket, SOL_SOCKET, SO_RCVTIMEO, (void *)&timeout, sizeof(struct timeval)) < 0){
		httpdata_safeSocketClose(&tcpSocket);
		return RESULT_ERROR_SOCKET_CREATION;
	}

	currentRetry = retries;
	do{
		dataResult = lwip_connect(tcpSocket, (struct sockaddr *)&serverAddr, sizeof(struct sockaddr_in));
		if(dataResult == 0)
			break;
	}while(currentRetry--);

	if(dataResult != 0){
		httpdata_safeSocketClose(&tcpSocket);
		return RESULT_ERROR_SOCKET_CONNECTION;
	}

	fatResult = f_open(&workFile, HTTPDATA_TEMP_FANNYPACK_DATA, (FA_CREATE_ALWAYS | FA_WRITE));
	if(fatResult != FR_OK)
		return RESULT_ERROR_SD_WRITE;

	// Receive THE Dataaaaaaaaaaaaaaaaaaaaaaaaaaa, baby
	g_tcpReply.magicCookie = CONF_FANNYPACK_MAGIC;

	successChunk = 0;
	totalLength = 0;
	dataOnRam = 0;
	dataOnDisk = 0;
	do{
		dataResult = lwip_recv(tcpSocket, (void *)&g_tcpData, sizeof(struct tcp_package_data), 0);
		if(dataResult == sizeof(struct tcp_package_data)){
			if(!totalLength){
				totalLength = g_tcpData.totalSize;
				expectedID = g_tcpData.transactionID;
				g_tcpReply.transactionID = g_tcpData.transactionID;

				successChunk = 1;
			}else{
				if(g_tcpData.totalSize == totalLength){
					if(g_tcpData.transactionID == expectedID)
						successChunk = 1;
				}
			}

			if(successChunk){
				if(((g_tcpData.packageOffset + g_tcpData.packageLength) - dataOnDisk) > bufferLength){ // we wont have space on RAM buffer
					fatResult = f_write(&workFile, dataBuffer, (g_tcpData.packageOffset - dataOnDisk), &dataResult);
					if((fatResult != FR_OK) || ((g_tcpData.packageOffset - dataOnDisk) != dataResult)){
						httpdata_safeSocketClose(&tcpSocket);
						f_close(&workFile);

						return RESULT_ERROR_SD_WRITE;
					}

					dataOnDisk += (g_tcpData.packageOffset - dataOnDisk);
					dataOnRam = 0;
				}

				copyMemNonBlocking((dataBuffer + (g_tcpData.packageOffset - dataOnDisk)) , g_tcpData.data, g_tcpData.packageLength);
				dataOnRam = 1;

				g_tcpReply.replyCode = FANNYPACK_TCP_REPLY_SUCCESS;
				successChunk = 0;
			}else{
				g_tcpReply.replyCode = FANNYPACK_TCP_REPLY_FAIL;
			}
		}

		currentRetry = retries;
		do{
			dataResult = lwip_send(tcpSocket, (void *)&g_tcpReply, sizeof(struct tcp_reply), 0);
			if(dataResult > 0)
				break;

		}while(currentRetry--);
		if(dataResult < 0){
			httpdata_safeSocketClose(&tcpSocket);
			f_close(&workFile);

			return RESULT_ERROR_SOCKET_SEND;
		}
	}while((g_tcpData.remainningData - g_tcpData.packageLength));

	// All package arrive, write the remainder on disk and unpack
	terminal_print("Server sent %dB, saving on disk and initiating unpacking...\n", g_tcpData.totalSize);
	httpdata_safeSocketClose(&tcpSocket);
	if(dataOnRam){
		fatResult = f_write(&workFile, dataBuffer, (g_tcpData.totalSize - dataOnDisk), &dataResult);
		if((fatResult != FR_OK) || ((g_tcpData.totalSize - dataOnDisk) != dataResult)){
			f_close(&workFile);

			return RESULT_ERROR_SD_WRITE;
		}
	}

	// Unpack data on destination
	f_sync(&workFile);
	f_close(&workFile);

	// check file data
	FILINFO fNo;
	f_stat(HTTPDATA_TEMP_FANNYPACK_DATA, &fNo);

	FANNYPACK_DefineKey(g_defaultKey);
	dataResult = FANNYPACK_ChunksExtractData(HTTPDATA_TEMP_ARCHIVER_DATA, HTTPDATA_TEMP_FANNYPACK_DATA, HTTPDATA_DATA_CHUNK_SIZE);

	// We don't need original data anymore
	f_unlink(HTTPDATA_TEMP_FANNYPACK_DATA);
	if(dataResult < 0){
		// something wrong unpacking data
		f_unlink(HTTPDATA_TEMP_ARCHIVER_DATA);
		return RESULT_ERROR_FANNYPACK;
	}

	terminal_print("Data extracted saving webdata on disk...\n");
	fatResult = f_stat(HTTPDATA_DEFAULT_FOLDER, &fNo);
	if(fatResult == FR_OK){
		// folder is present, clean it
		strcpy(directoryName, HTTPDATA_DEFAULT_FOLDER);
		fatResult = delete_node(directoryName, 128, &fNo);
		if(fatResult != FR_OK)
			return RESULT_ERROR_EACCESS;
	}

	dataResult = FANNYPACK_ChunksWriteArchiverEntries(HTTPDATA_TEMP_ARCHIVER_DATA, HTTPDATA_DEFAULT_FOLDER, HTTPDATA_DATA_CHUNK_SIZE);

	// We don't need the archiver data
	f_unlink(HTTPDATA_TEMP_ARCHIVER_DATA);
	if(dataResult < 0){
		return RESULT_ERROR_FANNYPACK;
	}else{
		terminal_print("Data successfully written on disk, new webdata is ready\n");
		return RESULT_OK;
	}
}

result_t httpData_initiateServer(int *server){
	struct timeval rcvTimeout;
	struct sockaddr_in serverAddr;

	(*server) = lwip_socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if((*server) < 0)
		return RESULT_ERROR_SOCKET_CREATION;

	// Reset large file buffer
	struct http_large_file_header *fileHeader = (struct http_large_file_header *)g_largeFileBuffer;
	fileHeader->fileName[0] = '\0';
	fileHeader->dataInBuffer = 0;

	rcvTimeout.tv_sec = 30;
	rcvTimeout.tv_usec = 0;
	if(lwip_setsockopt(*server, SOL_SOCKET, SO_RCVTIMEO, (void *)&rcvTimeout, sizeof(struct timeval)) < 0){
		httpdata_safeSocketClose(server);
		return RESULT_ERROR_SOCKET_CREATION;
	}

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = lwip_htons(HTTPDATA_DEFAULT_HTTP_PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;

	if(lwip_bind((*server), (struct sockaddr *)&serverAddr, sizeof(struct sockaddr_in)) < 0){
		httpdata_safeSocketClose(server);

		return RESULT_ERROR_SOCKET_CONNECTION;
	}

	lwip_listen(*server, HTTPDATA_MAX_CONNECTIONS);

	return RESULT_OK;
}

void httpdata_clientThread(void *argument){
	struct http_client_data *client = (struct http_client_data *)argument;
	uint8_t bIdx = client->clientIdx;
	uint32_t dataLeft, dataOffset, sendResult;
	struct timeval timeout;

	xSemaphoreTake(client->clientMutex, portMAX_DELAY);

	if(client->clientSock < 0){
		client->procResult = RESULT_ERROR_INVALID_STATE;
		xSemaphoreGive(client->clientMutex);
		vTaskDelete(NULL);
	}

	timeout.tv_sec = 120;
	timeout.tv_usec = 0;
	if(lwip_setsockopt(client->clientSock, SOL_SOCKET, SO_RCVTIMEO, (void *)&timeout, sizeof(struct timeval)) < 0){
		httpdata_safeSocketClose(&(client->clientSock));
		client->procResult = RESULT_ERROR_SOCKET_CREATION;
		xSemaphoreGive(client->clientMutex);
		vTaskDelete(NULL);
	}

	dataLeft = 0;
	dataOffset = 0;
	g_clientBuffers[bIdx].rxLength = lwip_recv(client->clientSock, g_clientBuffers[bIdx].rxData, HTTPDATA_BUFFER_LENGTH, MSG_MORE);
	if(g_clientBuffers[bIdx].rxLength == HTTPDATA_BUFFER_LENGTH){
		// it might be a biggy
		while(g_clientBuffers[bIdx].rxLength == HTTPDATA_BUFFER_LENGTH){
			g_clientBuffers[bIdx].rxLength = lwip_recv(client->clientSock, g_clientBuffers[bIdx].rxData, HTTPDATA_BUFFER_LENGTH, MSG_MORE);
		}
	}else if(g_clientBuffers[bIdx].rxLength > 0){
		xSemaphoreTake(g_largeBufferMutex, portMAX_DELAY);

		do{
			g_clientBuffers[bIdx].txLength = httpdata_parseInput(g_clientBuffers[bIdx].rxData,
					g_clientBuffers[bIdx].rxLength, g_clientBuffers[bIdx].txData, &dataLeft, &dataOffset);
			if(g_clientBuffers[bIdx].txLength){
				do{
					if(dataLeft)
						sendResult = lwip_send(client->clientSock, g_clientBuffers[bIdx].txData, g_clientBuffers[bIdx].txLength, MSG_MORE);
					else
						sendResult = lwip_send(client->clientSock, g_clientBuffers[bIdx].txData, g_clientBuffers[bIdx].txLength, 0);
				}while(sendResult <= 0);
			}
		}while(dataLeft);

		xSemaphoreGive(g_largeBufferMutex);
	}else{
		terminal_print("Bad request, Recv result: %d\n", g_clientBuffers[bIdx].rxLength);
	}

	httpdata_safeSocketClose(&(client->clientSock));

	client->procResult = RESULT_OK;
	client->isConnected = 0;

	xSemaphoreGive(client->clientMutex);
	vTaskDelete(NULL);
}

int httpdata_parseInput(uint8_t *data, uint32_t length, uint8_t *response, uint32_t *dataLeft, uint32_t *offset){
	char *method = NULL, *path = NULL, *version = NULL, *ext = NULL, *request = NULL;
	char dataFile[128];
	char lengthStr[16];
	uint32_t idx = 0;
	uint32_t dataRead, fileSize;
	uint8_t textMode = 0;

	FIL wwwFile;
	FRESULT fatResult;
	FILINFO fno;

	if(*offset == 0){
		// parse request
		method = (char *)data;
		while(data[idx] != '\n'){
			if(data[idx] == ' '){ // fields are separated by space
				if(!path){
					data[idx] = '\0';
					path = (data + idx + 1); // next field has to be path
				}else{
					data[idx] = '\0';
					version = (data + idx + 1);
				}
			}

			idx++;
		}

		data[idx] = '\0'; // end string on version
		strcpy(response, "HTTP/1.1 ");
		if(strcmp(method, "GET") == 0){
			// GET, check the page being requested
			if(strcmp(path, "/") == 0){
				strcpy(dataFile, HTTPDATA_DEFAULT_INDEX);
			}else{
				strcpy(dataFile, HTTPDATA_DEFAULT_FOLDER);
				strcat(dataFile, "/");

				if(path[0] == '/') // remove leading slash
					strcat(dataFile, (path + 1));
				else
					strcat(dataFile, path);
			}

			terminal_print("GET request for %s\n", dataFile);
		}else if(strcmp(method, "PUT") == 0){
			// client is asking for data, check if param has been set
			if(path[0] == '/') // remove leading slash
				path++;

			// let's check if there's rquest data
			idx++;
			while(idx < length){
				if(memcmp((data + idx), g_doubleBreak, 4) == 0){
					if(length > idx + 4)
						request = (data + idx + 4);

					data[length] = '\0'; // force string terminator
					break;
				}

				idx++;
			}

			terminal_print("PUT request for command %s\n", path);
			return httpdata_executePUT(path, response, request);
		}else{
			terminal_print("Invalid request, replying with 401\n");
			strcat(response, g_responseCodes[HTTPDATA_RESP_UNAUTH_IDX].name);
			strcat(response, g_defaultHeaderKill);
			strcpy(dataFile, HTTPDATA_DEFAULT_401);

			// check if 401 exists
			fatResult = f_stat(dataFile, &fno);
			if(fatResult != FR_OK){ // use static 401 version
				strcat(response, g_static401);
				return strlen(response);
			}
		}

		fatResult = f_stat(dataFile, &fno);
		if(fatResult != FR_OK){
			// respond with 404
			terminal_print("Invalid file, replying with 404\n");

			strcat(response, g_responseCodes[HTTPDATA_RESP_NOT_FOUND].name);
			strcat(response, g_defaultHeaderKill);

			fatResult = f_stat(HTTPDATA_DEFAULT_404, &fno);
			if(fatResult != FR_OK){ // respond with static version of 404
				strcat(response, g_static404);
				return strlen(response);
			}else{
				fileSize = fno.fsize;
				strcpy(dataFile, HTTPDATA_DEFAULT_404);
			}
		}else{
			// reply with OK
			fileSize = fno.fsize;
			*dataLeft = fileSize;
			itoa(fileSize, lengthStr, 10);

			strcat(response, g_responseCodes[HTTPDATA_RESP_OK_IDX].name);
			strcat(response, g_defaultHeaderKeepAlive);
			strcat(response, lengthStr);

			strcat(response, g_contentStr);
			ext = httpdata_getExtension(dataFile);
			if((strcmp(ext, "htm") == 0) || (strcmp(ext, "html") == 0)){
				textMode = 1;
				strcat(response, g_contentType[HTTPDATA_CONTENT_HTML_IDX]);
			}else if((strcmp(ext, "ico") == 0)){
				strcat(response, g_contentType[HTTPDATA_CONTENT_ICON_IDX]);
			}else if((strcmp(ext, "jpg") == 0) || (strcmp(ext, "jpeg") == 0)){
				strcat(response, g_contentType[HTTPDATA_CONTENT_JPEG_IDX]);
			}else if((strcmp(ext, "css") == 0)){
				textMode = 1;
				strcat(response, g_contentType[HTTPDATA_CONTENT_CSS_IDX]);
			}else if((strcmp(ext, "js") == 0)){
				textMode = 1;
				strcat(response, g_contentType[HTTPDATA_CONTENT_JS_IDX]);
			}else if((strcmp(ext, "png") == 0)){
				strcat(response, g_contentType[HTTPDATA_CONTENT_PNG_IDX]);
			}else{
				textMode = 1;
				strcat(response, g_contentType[HTTPDATA_CONTENT_PLAIN_IDX]);
			}

			strcat(response, g_doubleBreak);
		}

		strcpy(g_lastFile, dataFile);
	}else{
		// we are sending additional file data
		strcpy(dataFile, g_lastFile);
		response[0] = '\0'; // empty string
	}

	idx = strlen(response);
	dataRead = httpdata_getLargeFile(dataFile, (response + idx), *offset, (HTTPDATA_BUFFER_LENGTH - idx - 1));

	if((dataRead > *dataLeft) || (dataRead <= 0)){
		*dataLeft = 0;
		*offset = 0;
		return dataRead;
	}

	*dataLeft -= dataRead;
	*offset += dataRead;

	idx += dataRead;

	// Add a break line only if on the end of file
	if(*dataLeft == 0){
		if(textMode)
			response[idx++] = '\n';
	}

	if(*dataLeft)
		terminal_print("Request OK, sending fragment of %dB\n", idx);
	else
		terminal_print("Request OK, sending final packet of %dB\n", idx);

	return idx;
}

char *httpdata_getExtension(char *fileName){
	uint32_t dotIdx = 0, currentIdx = 0;

	while(fileName[currentIdx] != '\0'){
		if(fileName[currentIdx] == '.')
			dotIdx = currentIdx;

		currentIdx++;
	}

	return (fileName + dotIdx + 1);
}

uint32_t httpdata_getLargeFile(char *path, uint8_t *buffer, uint32_t offset, uint32_t maxLength){
	struct http_large_file_header *fileHeader = (struct http_large_file_header *)g_largeFileBuffer;
	uint32_t dataCopied = 0;
	FIL dataFile;
	FRESULT fatResult;
	FILINFO fno;

	if(strcmp(path, fileHeader->fileName) != 0){
		// We need to copy file data to RAM buffer
		strcpy(fileHeader->fileName, path);
		fatResult = f_stat(fileHeader->fileName, &fno);
		if(fatResult != FR_OK){
			terminal_print("File \'%s\' not found\n", fileHeader->fileName);
			fileHeader->fileName[0] = '\0'; // invalid file, release the RAM buffer
			return 0;
		}

		if(fno.fsize > HTTPDATA_LARGE_FILE_BUFFER_LENGTH){
			terminal_print("File \'%s\' to large for buffer, max: %dB, file size: %dB\n",
					fileHeader->fileName, fitSize(HTTPDATA_LARGE_FILE_BUFFER_LENGTH), fitSize(fno.fsize));

			fileHeader->fileName[0] = '\0';
			return 0;
		}

		fatResult = f_open(&dataFile, fileHeader->fileName, FA_READ);
		if(fatResult != FR_OK){
			terminal_print("Error opening file \'%s\', result: %d\n", fileHeader->fileName, fatResult);
			fileHeader->fileName[0] = '\0'; // , release the RAM buffer
			return 0;
		}

		// File OK, copy to buffer
		fatResult = f_read(&dataFile, (g_largeFileBuffer + HTTPDATA_LARGE_FILE_DATA_OFFSET), fno.fsize, (UINT *)&dataCopied);
		if(fatResult != FR_OK){
			terminal_print("Error copying file \'%s\' to RAM, result: %d\n", fileHeader->fileName, fatResult);
			fileHeader->fileName[0] = '\0'; // , release the RAM buffer
			return 0;
		}

		f_close(&dataFile);
		if(dataCopied != fno.fsize){
			terminal_print("Mismatch on file \'%\' copy. Expected: %sB, copied: %sB\n",fileHeader->fileName, fno.fsize, dataCopied);
			fileHeader->fileName[0] = '\0';
			return 0;
		}

		fileHeader->dataInBuffer = fno.fsize;
		terminal_print("File \'%s\' copied to RAM buffer, size: %sB\n", fileHeader->fileName, fitSize(fileHeader->dataInBuffer));
	}

	// Data either already on buffer or just copied, send it
	if((fileHeader->dataInBuffer - offset) > maxLength)
		dataCopied = maxLength;
	else
		dataCopied = (fileHeader->dataInBuffer - offset);

	if(copyMemNonBlocking(buffer, (g_largeFileBuffer + HTTPDATA_LARGE_FILE_DATA_OFFSET + offset), dataCopied) == 0)
		return 0;
	else
		return dataCopied;
}

FRESULT delete_node (
    TCHAR* path,    /* Path name buffer with the sub-directory to delete */
    UINT sz_buff,   /* Size of path name buffer (items) */
    FILINFO* fno    /* Name read buffer */
){
    UINT i, j;
    FRESULT fr;
    DIR dir;

    fr = f_opendir(&dir, path); /* Open the sub-directory to make it empty */
    if (fr != FR_OK) return fr;

    for (i = 0; path[i]; i++) ; /* Get current path length */
    path[i++] = '/';

    for (;;) {
        fr = f_readdir(&dir, fno);  /* Get a directory item */
        if (fr != FR_OK || !fno->fname[0]) break;   /* End of directory? */
        j = 0;
        do {    /* Make a path name */
            if (i + j >= sz_buff) { /* Buffer over flow? */
                fr = 100; break;    /* Fails with 100 when buffer overflow */
            }
            path[i + j] = fno->fname[j];
        } while (fno->fname[j++]);
        if (fno->fattrib & AM_DIR) {    /* Item is a sub-directory */
            fr = delete_node(path, sz_buff, fno);
        } else {                        /* Item is a file */
            fr = f_unlink(path);
        }
        if (fr != FR_OK) break;
    }

    path[--i] = 0;  /* Restore the path name */
    f_closedir(&dir);

    if (fr == FR_OK) fr = f_unlink(path);  /* Delete the empty sub-directory */
    return fr;
}

int httpdata_executePUT(char *command, char *resultData, char *request){
	char **formats = NULL;
	uint32_t formatCount = 0;
	uint8_t badReq = 0;
	cJSON *jsonData;
	const cJSON *obj;
	const cJSON *arrayObj;
	const cJSON *arrayItem;
	char formatName[64U];
	uint32_t fieldCount;
	struct card_format *theFormat = NULL;
	struct card_field *theFields = NULL;
	int cardResult;
	uint8_t isUpdate = 0;

	FIL lcdFile;
	static uint8_t isLcdFileOpen = 0;
	uint32_t writtenData;

	// Card data
	struct card_sector_data *theSectors;

	if(!command)
		return 0;

	if(strcmp(command, g_validCommands[8]) == 0){
		// update card has a similar structure as write card, just set up a flag
		isUpdate = 1;
		strcpy(command, g_validCommands[4]);
	}

	strcpy(resultData, "HTTP/1.1 ");
	if(strcmp(command, g_validCommands[0]) == 0){
		// GET FORMATS
		formats = cardFormat_getFormatNames(&formatCount);

		strcat(resultData, g_responseCodes[HTTPDATA_RESP_OK_IDX].name);
		strcat(resultData, g_defaultCommandHeader);
		if(formats && formatCount > 0){
			sprintf(resultData, "%s[\"%s\"", resultData, formats[0]);
			for(uint32_t idx = 1; idx < formatCount; idx++){
				sprintf(resultData, "%s, \"%s\"", resultData, formats[idx]);
			}
			sprintf(resultData, "%s]", resultData);
		}else{
			// error getting formats or no formats
			sprintf(resultData, "[]", resultData);
		}

		badReq = 0;
	}else if(strcmp(command, g_validCommands[1]) == 0){
		// ADD FORMAT
		if(!request){
			badReq = 1;
			goto sendBad;
		}

		if(request[0] != '{'){ // not JSON
			badReq = 1;
			goto sendBad;
		}

		jsonData = cJSON_Parse(request);

		if(!jsonData){
			badReq = 1;
			goto sendBad;
		}

		obj = cJSON_GetObjectItemCaseSensitive(jsonData, "formatName");
		if(!cJSON_IsString(obj) || obj->valuestring == NULL){
			cJSON_Delete(jsonData);
			badReq = 1;
			goto sendBad;
		}
		strcpy(formatName, obj->valuestring);

		obj = cJSON_GetObjectItemCaseSensitive(jsonData, "fieldCount");
		if(!cJSON_IsNumber(obj) || obj->valueint < 0){
			cJSON_Delete(jsonData);
			badReq = 1;
			goto sendBad;
		}
		fieldCount = obj->valueint;

		strcat(resultData, g_responseCodes[HTTPDATA_RESP_OK_IDX].name);
		strcat(resultData, g_defaultHeaderKill);

		cardResult = cardFormat_addFormat(formatName);
		if(cardResult == RESULT_ERROR_EXISTS){
			cJSON_free(jsonData);
			strcat(resultData, "A format with this name already exists");
			return strlen(resultData);
		}

		obj = cJSON_GetObjectItemCaseSensitive(jsonData, "fields");
		if(!cJSON_IsArray(obj)){
			badReq = 1;
			goto sendBad;
		}

		if(fieldCount){
			theFields = (struct card_field *)pvPortMalloc(CARD_FORMAT_STRUCT_FIELD_LENGTH * fieldCount);
			if(!theFields){
				cJSON_free(jsonData);
				strcat(resultData, "Error trying to save the format fields");
				return strlen(resultData);
			}
		}

		for(uint32_t idx = 0; idx < fieldCount; idx++){
			arrayObj = cJSON_GetArrayItem(obj, idx);
			if(!cJSON_IsObject(arrayObj)){
				cJSON_Delete(jsonData);
				badReq = 1;
				goto sendBad;
			}

			arrayItem = cJSON_GetObjectItemCaseSensitive(arrayObj, "name");
			if(!cJSON_IsString(arrayItem) || arrayItem->valuestring == NULL){
				cJSON_Delete(jsonData);
				badReq = 1;
				goto sendBad;
			}
			strcpy(theFields[idx].sName, arrayItem->valuestring);

			arrayItem = cJSON_GetObjectItemCaseSensitive(arrayObj, "type");
			if(!cJSON_IsNumber(arrayItem) || arrayItem->valueint < 0){
				cJSON_Delete(jsonData);
				badReq = 1;
				goto sendBad;
			}
			theFields[idx].cfDataType = arrayItem->valueint;

			arrayItem = cJSON_GetObjectItemCaseSensitive(arrayObj, "user");
			if(!cJSON_IsNumber(arrayItem) || arrayItem->valueint < 0){
				cJSON_Delete(jsonData);
				badReq = 1;
				goto sendBad;
			}
			theFields[idx].caUserAccess = arrayItem->valueint;

			arrayItem = cJSON_GetObjectItemCaseSensitive(arrayObj, "master");
			if(!cJSON_IsNumber(arrayItem) || arrayItem->valueint < 0){
				cJSON_Delete(jsonData);
				badReq = 1;
				goto sendBad;
			}
			theFields[idx].caAdminAccess = arrayItem->valueint;
		}

		// try adding the fields to the format
		cJSON_free(jsonData);
		cardResult = cardFormat_saveFields(formatName, theFields, fieldCount);
		if(cardResult < 0){
			strcat(resultData, "Error saving the fields");
			return strlen(resultData);
		}
		vPortFree(theFields);

		cardResult = cardFormat_saveModifications();
		if(cardResult < 0){
			strcat(resultData, "Error saving format data to database");
			return strlen(resultData);
		}

		terminal_print("Added new format \'%s\'\n", formatName);

		badReq = 0;
		strcat(resultData, "0");
	}else if(strcmp(command, g_validCommands[2]) == 0){
		// CLEAR FORMATS
		badReq = 0;
		cardFormat_deleteDatabase();

		strcat(resultData, g_responseCodes[HTTPDATA_RESP_OK_IDX].name);
		strcat(resultData, g_defaultHeaderKill);
		strcat(resultData, "0");
	}else if(strcmp(command, g_validCommands[3]) == 0){
		// GET FIELDS
		if(!request){
			badReq = 1;
			goto sendBad;
		}

		if(request[0] == '\0'){ // empty string
			badReq = 1;
			goto sendBad;
		}

		strcat(resultData, g_responseCodes[HTTPDATA_RESP_OK_IDX].name);
		strcat(resultData, g_defaultCommandHeader);

		cardResult = cardFormat_getFormat(request, &theFormat);
		if(cardResult < 0){
			sprintf(resultData, "%sFormat \'%s\' could not be found", resultData, request);
			badReq = 0;
		}else{
			sprintf(resultData, "%s{\"formatName\": \"%s\", \"fieldCount\": %d, \"fields\": [",
					resultData, theFormat->sName, theFormat->uiNumberOfFields);
			for(uint32_t idx = 0; idx < theFormat->uiNumberOfFields; idx++){
				if(idx < (theFormat->uiNumberOfFields - 1)){
					sprintf(resultData, "%s{\"name\": \"%s\", \"type\": %d, \"user\": %d, \"master\": %d},",
						resultData, theFormat->cfFields[idx].sName, theFormat->cfFields[idx].cfDataType,
						theFormat->cfFields[idx].caUserAccess, theFormat->cfFields[idx].caAdminAccess);
				}else{ // last field no trailing comma
					sprintf(resultData, "%s{\"name\": \"%s\", \"type\": %d, \"user\": %d, \"master\": %d}",
							resultData, theFormat->cfFields[idx].sName, theFormat->cfFields[idx].cfDataType,
							theFormat->cfFields[idx].caUserAccess, theFormat->cfFields[idx].caAdminAccess);
				}
			}
			sprintf(resultData, "%s]}", resultData);
			badReq = 0;
		}
	}else if(strcmp(command, g_validCommands[4]) == 0){
		// WRITE CARD

		g_cardOperation = 0; // set operation as idle
		if(!request){
			badReq = 1;
			goto sendBad;
		}

		if(request[0] != '{'){ // not JSON
			badReq = 1;
			goto sendBad;
		}

		jsonData = cJSON_Parse(request);

		if(!jsonData){
			badReq = 1;
			goto sendBad;
		}

		obj = cJSON_GetObjectItemCaseSensitive(jsonData, "formatName");
		if(!cJSON_IsString(obj) || obj->valuestring == NULL){
			cJSON_Delete(jsonData);
			badReq = 1;
			goto sendBad;
		}
		strcpy(formatName, obj->valuestring);

		obj = cJSON_GetObjectItemCaseSensitive(jsonData, "fieldCount");
		if(!cJSON_IsNumber(obj) || obj->valueint < 0){
			cJSON_Delete(jsonData);
			badReq = 1;
			goto sendBad;
		}
		fieldCount = obj->valueint;

		strcat(resultData, g_responseCodes[HTTPDATA_RESP_OK_IDX].name);
		strcat(resultData, g_defaultHeaderKill);

		cardResult = cardFormat_getFormat(formatName, &theFormat);
		if(cardResult < 0 || !theFormat){
			cJSON_free(jsonData);
			return sprintf(resultData, "%sError getting format \'%s\' data. Error: %d", resultData, formatName, cardResult);
		}

		if(!isUpdate){ // new card needs all fields
			if(fieldCount != theFormat->uiNumberOfFields){
				cJSON_free(jsonData);
				return sprintf(resultData, "%sMismatch field count. Expected: %d, given: %d", resultData, theFormat->uiNumberOfFields, fieldCount);
			}
		}

		// Init the card data
		cardResult = cardData_defineKey(cardFormat_getMasterKey(), RFID_AUTH_KEY_B);
		if(cardResult != RESULT_OK){
			cJSON_free(jsonData);
			return sprintf(resultData, "%sError initiating card machine", resultData);
		}

		theSectors = cardData_getSectorsData();
		obj = cJSON_GetObjectItemCaseSensitive(jsonData, "fields");
		if(!cJSON_IsArray(obj)){
			cJSON_free(jsonData);
			cardData_releaseSectorData();
			badReq = 1;
			goto sendBad;
		}

		for(uint32_t idx = 0; idx < fieldCount; idx++){
			arrayObj = cJSON_GetArrayItem(obj, idx);
			if(!cJSON_IsObject(arrayObj)){
				cJSON_Delete(jsonData);
				cardData_releaseSectorData();
				badReq = 1;
				goto sendBad;
			}

			arrayItem = cJSON_GetObjectItemCaseSensitive(arrayObj, "fieldName");
			if(!cJSON_IsString(arrayItem) || arrayItem->valuestring == NULL){
				cJSON_Delete(jsonData);
				cardData_releaseSectorData();
				badReq = 1;
				goto sendBad;
			}

			cardResult = cardFormat_isField(theFormat, arrayItem->valuestring);
			if(cardResult == RESULT_ERROR_OUT_OF_BOUNDS){
				cJSON_Delete(jsonData);
				cardData_releaseSectorData();
				return sprintf(resultData, "%s\'%s\' is not a field of format \'%s\'", resultData, arrayItem->valuestring, formatName);
			}
			if(cardResult < 0){
				cJSON_Delete(jsonData);
				badReq = 1;
				goto sendBad;
			}

			if(!isUpdate){ // no need to set trailer config on update
				if(cardFormat_createTrailerConfig(theFormat->cfFields[cardResult], &(theSectors[cardResult].trailerConfig)) != RESULT_OK){
					cJSON_Delete(jsonData);
					cardData_releaseSectorData();
					return sprintf(resultData, "%sError creating access data for field \'%s\'", resultData, theFormat->cfFields[cardResult].sName);
				}
			}

			arrayItem = cJSON_GetObjectItemCaseSensitive(arrayObj, "value");
			if(!arrayItem){
				cJSON_Delete(jsonData);
				cardData_releaseSectorData();
				badReq = 1;
				goto sendBad;
			}

			theSectors[cardResult].update = 1;
			switch(theFormat->cfFields[cardResult].cfDataType){
			case CARD_FIELD_TYPE_UUID:
				if(!cJSON_IsNumber(arrayItem)){
					cJSON_Delete(jsonData);
					cardData_releaseSectorData();
					badReq = 1;
					goto sendBad;
				}

				if(arrayItem->valueint){
					for(uint8_t uuid = 0; uuid < CARD_FORMAT_UUID_LENGTH; uuid++)
						theSectors[cardResult].sectorData[uuid] = (uint8_t)getRandom();
				}else{
					theSectors[cardResult].update = 0;
				}
				break;
			case CARD_FIELD_TYPE_TEXT:
				if(!cJSON_IsString(arrayItem)){
					cJSON_Delete(jsonData);
					cardData_releaseSectorData();
					badReq = 1;
					goto sendBad;
				}

				strcpy(theSectors[cardResult].sectorData, arrayItem->valuestring);
				break;
			case CARD_FIELD_TYPE_NUMBER:
				if(!cJSON_IsNumber(arrayItem)){
					cJSON_Delete(jsonData);
					cardData_releaseSectorData();
					badReq = 1;
					goto sendBad;
				}

				*((int *)theSectors[cardResult].sectorData) = arrayItem->valueint;
				break;
			case CARD_FIELD_TYPE_PURSE:
				if(!cJSON_IsNumber(arrayItem)){
					cJSON_Delete(jsonData);
					cardData_releaseSectorData();
					badReq = 1;
					goto sendBad;
				}

				*((int *)theSectors[cardResult].sectorData) = arrayItem->valueint;

				arrayItem = cJSON_GetObjectItemCaseSensitive(arrayObj, "op");
				if(!cJSON_IsNumber(arrayItem)){
					cJSON_Delete(jsonData);
					cardData_releaseSectorData();
					badReq = 1;
					goto sendBad;
				}

				theSectors[cardResult].purseOp = arrayItem->valueint;
				break;
			default:
				cJSON_Delete(jsonData);
				cardData_releaseSectorData();
				return sprintf(resultData, "%sBad data type for field \'%s\'", resultData, theFormat->cfFields[cardResult].sName);
			}
		}

		// All field data is ready to be written on disk
		cJSON_Delete(jsonData);
		cardData_releaseSectorData();

		terminal_print("Executing card write operation with format \'%s\'\n", theFormat->sName);
		g_cardOperation = 1; // set operation as executing

		if(isUpdate)
			cardResult = cardData_initiateCardOperation(CARD_OPERATION_WRITE, theFormat, cardOpCallback);
		else
			cardResult = cardData_initiateCardOperation(CARD_OPERATION_BEGIN_FORMAT, theFormat, cardOpCallback);

		if(cardResult == RESULT_OK){
			sprintf(resultData, "%s{\"timeout\": %d}", resultData, CARD_DATA_DEFAULT_CONN_TIMEOUT);
			badReq = 0;
		}else{
			g_cardOperation = RESULT_ERROR_STACK_OVERFLOW;
			return sprintf(resultData, "%sError initiating the card operation. Result: %d", resultData, cardResult);
		}
	}else if(strcmp(command, g_validCommands[5]) == 0){
		// WRITE STATUS
		switch(g_cardOperation){
		case RESULT_OK:
			g_cardOperation = RESULT_ERROR_NO_OPERATION;
			return sprintf(resultData, "HTTP/1.1 %s%s0", g_responseCodes[HTTPDATA_RESP_OK_IDX].name, g_defaultHeaderKill);
		case RESULT_ERROR_NO_OPERATION:
			return sprintf(resultData, "HTTP/1.1 %s%s1", g_responseCodes[HTTPDATA_RESP_OK_IDX].name, g_defaultHeaderKill);
		case RESULT_ERROR_FORMAT_MISMATCH:
			return sprintf(resultData, "HTTP/1.1 %s%sFormat on card is different", g_responseCodes[HTTPDATA_RESP_OK_IDX].name, g_defaultHeaderKill);
		case 1: // operation on going
			return sprintf(resultData, "HTTP/1.1 %s%s2", g_responseCodes[HTTPDATA_RESP_OK_IDX].name, g_defaultHeaderKill);
		default:
			g_cardOperation = RESULT_ERROR_NO_OPERATION;
			return sprintf(resultData, "HTTP/1.1 %s%sError executing card operation, result: %d", g_responseCodes[HTTPDATA_RESP_OK_IDX].name, g_defaultHeaderKill, g_cardOperation);
		}
	}else if(strcmp(command, g_validCommands[6]) == 0){
		// READ CARD
		strcat(resultData, g_responseCodes[HTTPDATA_RESP_OK_IDX].name);
		strcat(resultData, g_defaultHeaderKill);

		// Init the card data
		cardResult = cardData_defineKey(cardFormat_getMasterKey(), RFID_AUTH_KEY_B);
		if(cardResult != RESULT_OK){
			cJSON_free(jsonData);
			return sprintf(resultData, "%sError initiating card machine", resultData);
		}

		terminal_print("Executing card read operation\n");
		cardResult = cardData_initiateCardOperation(CARD_OPERATION_READ, NULL, cardOpCallback);
		if(cardResult == RESULT_OK){
			g_cardOperation = 1; // set operation as executing
			sprintf(resultData, "%s{\"timeout\": %d}", resultData, CARD_DATA_DEFAULT_CONN_TIMEOUT);
			badReq = 0;
		}else{
			return sprintf(resultData, "%sError initiating the card operation. Result: %d", resultData, cardResult);
		}
	}else if(strcmp(command, g_validCommands[7]) == 0){
		// READ STATUS
		sprintf(resultData, "HTTP/1.1 %s%s", g_responseCodes[HTTPDATA_RESP_OK_IDX].name, g_defaultHeaderKill);
		if(g_cardOperation == RESULT_OK){
			// read successful, parse data
			g_cardOperation = RESULT_ERROR_NO_OPERATION;

			if(g_responseFormat){
				sprintf(resultData, "%s{\"formatName\":\"%s\",\"fieldCount\":%d,\"fields\":[", resultData, g_responseFormat->sName, g_responseFormat->uiNumberOfFields);

				for(uint32_t idx = 0; idx < g_responseFormat->uiNumberOfFields; idx++){
					sprintf(resultData, "%s{\"fieldName\":\"%s\",\"type\":%d,\"user\":%d,\"master\":%d,\"value\":", resultData,
							g_responseFormat->cfFields[idx].sName, g_responseFormat->cfFields[idx].cfDataType, g_responseFormat->cfFields[idx].caUserAccess, g_responseFormat->cfFields[idx].caAdminAccess);
					switch(g_responseFormat->cfFields[idx].cfDataType){
					case CARD_FIELD_TYPE_UUID:
						sprintf(resultData, "%s\"", resultData);
						for(uint8_t uIdx = 0; uIdx < CARD_FORMAT_UUID_LENGTH; uIdx++)
							sprintf(resultData, "%s%02X", resultData, g_responseCardData[idx].sectorData[uIdx]);

						sprintf(resultData, "%s\"}", resultData);
						break;
					case CARD_FIELD_TYPE_TEXT:
						sprintf(resultData, "%s\"%s\"}", resultData, g_responseCardData[idx].sectorData);
						break;
					case CARD_FIELD_TYPE_NUMBER:
					case CARD_FIELD_TYPE_PURSE:
						sprintf(resultData, "%s%d}", resultData, *((int *)g_responseCardData[idx].sectorData));
						break;
					}

					if(idx < (g_responseFormat->uiNumberOfFields - 1))
						sprintf(resultData, "%s,", resultData);
				}

				sprintf(resultData, "%s]}", resultData);
				badReq = 0;
			}else{
				return sprintf(resultData, "%sError getting format name from card", resultData);
			}
		}else if(strcmp(command, g_validCommands[9]) == 0){
			// LCD DATA
			if(!isLcdFileOpen){
				// Open and init lcd file
				if(f_open(&lcdFile, HTTPDATA_TEMP_LCD_FILE_DATA, (FA_CREATE_ALWAYS | FA_WRITE)) != FR_OK){
					badReq = 1;
					goto sendBad;
				}
				isLcdFileOpen = 1;
			}

			if(f_write(&lcdFile, request, 1024, (UINT *)&writtenData) != FR_OK){
				badReq = 1;
				goto sendBad;
			}

			return sprintf(resultData, "HTTP/1.1 %s%s", g_responseCodes[HTTPDATA_RESP_OK_IDX].name, g_defaultHeaderKill);
		}else if(strcmp(command, g_validCommands[10]) == 0){
			// Final LCD DATA
			if(f_write(&lcdFile, request, 1024, (UINT *)&writtenData) != FR_OK){
				badReq = 1;
				goto sendBad;
			}

			f_close(&lcdFile);
			isLcdFileOpen = 0;

			return sprintf(resultData, "HTTP/1.1 %s%s", g_responseCodes[HTTPDATA_RESP_OK_IDX].name, g_defaultHeaderKill);
		}else{
			// switch the rest of cases
			switch(g_cardOperation){
			case RESULT_ERROR_NO_OPERATION:
				return sprintf(resultData, "HTTP/1.1 %s%s1", g_responseCodes[HTTPDATA_RESP_OK_IDX].name, g_defaultHeaderKill);
			case 1: // operation on going
				return sprintf(resultData, "HTTP/1.1 %s%s2", g_responseCodes[HTTPDATA_RESP_OK_IDX].name, g_defaultHeaderKill);
			default:
				g_cardOperation = RESULT_ERROR_NO_OPERATION;
				return sprintf(resultData, "HTTP/1.1 %s%sError executing card operation, result: %d", g_responseCodes[HTTPDATA_RESP_OK_IDX].name, g_defaultHeaderKill, g_cardOperation);
			}
		}
	}else{
		badReq = 1;
	}

sendBad:
	if(badReq){
		// not a valid command
		strcpy(resultData, "HTTP/1.1 ");
		strcat(resultData, g_responseCodes[HTTPDATA_RESP_BAD_REQ_IDX].name);
		strcat(resultData, g_defaultHeaderKill);
		strcat(resultData, g_static400);
	}

	return strlen(resultData);
}

void cardOpCallback(card_operation_t op, result_t res, struct card_sector_data *data, struct card_format *format){
	if(res == RESULT_OK)
		terminal_print("Card operation successful\n");
	else if(res < 0)
		terminal_print("Operation on card failed. Error: %d\n", res);

	if((op == CARD_OPERATION_BEGIN_FORMAT) || (op == CARD_OPERATION_WRITE)){
		g_cardOperation = res;
	}else if(op == CARD_OPERATION_READ){
		g_responseFormat = format;
		copyMemNonBlocking(&g_responseCardData, data, (sizeof(struct card_sector_data) * CARD_DATA_SECTOR_COUNT));

		g_cardOperation = res;
	}else{
		g_cardOperation = RESULT_ERROR_NO_OPERATION;
	}
}
