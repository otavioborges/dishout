/*
 * cardData.c
 *
 *  Created on: Jan. 20, 2021
 *      Author: otavio
 */

#define ASSERT_THREAD_OP(x)      if(result != RESULT_OK){                                    \
                                    RFID_Halt();                                             \
                                    RFID_Disconnect();                                       \
                                    xSemaphoreGive(g_mutexSectorData);                       \
									if(g_threadCallback)                                     \
											g_threadCallback(thisOp, result, NULL, NULL);    \
									vTaskDelete(NULL);                                       \
								}

#include "cardData.h"
#include "priorities.h"
#include "terminal.h"

#include "FreeRTOS.h"
#include "semphr.h"

#include <string.h>

static uint8_t g_cardConnected = 0;
static uint16_t g_atqa;
static uint8_t g_cardUID[4];
static rfid_auth_key_t g_keyType = 0xFFU;
static uint8_t g_cardKey[6];
static struct card_sector_data g_sectors[CARD_DATA_SECTOR_COUNT];
static struct card_format g_targetFormat;
static TaskHandle_t g_cardOpHandle;
static SemaphoreHandle_t g_mutexSectorData;
static card_operation_result_t g_threadCallback = NULL;
static card_operation_t g_operation;

static rfid_trailer_config_t g_deaultTrailerConfig = {
		{0, 0, 0, 0, 0, 0},                                         // Key A
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},                       // Key B
		RFID_ACCESS_KEY_B,											// Write Key A
		RFID_ACCESS_BOTH,											// Read Access
		RFID_ACCESS_KEY_B,											// Write Access
		RFID_ACCESS_NEVER,											// Read Key B
		RFID_ACCESS_KEY_B,											// Write Key B
		{RFID_ACCESS_BOTH, RFID_ACCESS_BOTH, RFID_ACCESS_BOTH},		// Read Data
		{RFID_ACCESS_KEY_B, RFID_ACCESS_KEY_B, RFID_ACCESS_KEY_B},	// Write Data
		{RFID_ACCESS_BOTH, RFID_ACCESS_BOTH, RFID_ACCESS_BOTH},		// Increment
		{RFID_ACCESS_BOTH, RFID_ACCESS_BOTH, RFID_ACCESS_BOTH}		// Decrement
};

static rfid_trailer_config_t g_deaultFormatConfig = {
		{0, 0, 0, 0, 0, 0},                                         // Key A
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},                       // Key B
		RFID_ACCESS_KEY_B,											// Write Key A
		RFID_ACCESS_BOTH,											// Read Access
		RFID_ACCESS_KEY_B,											// Write Access
		RFID_ACCESS_NEVER,											// Read Key B
		RFID_ACCESS_KEY_B,											// Write Key B
		{RFID_ACCESS_BOTH, RFID_ACCESS_BOTH, RFID_ACCESS_BOTH},		// Read Data
		{RFID_ACCESS_KEY_B, RFID_ACCESS_KEY_B, RFID_ACCESS_KEY_B},	// Write Data
		{RFID_ACCESS_NEVER, RFID_ACCESS_NEVER, RFID_ACCESS_NEVER},	// Increment
		{RFID_ACCESS_NEVER, RFID_ACCESS_NEVER, RFID_ACCESS_NEVER}	// Decrement
};

static void cardOperationThread(void *argument);

int cardData_Connect(uint32_t timeout){
	rfid_result_t result = 0xFFU; // Disallowed result to check if check has to be maintained
	if(timeout == CARD_DATA_TIMEOUT_INFINITE){
		for(;;){
			if(RFID_IsCardPresent(&g_atqa) == RFID_Result_Found){
					result = RFID_GetUID(g_atqa, g_cardUID);
					break;
			}

			vTaskDelay(100);
		}
	}else{
		timeout *= 10; // delay using 0.1ms
		while(timeout--){
			if(RFID_IsCardPresent(&g_atqa) == RFID_Result_Found){
					result = RFID_GetUID(g_atqa, g_cardUID);
					break;
			}

			vTaskDelay(100);
		}
	}

	if(result == 0xFFU) // card was not found in able time
		return RESULT_ERROR_TIMEOUT;
	// TODO: use real error results on rfid_getUID
	if(result == RFID_Result_Success){
		g_cardConnected = 1;
		return RESULT_OK;
	}else{
		g_cardConnected = 0;
		return RESULT_ERROR_TIMEOUT;
	}
}

int cardData_defineKey(uint8_t *key, rfid_auth_key_t keyType){
	if((keyType != RFID_AUTH_KEY_A) && (keyType != RFID_AUTH_KEY_B))
		return RESULT_ERROR_BAD_PARAMS;

	if(copyMemNonBlocking(g_cardKey, key, 6U) != 6)
		return RESULT_ERROR_INVALID_STATE;

	g_keyType = keyType;
	if(g_keyType == RFID_AUTH_KEY_B){
		if(copyMemNonBlocking(g_deaultFormatConfig.keyB, key, 6) != 6)
			return RESULT_ERROR_INVALID_STATE;
	}

	return RESULT_OK;
}

int cardData_TryCardClean(void){
	rfid_result_t result;

	if(!g_cardConnected)
		return RESULT_ERROR_INVALID_STATE;

	for(uint8_t sec = 0; sec < (CARD_DATA_SECTOR_COUNT + 1); sec++){
		result = RFID_Authenticate(g_cardUID, g_cardKey, ((sec << 2) + 3), g_keyType);
		if(result == RFID_Result_Success){
			result = RFID_SetTrailerBlock(((sec << 2) + 3), 0x00, g_deaultTrailerConfig);
			if(result != RFID_Result_Success)
				break;
		}else{
			break;
		}
	}

	switch(result){
	case RFID_Result_Success:
		return RESULT_OK;
	case RFID_Result_Timeout:
		return RESULT_ERROR_TIMEOUT;
	case RFID_Result_Unknown:
		return RESULT_ERROR_INVALID_STATE;
	case RFID_Result_NACK:
		return RESULT_ERROR_EACCESS;
	default:
		return RESULT_ERROR_BAD_PARAMS;
	}
}

int cardData_DefineFormat(struct card_format format){
	rfid_result_t result;
	uint32_t formatNameLength;

	if(!g_cardConnected)
		return RESULT_ERROR_INVALID_STATE;

	result = RFID_Authenticate(g_cardUID, g_cardKey, CARD_DATA_FORMAT_TRAILER, g_keyType);
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;
	result = RFID_SetTrailerBlock(CARD_DATA_FORMAT_TRAILER, 0x01, g_deaultFormatConfig);
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	formatNameLength = strlen(format.sName) + 1; // string terminator goes along
	if(formatNameLength > CARD_DATA_BLOCK_SIZE){
		result = RFID_Write(format.sName, CARD_DATA_BLOCK_SIZE, 1);
		if(result != RFID_Result_Success)
			return RESULT_ERROR_EACCESS;

		result = RFID_Write((format.sName + CARD_DATA_BLOCK_SIZE), ((formatNameLength - CARD_DATA_BLOCK_SIZE) & 0x0FU), 2);
		if(result != RFID_Result_Success)
			return RESULT_ERROR_EACCESS;
	}else{
		result = RFID_Write(format.sName, formatNameLength, 1);
		if(result != RFID_Result_Success)
			return RESULT_ERROR_EACCESS;
	}

	return RESULT_OK; // all is fine
}

int cardData_WriteSector(uint8_t *data, uint8_t sec){
	int result;
	if(!g_cardConnected)
		return RESULT_ERROR_INVALID_STATE;

	result = RFID_Authenticate(g_cardUID, g_cardKey, (sec << 2), g_keyType);
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	result = RFID_Write(data, CARD_DATA_BLOCK_SIZE, (sec << 2));
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	result = RFID_Authenticate(g_cardUID, g_cardKey, ((sec << 2) + 1), g_keyType);
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	result = RFID_Write((data + CARD_DATA_BLOCK_SIZE), CARD_DATA_BLOCK_SIZE, ((sec << 2) + 1));
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	result = RFID_Authenticate(g_cardUID, g_cardKey, ((sec << 2) + 2), g_keyType);
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	result = RFID_Write((data + (CARD_DATA_BLOCK_SIZE << 1)), CARD_DATA_BLOCK_SIZE, ((sec << 2) + 2));
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	return RESULT_OK;
}

int cardData_ReadPurseValue(int32_t *data, uint8_t sec, uint8_t *ID){
	int result;
	result = RFID_Authenticate(g_cardUID, g_cardKey, (sec << 2), g_keyType);
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	result = RFID_ValueRead((sec << 2), data, ID);
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	return RESULT_OK;
}

int cardData_ReadSector(uint8_t *data, uint8_t sec, uint8_t skipFirst){
	int result;
	uint32_t offset = 0;

	if(!skipFirst){
		result = RFID_Authenticate(g_cardUID, g_cardKey, (sec << 2), g_keyType);
		if(result != RFID_Result_Success)
			return RESULT_ERROR_EACCESS;

		result = RFID_Read(g_cardUID, (sec << 2), data);
		if(result != RFID_Result_Success)
			return RESULT_ERROR_EACCESS;

		offset += CARD_DATA_BLOCK_SIZE;
	}

	result = RFID_Authenticate(g_cardUID, g_cardKey, ((sec << 2) + 1), g_keyType);
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	result = RFID_Read(g_cardUID, ((sec << 2) + 1), (data + offset));
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	offset += CARD_DATA_BLOCK_SIZE;
	result = RFID_Authenticate(g_cardUID, g_cardKey, ((sec << 2) + 2), g_keyType);
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	result = RFID_Read(g_cardUID, ((sec << 2) + 2), (data + offset));
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	return RESULT_OK;
}

int cardData_PurseOperation(uint32_t delta, uint8_t sec, card_purse_op_t op){
	int result;

	result = RFID_Authenticate(g_cardUID, g_cardKey, (sec << 2), g_keyType);
	if(result != RFID_Result_Success)
		return RESULT_ERROR_EACCESS;

	switch(op){
	case CARD_FIELD_OP_ALTER: // alter allows signed value
		result = RFID_ValueMode((sec << 2), (int32_t)delta, sec);
		break;
	case CARD_FIELD_OP_INCREMENT:
		result = RFID_Increment((sec << 2), delta);
		if(result != RFID_Result_Success)
			return RESULT_ERROR_EACCESS;

		result = RFID_Transfer((sec << 2));
		break;
	case CARD_FIELD_OP_DECREMENT:
		result = RFID_Decrement((sec << 2), delta);
		if(result != RFID_Result_Success)
			return RESULT_ERROR_EACCESS;

		result = RFID_Transfer((sec << 2));
		break;
	case CARD_FIELD_OP_NONE:
		return RESULT_OK; // nothing to be done
	default:
		return RESULT_ERROR_BAD_PARAMS;
	}

	switch(result){
	case RFID_Result_Success:
		return RESULT_OK;
	case RFID_Result_NACK:
		return RESULT_ERROR_EACCESS;
	default:
		return RESULT_ERROR_INVALID_STATE;
	}
}

struct card_sector_data *cardData_getSectorsData(void){
	if(!g_mutexSectorData){
		g_mutexSectorData = xSemaphoreCreateMutex();
		if(!g_mutexSectorData)
			return NULL;
	}

	xSemaphoreTake(g_mutexSectorData, portMAX_DELAY);

	// clear update fields
	for(uint8_t idx = 0; idx < CARD_DATA_SECTOR_COUNT; idx++)
		g_sectors[idx].update = 0;

	return g_sectors;
}

void cardData_releaseSectorData(void){
	xSemaphoreGive(g_mutexSectorData);
}

int cardData_initiateCardOperation(card_operation_t op, struct card_format *targetFormat, card_operation_result_t callback){
	if((g_keyType != RFID_AUTH_KEY_A) && (g_keyType != RFID_AUTH_KEY_B))
		return RESULT_ERROR_INVALID_KEY;

	if((op == CARD_OPERATION_BEGIN_FORMAT) || (op == CARD_OPERATION_WRITE)){
		if(!targetFormat)
			return RESULT_ERROR_BAD_PARAMS;

		copyMemNonBlocking(&g_targetFormat, targetFormat, sizeof(struct card_format));
	}

	g_threadCallback = callback;
	g_operation = op;
	if(xTaskCreate(cardOperationThread, "RFID_OP", CARD_DATA_THREAD_STACK_SIZE, &g_operation, PRIORITY_TASK_RFID, &g_cardOpHandle) != pdPASS)
		return RESULT_ERROR_STACK_OVERFLOW;
	else
		return RESULT_OK;
}

void cardOperationThread(void *argument){
	card_operation_t thisOp = *((card_operation_t *)argument);
	char formatName[CARD_DATA_SECTOR_SIZE];
	struct card_format *readFormat = NULL;
	int result;
	uint8_t valueID;

	result = cardData_Connect(CARD_DATA_DEFAULT_CONN_TIMEOUT);
	if(result == RESULT_OK)
		terminal_print("Card connected, initiating operation\n");
	else
		terminal_print("No card tapped...exiting\n");

	ASSERT_THREAD_OP(result);

	if(!g_mutexSectorData){
		g_mutexSectorData = xSemaphoreCreateMutex();
		if(!g_mutexSectorData){
			result = RESULT_ERROR_STACK_OVERFLOW;
			ASSERT_THREAD_OP(result);
		}
	}

	xSemaphoreTake(g_mutexSectorData, portMAX_DELAY); // block the sector data
	if(thisOp == CARD_OPERATION_BEGIN_FORMAT){
		// Begin the format by writing fields permissions
		result = cardData_TryCardClean();
		ASSERT_THREAD_OP(result);

		for(uint8_t sec = 1; sec <= g_targetFormat.uiNumberOfFields; sec++){
			result = RFID_Authenticate(g_cardUID, g_cardKey, ((sec << 2) + 3), g_keyType);
			if(result == RFID_Result_Success){
				result = RFID_SetTrailerBlock(((sec << 2) + 3), 0x00, g_sectors[(sec - 1)].trailerConfig);
				if(result != RFID_Result_Success)
					break;
			}else{
				break;
			}
		}

		if(result != RFID_Result_Success){
			RFID_Halt();
			RFID_Disconnect();
			xSemaphoreGive(g_mutexSectorData);
			if(g_threadCallback)
				g_threadCallback(thisOp, RESULT_ERROR_EACCESS, NULL, NULL);

			vTaskDelete(NULL);
		}

		result = cardData_DefineFormat(g_targetFormat);
		ASSERT_THREAD_OP(result);
	}

	if(thisOp == CARD_OPERATION_WRITE){
		result = cardData_ReadSector(formatName, 0, 1); // card format name skips first sector (manuf. data)
		ASSERT_THREAD_OP(result);

		if(strcmp(g_targetFormat.sName, formatName) != 0){ // format on card and trying to be written must match
			RFID_Halt();
			RFID_Disconnect();
			xSemaphoreGive(g_mutexSectorData);
			if(g_threadCallback)
				g_threadCallback(thisOp, RESULT_ERROR_FORMAT_MISMATCH, NULL, NULL);

			vTaskDelete(NULL);
		}
	}

	if((thisOp == CARD_OPERATION_BEGIN_FORMAT) || (thisOp == CARD_OPERATION_WRITE)){
		for(uint32_t sec = 1; sec <= g_targetFormat.uiNumberOfFields; sec++){
			if(g_targetFormat.cfFields[sec-1].cfDataType == CARD_FIELD_TYPE_PURSE){
				if(thisOp == CARD_OPERATION_BEGIN_FORMAT) // begin format forces operation as ALTER to create the value
					g_sectors[(sec - 1)].purseOp = CARD_FIELD_OP_ALTER;

				if((g_sectors[(sec - 1)].update) || (thisOp != CARD_OPERATION_WRITE))
					result = cardData_PurseOperation(*((uint32_t *)g_sectors[(sec - 1)].sectorData), sec, g_sectors[(sec - 1)].purseOp);
			}else{// Write all three blocks with data
				// on write only write if update is marked
				if((g_sectors[(sec - 1)].update) || (thisOp != CARD_OPERATION_WRITE))
					result = cardData_WriteSector(g_sectors[(sec - 1)].sectorData, sec);
			}
			ASSERT_THREAD_OP(result);
		}
	}

	if(thisOp == CARD_OPERATION_READ){
		// Get the format
		result = cardData_ReadSector(formatName, 0, 1); // card format name skips first sector (manuf. data)
		ASSERT_THREAD_OP(result);
		result = cardFormat_getFormat(formatName, &readFormat);
		ASSERT_THREAD_OP(result);

		for(uint32_t sec = 1; sec < (readFormat->uiNumberOfFields + 1); sec++){
			// Write all three blocks with data
			if(readFormat->cfFields[sec-1].cfDataType == CARD_FIELD_TYPE_PURSE){
				result = cardData_ReadPurseValue((int32_t *)g_sectors[(sec - 1)].sectorData, sec, &valueID);
			}else{
				result = cardData_ReadSector(g_sectors[(sec - 1)].sectorData, sec, 0);
			}
			ASSERT_THREAD_OP(result);
		}
	}
	xSemaphoreGive(g_mutexSectorData);

	// if we reached this all went well
	if(g_threadCallback)
		g_threadCallback(thisOp, result, g_sectors, readFormat);

	RFID_Halt();
	RFID_Disconnect();
	vTaskDelete(NULL);
}
