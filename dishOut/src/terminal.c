/*
 * terminal.c
 *
 *  Created on: Jan. 18, 2021
 *      Author: otavio
 */

#define USART1_CLK_DIV                1U
#define USART1_DEFAULT_BAUDRATE       115200U
#define USART1_WAIT_FOR_DMA_MS        1500U

#include "terminal.h"
#include "gpio.h"
#include "stm32f7xx.h"
#include "lcd.h"

#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#include "FreeRTOS.h"
#include "semphr.h"

static char g_terminalBuffer[512U];
static SemaphoreHandle_t g_terminalBlock;

#ifdef TERMINAL_ON_LCD
static char g_lcdBufferText[17][30];
static uint8_t g_linesOnBuffer = 0;
static uint8_t g_nextWriteLine = 0;
static int8_t g_firstDrawLine = 0;

void terminal_print_on_lcd(const char *message){
	uint32_t textLength = strlen(message);
	uint32_t currentLine;

	for(uint32_t offset = 0; offset < textLength; offset += 30){
		if((textLength - offset) > 29)
			copyMemNonBlocking(g_lcdBufferText[g_nextWriteLine + 1], (message + offset), 30);
		else
			copyMemNonBlocking(g_lcdBufferText[g_nextWriteLine + 1], (message + offset), (textLength - offset));

		g_nextWriteLine++;
		if(g_nextWriteLine > 16)
			g_nextWriteLine = 0;

		if(g_linesOnBuffer < 17){
			g_linesOnBuffer++;
		}else{
			g_firstDrawLine++;
			if(g_firstDrawLine > 16)
				g_firstDrawLine = 0;
		}
	}

	currentLine = g_firstDrawLine;
	for(uint8_t line = 0; line < g_linesOnBuffer; line++){
		lcd_rectangle(COLOR_BLACK, 0, (currentLine << 4), LCD_WIDTH, 16);
		lcd_printText(g_lcdBufferText[currentLine], COLOR_WHITE, COLOR_BLACK, 0, (currentLine << 4));
		currentLine++;
		if(currentLine > 16)
			currentLine = 0;
	}
}
#endif

result_t terminal_init(void){
	// Pins: RX - PB7, TX - PA9
	RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN);

	GPIO_Clear(GPIOA, 9);
	GPIO_SetAF(GPIOA, 9, 7, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_HIGH, GPIO_PULL_PULL_UP);

	GPIO_Clear(GPIOB, 7);
	GPIO_SetAF(GPIOB, 7, 7, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_HIGH, GPIO_PULL_PULL_UP);

	// Set USART1 clk same as APB2
	RCC->DCKCFGR2 &= ~RCC_DCKCFGR2_USART1SEL;
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

	// USART1 config: 115200 1N8
	USART1->CR1 = 0; // disable the peripheral for safety
	USART1->CR1 = (USART_CR1_TE | USART_CR1_RE); // Oversampling by 16
	USART1->CR2 = 0; // 1 stop bit
	USART1->CR3 = USART_CR3_DMAT;
	USART1->BRR = ((SystemCoreClock >> USART1_CLK_DIV) / USART1_DEFAULT_BAUDRATE);
	USART1->GTPR = 0;
	USART1->RTOR = 0;
	USART1->RQR = 0;

	// Enable the peripheral
	USART1->CR1 |= USART_CR1_UE;

	// Configure the DMA for UART transmission
	// For USART1 TX DMA2 - Stream 7, Channel 4
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	g_terminalBlock = xSemaphoreCreateMutex();

	DMA2_Stream7->CR = 0;

	return RESULT_OK;
}

result_t terminal_send(const uint8_t *buffer, uint32_t length){
	// Wait for channel to be available
	if(waitUntilFlag(&(DMA2_Stream7->CR), DMA_SxCR_EN, USART1_WAIT_FOR_DMA_MS, 1) != RESULT_OK)
		return RESULT_ERROR_TIMEOUT;

	DMA2_Stream7->CR = ((4 << DMA_SxCR_CHSEL_Pos) | DMA_SxCR_MINC | (1 << DMA_SxCR_DIR_Pos));
	DMA2_Stream7->NDTR = length;
	DMA2_Stream7->PAR = (uint32_t)&(USART1->TDR);
	DMA2_Stream7->M0AR = (uint32_t)buffer;

	DMA2->HIFCR = 0x0F400000U;
	DMA2_Stream7->CR |= DMA_SxCR_EN;

	return RESULT_OK;
}

int terminal_print(const char *__b, ...){
	uint32_t length;
	va_list l;
	if(!__b)
		return 0;

	// Wait for channel to be available
	xSemaphoreTake(g_terminalBlock, portMAX_DELAY);
	if(waitUntilFlag(&(DMA2_Stream7->CR), DMA_SxCR_EN, USART1_WAIT_FOR_DMA_MS, 1) != RESULT_OK){
		xSemaphoreGive(g_terminalBlock);
		return 0;
	}

	// parse the input
	va_start(l, __b);
	length = vsprintf(g_terminalBuffer, __b, l);
	va_end(l);

#ifdef TERMINAL_ON_LCD
	terminal_print_on_lcd(g_terminalBuffer);
#endif

	DMA2_Stream7->CR = ((4 << DMA_SxCR_CHSEL_Pos) | DMA_SxCR_MINC | (1 << DMA_SxCR_DIR_Pos));
	DMA2_Stream7->NDTR = length;
	DMA2_Stream7->PAR = (uint32_t)&(USART1->TDR);
	DMA2_Stream7->M0AR = (uint32_t)g_terminalBuffer;

	DMA2->HIFCR = 0x0F400000U;
	DMA2_Stream7->CR |= DMA_SxCR_EN;
	xSemaphoreGive(g_terminalBlock);

	return length;
}
