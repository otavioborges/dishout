/*
 * tcpIpDriver.c
 *
 *  Created on: Jan. 5, 2021
 *      Author: otavio
 */

#include "rmii.h"
#include "sdram.h"
#include "priorities.h"

#include "FreeRTOS.h"
#include "semphr.h"
#include "queue.h"

#include "tcpIpDriver.h"

#include "lwip/etharp.h"
#include "lwip/sys.h"

#include <string.h>

#define TCPIP_RECV_COUNT           8U
#define TCPIP_RECV_BUFFER_LENGTH   2048U
#define TCPIP_SEND_TIMEOUT         500U

static uint8_t g_recvBuffers[TCPIP_RECV_COUNT][TCPIP_RECV_BUFFER_LENGTH] __attribute__((aligned(16)));
static uint32_t g_recvLength[TCPIP_RECV_COUNT];
static uint8_t g_sendBuffer[TCPIP_RECV_BUFFER_LENGTH];
static uint32_t g_sendLength;

static SemaphoreHandle_t g_writeMutex;

volatile static uint8_t g_bufferIdx[TCPIP_RECV_COUNT];
static TaskHandle_t g_recvTask;
static struct netif *g_globalInterface = NULL;

static void RecvThread(void *argument);
err_t tcpIpDriver_output(struct netif *netif, struct pbuf *p);

void rmii_recvHandler(uint32_t bufferIdx){
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	g_bufferIdx[bufferIdx] = 1;
	if(g_recvTask != NULL){
		vTaskNotifyGiveFromISR(g_recvTask, &xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}
}

err_t tcpIpDriver_init(struct netif *netif){
	result_t opResult;

	for(uint8_t idx = 0; idx < TCPIP_RECV_COUNT; idx++){
		g_recvLength[idx] = 0;
		g_bufferIdx[idx] = 0;
	}

	rmii_initRecvBuffer((uint8_t*)g_recvBuffers, TCPIP_RECV_COUNT, g_recvLength, TCPIP_RECV_BUFFER_LENGTH);
	opResult = rmii_init();
	if(opResult != RESULT_OK)
		return ERR_MEM;

	opResult = rmii_start();
	if(opResult != RESULT_OK)
		return ERR_MEM;

	g_writeMutex = xSemaphoreCreateMutex();
	if(xTaskCreate(RecvThread, "RECV", 1024U, (void *)&g_bufferIdx, PRIORITY_TASK_RECV, &g_recvTask) != pdPASS)
		return ERR_IF;

	copyMemNonBlocking((void *)netif->hwaddr, (void *)rmii_interfaceMacAddr, 6);
	netif->hwaddr_len = 6;
	netif->linkoutput = tcpIpDriver_output;
	netif->mtu = 1500U;
	strcpy(netif->name, "en");
	netif->next = NULL;
	netif->output = etharp_output;

	// TODO: monitor link status
	netif->flags = (NETIF_FLAG_LINK_UP | NETIF_FLAG_ETHARP | NETIF_FLAG_BROADCAST);

	g_globalInterface = netif;

	return ERR_OK;
}

err_t tcpIpDriver_output(struct netif *netif, struct pbuf *p){
	result_t opResult;

	if(xSemaphoreTake(g_writeMutex, TCPIP_SEND_TIMEOUT) != pdTRUE)
		return pdFAIL;

	g_sendLength = 0;
	while(p){
		copyMemNonBlocking((g_sendBuffer + g_sendLength), p->payload, p->len);
		g_sendLength += p->len;
		p = p->next;
	}

	opResult = rmii_transmit(g_sendBuffer, g_sendLength);
	if(opResult != RESULT_OK){
		xSemaphoreGive(g_writeMutex);
		return ERR_IF;
	}

	xSemaphoreGive(g_writeMutex);
	return ERR_OK;
}

static void RecvThread(void *argument){
	struct pbuf *recvPacket, *next;
	uint32_t recvLength, offset;
	uint8_t nextBuffer;

	for(;;){
		ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
		for(uint8_t idx = 0; idx < TCPIP_RECV_COUNT; idx++){
			if(g_bufferIdx[idx]){
				nextBuffer = idx;
				g_bufferIdx[idx] = 0;
			}
		}

		if(g_globalInterface){
			if(g_recvLength[nextBuffer]){
				recvPacket = pbuf_alloc(PBUF_RAW, g_recvLength[nextBuffer], PBUF_POOL);
				if(recvPacket){
					if(g_recvLength[nextBuffer] > recvPacket->len){
						next = recvPacket;
						offset = 0;
						while(next && (offset < g_recvLength[nextBuffer])){
							if((g_recvLength[nextBuffer] - offset) > next->len)
								recvLength = next->len;
							else
								recvLength = g_recvLength[nextBuffer] - offset;

							copyMemNonBlocking(next->payload, (g_recvBuffers[nextBuffer] + offset), recvLength);
							next->len = recvLength;
							next->tot_len = g_recvLength[nextBuffer];
							offset += recvLength;
							next = next->next;
						}
					}else{
						recvPacket->len = g_recvLength[nextBuffer];
						recvPacket->tot_len = g_recvLength[nextBuffer];
						copyMemNonBlocking(recvPacket->payload, (void *)(g_recvBuffers + nextBuffer), g_recvLength[nextBuffer]);
					}

					g_globalInterface->input(recvPacket, g_globalInterface);
					pbuf_free(recvPacket);
				}
			}else{
				__asm("nop");
			}
		}
	}
}

/* lwIP external tools implementations */
sys_prot_t sys_arch_protect(void){
	vPortEnterCritical();
	return (sys_prot_t)1;
}

void sys_arch_unprotect(sys_prot_t pval){
	(void)pval;
	vPortExitCritical();
}

err_t sys_mutex_new(sys_mutex_t *mutex){
	*mutex = xSemaphoreCreateMutex();
	if(*mutex)
		return ERR_OK;
	else
		return ERR_MEM;
}

void sys_mutex_lock(sys_mutex_t *mutex){
	if(*mutex)
		xSemaphoreTake(*mutex, portMAX_DELAY);
}

void sys_mutex_unlock(sys_mutex_t *mutex){
	if(*mutex)
		xSemaphoreGive(*mutex);
}


void sys_mutex_free(sys_mutex_t *mutex){
	if(*mutex)
		vQueueDelete(*mutex);
}

err_t sys_sem_new(sys_sem_t *sem, u8_t count){
	*sem = xSemaphoreCreateCounting(0xFFFFU, count);
	if(*sem)
		return ERR_OK;
	else
		return ERR_MEM;
}

void sys_sem_signal(sys_sem_t *sem){
	if(*sem)
		xSemaphoreGive(*sem);
}

u32_t sys_arch_sem_wait(sys_sem_t *sem, u32_t timeout){
	BaseType_t result;

	if(*sem){
		if(timeout)
			result = xSemaphoreTake(*sem, pdMS_TO_TICKS(timeout));
		else
			result = xSemaphoreTake(*sem, portMAX_DELAY);

		if(result == pdTRUE)
			return 0;
		else
			return SYS_ARCH_TIMEOUT;
	}else{
		return 0;
	}
}

void sys_sem_free(sys_sem_t *sem){
	if(*sem)
		vQueueDelete(*sem);
}

int sys_sem_valid(sys_sem_t *sem){
	if(*sem)
		return 1;
	else
		return 0;
}

void sys_sem_set_invalid(sys_sem_t *sem){
	*sem = NULL;
}

err_t sys_mbox_new(sys_mbox_t *mbox, int size){
	*mbox = xQueueCreate(size, sizeof(void *));
	if(*mbox)
		return ERR_OK;
	else
		return ERR_MEM;
}

void sys_mbox_post(sys_mbox_t *mbox, void *msg){
	if(*mbox && msg) // TODO: can NULL be queued?
		xQueueSend(*mbox, &msg, portMAX_DELAY);
}

err_t sys_mbox_trypost(sys_mbox_t *mbox, void *msg){
		if(xQueueSend(*mbox, &msg, 0) == pdPASS)
			return ERR_OK;
		else
			return ERR_MEM;
}

err_t sys_mbox_trypost_fromisr(sys_mbox_t *mbox, void *msg){
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if(*mbox && msg){
		if(xQueueSendFromISR(*mbox, msg, &xHigherPriorityTaskWoken) == pdTRUE){
			portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
			return ERR_OK;
		}else{
			portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
			return ERR_MEM;
		}
	}else{
		return ERR_OK;
	}
}

u32_t sys_arch_mbox_fetch(sys_mbox_t *mbox, void **msg, u32_t timeout){
	void *dummy;
	BaseType_t result;

	if(*mbox){
		if(timeout)
			result = xQueueReceive(*mbox, &dummy, pdMS_TO_TICKS(timeout));
		else
			result = xQueueReceive(*mbox, &dummy, portMAX_DELAY);

		if(result == pdTRUE){
			*msg = dummy;
			return 0;
		}else{
			*msg = NULL;
			return SYS_ARCH_TIMEOUT;
		}
	}else{
		return 0;
	}
}

u32_t sys_arch_mbox_tryfetch(sys_mbox_t *mbox, void **msg){
	void *dummy;

	if(*mbox){
		if(xQueueReceive(*mbox, &dummy, 0) == pdTRUE){
			*msg = dummy;
			return 0;
		}else{
			return SYS_MBOX_EMPTY;
		}
	}else{
		return SYS_MBOX_EMPTY;
	}
}

void sys_mbox_set_invalid(sys_mbox_t *mbox){
	*mbox = NULL;
}

void sys_mbox_free(sys_mbox_t *mbox){
	if(*mbox)
		vQueueDelete(*mbox);
}

int sys_mbox_valid(sys_mbox_t *mbox){
	if(*mbox)
		return 1;
	else
		return 0;
}

/* lwIP System Calls */
void sys_init(void){
	// nothing to be done
}

sys_thread_t sys_thread_new(const char *name, lwip_thread_fn thread, void *arg, int stacksize, int prio){
	sys_thread_t result;
	if(xTaskCreate(thread, name, stacksize, arg, prio, &result) != pdPASS)
		ErrorHandler(RESULT_ERROR_TASK_CREATE);
	else
		return result;

	return NULL;
}

u32_t sys_now(void){
	return getUptime();
}

void sys_msleep(u32_t ms){
	vTaskDelay(ms);
}

void* pvPortCalloc(size_t xElements, size_t xSizeOfElement){
	void *result = pvPortMalloc((xElements * xSizeOfElement));
	if(result){
		memset(result, 0, (xElements * xSizeOfElement));
	}

	return result;
}
