/*
 * gpio.c
 *
 *  Created on: Nov. 15, 2020
 *      Author: otavio
 */

#include "port.h"

void PORT_Clear(GPIO_TypeDef *port, uint16_t mask){
	port->OTYPER &= ~(mask);

	for(uint8_t pin = 0; pin < 16; pin++){
		if(!((mask >> pin) & 0x01))
			continue;

		port->MODER &= ~(0x0003 << (pin << 1));
		port->OSPEEDR &= ~(0x0003 << (pin << 1));
		port->PUPDR &= ~(0x0003 << (pin << 1));
		if(pin < 8)
			port->AFR[0] &= ~(0x000F << (pin << 2));
		else
			port->AFR[1] &= ~(0x000F << ((pin - 8) << 2));
	}
}

void PORT_SetOutput(GPIO_TypeDef *port, uint16_t mask, gpio_type_t type, gpio_speed_t speed, gpio_pull_t pull){
	if(type)
		port->OTYPER |= mask;
	else
		port->OTYPER &= ~mask;

	for(uint8_t pin = 0; pin < 16; pin++){
		if(!((mask >> pin) & 0x01))
			continue;

		port->MODER |= (0x01 << (pin << 1));
		port->OSPEEDR |= (speed << (pin << 1));
		port->PUPDR |= (pull << (pin << 1));
	}
}

void PORT_SetInput(GPIO_TypeDef *port, uint16_t mask, gpio_speed_t speed, gpio_pull_t pull){
	for(uint8_t pin = 0; pin < 16; pin++){
		if(!((mask >> pin) & 0x01))
			continue;

		port->OSPEEDR |= (speed << (pin << 1));
		port->PUPDR |= (pull << (pin << 1));
	}
}

void PORT_SetAF(GPIO_TypeDef *port, uint16_t mask, uint8_t alternate, gpio_type_t type, gpio_speed_t speed, gpio_pull_t pull){
	if(type)
		port->OTYPER |= mask;
	else
		port->OTYPER &= ~mask;

	for(uint8_t pin = 0; pin < 16; pin++){
		if(!((mask >> pin) & 0x01))
			continue;

		port->MODER |= (0x02 << (pin << 1));
		port->OSPEEDR |= (speed << (pin << 1));
		port->PUPDR |= (pull << (pin << 1));
		if(pin < 8)
			port->AFR[0] |= (alternate << (pin << 2));
		else
			port->AFR[1] |= (alternate << ((pin - 8) << 2));
	}
}

void PORT_Reset(GPIO_TypeDef *port, uint16_t mask){
	port->BSRR = ((uint32_t)mask) << 16;
}

void PORT_Set(GPIO_TypeDef *port, uint16_t mask){
	port->BSRR = (uint32_t)mask & 0xFFFF;
}

uint16_t PORT_Read(GPIO_TypeDef *port, uint16_t mask){
	return (uint16_t)(port->IDR & mask);
}
