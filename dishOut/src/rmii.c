/*
 * rmii.c
 *
 *  Created on: Jan. 5, 2021
 *      Author: otavio
 */

#include "rmii.h"
#include "port.h"
#include "gpio.h"
#include "util.h"
#include "priorities.h"
#include "stm32f7xx.h"

#define ETHERNET_PORT_GPIOA			0x86
#define ETHERNET_PORT_GPIOC			0x32
#define ETHERNET_PORT_GPIOG			0x6804

#define RMII_TIMEOUT                3000U
#define RMII_LINK_TIMEOUT           30000U

#define ETHERNET_NO_LINK_TIMEOUT	0

#define ETHERNET_DMATX_DESC0_OWN	(uint32_t)(1 << 31)
#define ETHERNET_DMATX_DESC0_LS		(uint32_t)(1 << 29)
#define ETHERNET_DMATX_DESC0_FS		(uint32_t)(1 << 28)
#define ETHERNET_DMATX_DESC0_DC		(uint32_t)(1 << 27)
#define ETHERNET_DMATX_DESC0_DP		(uint32_t)(1 << 26)
#define ETHERNET_DMATX_DESC0_TCH	(uint32_t)(1 << 20)
#define ETHERNET_DMATX_DESC0_CIC	(uint32_t)(3 << 22)

#define ETHERNET_DMATX_DESC1_TBS2	(uint32_t)(0x1FFF << 16)
#define ETHERNET_DMATX_DESC1_TBS	(uint32_t)(0x1FFF)

#define ETHERNET_DMARX_DESC0_OWN	(uint32_t)(1 << 31)
#define ETHERNET_DMARX_DESC0_AFM	(uint32_t)(1 << 30)
#define ETHERNET_DMARX_DESC0_FL		(uint32_t)(0x3FFF << 16)
#define ETHERNET_DMARX_DESC0_FS		(uint32_t)(1 << 9)
#define ETHERNET_DMARX_DESC0_LS		(uint32_t)(1 << 8)

#define ETHERNET_DMARX_DESC1_RCH	(uint32_t)(1 << 14)
#define ETHERNET_DMARX_DESC1_RBS	(uint32_t)(0x1FFF)

#define ETHERNET_PHY_ADDRESS		0x00
#define ETHERNET_PHY_REG_CONTROL	0x00
#define ETHERNET_PHY_REG_STATUS		0x01
#define ETHERNET_PHY_REG_PHY_ID1	0x02
#define ETHERNET_PHY_REG_PHY_ID2	0x03
#define ETHERNET_PHY_REG_SPECIAL	0x1F

ethernet_dma_desc_t g_rxDmaDescriptor[ETHERNET_RX_BUFFER_COUNT] __attribute__((section(".RxDescripSection")));
ethernet_dma_desc_t g_txDmaDescriptor[ETHERNET_TX_BUFFER_COUNT] __attribute__((section(".TxDescripSection")));
ethernet_dma_desc_t *g_nextRxDesc = g_rxDmaDescriptor;	// points to the first descriptor
ethernet_dma_desc_t *g_nextTxDesc = g_txDmaDescriptor;	// points to the first descriptor

uint8_t g_rxBuffer[ETHERNET_RX_BUFFER_COUNT][ETHERNET_BUFFER_SIZE] __attribute__((section(".RxarraySection")));
uint8_t g_txBuffer[ETHERNET_TX_BUFFER_COUNT][ETHERNET_BUFFER_SIZE] __attribute__((section(".TxarraySection")));

const uint8_t rmii_interfaceMacAddr[6] = {0x00, 0xFE, 0x1B, 0x35, 0x9E, 0x12};
static uint8_t rmii_totalRecvBuffers;
static uint8_t rmii_currentRecvBuffer;
static uint32_t rmii_currentRecvOffset = 0;
static uint8_t *rmii_recvBuffers;
static uint32_t *rmii_recvLength;
static uint32_t rmii_bufferLength;
static volatile uint32_t rmii_pendingRecvs = 0;

void DMA2_Stream0_IRQHandler(void){
	ethernet_dma_desc_t *descriptor = g_nextRxDesc;
	uint32_t flags = (descriptor->desc0 & (ETHERNET_DMARX_DESC0_FS | ETHERNET_DMARX_DESC0_LS));
	uint32_t frameLength;

	if(flags == (ETHERNET_DMARX_DESC0_FS | ETHERNET_DMARX_DESC0_LS)){
		rmii_recvLength[rmii_currentRecvBuffer] = ((descriptor->desc0 & ETHERNET_DMARX_DESC0_FL) >> 16);
		rmii_currentRecvOffset = 0;
	}else if(flags == ETHERNET_DMARX_DESC0_FS){
		rmii_recvLength[rmii_currentRecvBuffer] = ((descriptor->desc0 & ETHERNET_DMARX_DESC0_FL) >> 16);
		rmii_currentRecvOffset = rmii_recvLength[rmii_currentRecvBuffer];
	}else if(flags == ETHERNET_DMARX_DESC0_LS){
		rmii_recvLength[rmii_currentRecvBuffer] += ((descriptor->desc0 & ETHERNET_DMARX_DESC0_FL) >> 16);
		rmii_currentRecvOffset = 0;
	}else{ // middle of frame
		rmii_recvLength[rmii_currentRecvBuffer] += ((descriptor->desc0 & ETHERNET_DMARX_DESC0_FL) >> 16);
		rmii_currentRecvOffset += rmii_recvLength[rmii_currentRecvBuffer];
	}

	if(flags & ETHERNET_DMARX_DESC0_LS){
		// Signal that frame was fully received
		rmii_recvHandler(rmii_currentRecvBuffer);

		rmii_currentRecvBuffer++;
		if(rmii_currentRecvBuffer >= rmii_totalRecvBuffers)
			rmii_currentRecvBuffer = 0;
	}

	// Release this frame
	descriptor->desc0 |= ETHERNET_DMARX_DESC0_OWN;
	// Let us point to the next descriptor
	g_nextRxDesc = (ethernet_dma_desc_t *)descriptor->desc3;

	// Check if there is pending frames
	if(rmii_pendingRecvs){
		descriptor = g_nextRxDesc;

		frameLength = ((descriptor->desc0 & ETHERNET_DMARX_DESC0_FL) >> 16);
		DMA2_Stream0->CR = ((1 << DMA_SxCR_CHSEL_Pos) | DMA_SxCR_MINC | DMA_SxCR_PINC | DMA_SxCR_DIR_1 | DMA_SxCR_TCIE);
		DMA2_Stream0->NDTR = frameLength;
		DMA2_Stream0->PAR = (descriptor->desc2);
		DMA2_Stream0->M0AR = (uint32_t)(rmii_recvBuffers + rmii_currentRecvOffset + (rmii_currentRecvBuffer * rmii_bufferLength));

		DMA2->LIFCR = 0x3F;
		DMA2_Stream0->CR |= DMA_SxCR_EN;

		rmii_pendingRecvs--;
	}else{
		DMA2->LIFCR = 0x3F;
	}
}

void ETH_IRQHandler(void){
	uint16_t frameLength = 0;
	uint8_t framesCount = 0;
	ethernet_dma_desc_t *descriptor = g_nextRxDesc;

	while(descriptor->desc0 & ETHERNET_DMARX_DESC0_OWN)
		__asm("nop");

	// we own the descriptor, copy data
	if(DMA2_Stream0->CR & DMA_SxCR_EN){ // dma is busy, mark for later
		rmii_pendingRecvs++;
		return;
	}

	frameLength = ((descriptor->desc0 & ETHERNET_DMARX_DESC0_FL) >> 16);
	DMA2_Stream0->CR = ((1 << DMA_SxCR_CHSEL_Pos) | DMA_SxCR_MINC | DMA_SxCR_PINC | DMA_SxCR_DIR_1 | DMA_SxCR_TCIE);
	DMA2_Stream0->NDTR = frameLength;
	DMA2_Stream0->PAR = (descriptor->desc2);
	DMA2_Stream0->M0AR = (uint32_t)(rmii_recvBuffers + rmii_currentRecvOffset + (rmii_currentRecvBuffer * rmii_bufferLength));

	DMA2->LIFCR = 0x3F;
	DMA2_Stream0->CR |= DMA_SxCR_EN;

	// Release the IRQ
	ETH->DMASR = 0x1FFFF;
}

static result_t phyWrite(uint8_t addr, uint8_t reg, uint16_t value){
	uint32_t miiarValue = (((addr & 0x1F) << ETH_MACMIIAR_PA_Pos) | ((reg & 0x1F) << ETH_MACMIIAR_MR_Pos) | ETH_MACMIIAR_MW);

	if(waitUntilFlag(&(ETH->MACMIIAR), ETH_MACMIIAR_MB, RMII_TIMEOUT, 1) != RESULT_OK)
		return RESULT_ERROR_TIMEOUT;

	ETH->MACMIIDR = value;
	ETH->MACMIIAR = miiarValue;
	ETH->MACMIIAR |= ETH_MACMIIAR_MB;

	if(waitUntilFlag(&(ETH->MACMIIAR), ETH_MACMIIAR_MB, RMII_TIMEOUT, 1) != RESULT_OK)
		return RESULT_ERROR_TIMEOUT;
	else
		return RESULT_OK;
}

result_t phyRead(uint8_t addr, uint8_t reg, uint16_t *value){
	uint32_t miiarValue = (((addr & 0x1F) << ETH_MACMIIAR_PA_Pos) | ((reg & 0x1F) << ETH_MACMIIAR_MR_Pos));

	if(waitUntilFlag(&(ETH->MACMIIAR), ETH_MACMIIAR_MB, RMII_TIMEOUT, 1) != RESULT_OK){
		*value = 0;
		return RESULT_ERROR_TIMEOUT;
	}

	ETH->MACMIIAR = miiarValue;
	ETH->MACMIIAR |= ETH_MACMIIAR_MB;

	if(waitUntilFlag(&(ETH->MACMIIAR), ETH_MACMIIAR_MB, RMII_TIMEOUT, 1) != RESULT_OK){
		*value = 0;
		return RESULT_ERROR_TIMEOUT;
	}else{
		*value = (uint16_t)ETH->MACMIIDR;
		return RESULT_OK;
	}
}

static void writeMACRegister(volatile uint32_t *reg, uint32_t value){
	*(reg) = value;
	value = *(reg);
	delay(1);
	*(reg) = value;
}

result_t rmii_init(void){
	uint16_t phyValue = 0;
	uint32_t timeout;
	result_t opResult;

	/* Ethernet pins configuration *********************************************
		RMII_REF_CLK ----------------------> PA1
		RMII_MDIO -------------------------> PA2
		RMII_MDC --------------------------> PC1
		RMII_MII_CRS_DV -------------------> PA7
		RMII_MII_RXD0 ---------------------> PC4
		RMII_MII_RXD1 ---------------------> PC5
		RMII_MII_RXER ---------------------> PG2
		RMII_MII_TX_EN --------------------> PG11
		RMII_MII_TXD0 ---------------------> PG13
		RMII_MII_TXD1 ---------------------> PG14
	***************************************************************************/

	RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIOGEN);

	PORT_Clear(GPIOA, ETHERNET_PORT_GPIOA);
	PORT_Clear(GPIOC, ETHERNET_PORT_GPIOC);
	PORT_Clear(GPIOG, ETHERNET_PORT_GPIOG);

	PORT_SetAF(GPIOA, ETHERNET_PORT_GPIOA, 11, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	PORT_SetAF(GPIOC, ETHERNET_PORT_GPIOC, 11, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	PORT_SetAF(GPIOG, ETHERNET_PORT_GPIOG, 11, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);

	NVIC_SetPriority(ETH_IRQn, PRIORITY_IRQ_ETH);
	NVIC_EnableIRQ(ETH_IRQn);

	// Enable SYSCFG for RMII
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
	SYSCFG->PMC |= SYSCFG_PMC_MII_RMII_SEL;

	RCC->AHB1ENR |= (RCC_AHB1ENR_ETHMACEN | RCC_AHB1ENR_ETHMACPTPEN | RCC_AHB1ENR_ETHMACRXEN | RCC_AHB1ENR_ETHMACTXEN);

	// Reset interface
	ETH->DMABMR |= ETH_DMABMR_SR;
	if(waitUntilFlag(&(ETH->DMABMR), ETH_DMABMR_SR, RMII_TIMEOUT, 1) != RESULT_OK){
		RCC->AHB1ENR &= ~(RCC_AHB1ENR_ETHMACEN | RCC_AHB1ENR_ETHMACPTPEN | RCC_AHB1ENR_ETHMACRXEN | RCC_AHB1ENR_ETHMACTXEN);
		return RESULT_ERROR_TIMEOUT;
	}

	// configure MDC divider, HCLK = 216MHz. CR = 0b100 Div102
	ETH->MACMIIAR &= ~ETH_MACMIIAR_CR;
	ETH->MACMIIAR |= ETH_MACMIIAR_CR_Div102;

	// Config the PHY
	phyValue = 0x8000;
	opResult = phyWrite(ETHERNET_PHY_ADDRESS, ETHERNET_PHY_REG_CONTROL, phyValue);
	ASSERT_RESULT(opResult);
	delay(5);

	// Wait for link UP
	timeout = RMII_LINK_TIMEOUT + getUptime();
	do{
		opResult = phyRead(ETHERNET_PHY_ADDRESS, ETHERNET_PHY_REG_STATUS, &phyValue);
		ASSERT_RESULT(opResult);
	}while((getUptime() < timeout) && ((phyValue & 0x0004) == 0));

	if((phyValue & 0x0004) == 0)
		return RESULT_ERROR_LINK_DOWN;

	// Initiate auto-negotiation
	phyValue = 0x1000;
	opResult = phyWrite(ETHERNET_PHY_ADDRESS, ETHERNET_PHY_REG_CONTROL, phyValue);
	ASSERT_RESULT(opResult);

	// Wait for auto-negotiation
	timeout = RMII_LINK_TIMEOUT + getUptime();
	do{
		opResult = phyRead(ETHERNET_PHY_ADDRESS, ETHERNET_PHY_REG_SPECIAL, &phyValue);
		ASSERT_RESULT(opResult);
	}while((getUptime() < timeout) && ((phyValue & 0x1000) == 0));

	if((phyValue & 0x1000) == 0)
		return RESULT_ERROR_PHY;
	else
		return RESULT_OK;
}

result_t rmii_start(void){
	uint16_t desc;
	uint16_t phyValue = 0;
	uint32_t maccrValue = 0;
	result_t opResult;

	// start the descriptors, TRASMIT
	for(desc = 0; desc < ETHERNET_TX_BUFFER_COUNT; desc++){
		g_txDmaDescriptor[desc].desc0 = 0; // Control descriptors

		g_txDmaDescriptor[desc].desc0 = (ETHERNET_DMATX_DESC0_TCH | ETHERNET_DMATX_DESC0_CIC);
		g_txDmaDescriptor[desc].desc1 = 0;
		g_txDmaDescriptor[desc].desc2 = (uint32_t)(g_txBuffer + desc);
		if(desc == (ETHERNET_RX_BUFFER_COUNT - 1)){
			// Last descriptor, point to first buffer
			g_txDmaDescriptor[desc].desc3 = (uint32_t)g_txDmaDescriptor;
		}else{
			// Point to next buffer on chain
			g_txDmaDescriptor[desc].desc3 = (uint32_t)(g_txDmaDescriptor + desc + 1);
		}
	}

	// RECEIVE
	for(desc = 0; desc < ETHERNET_RX_BUFFER_COUNT; desc++){
		g_rxDmaDescriptor[desc].desc0 = 0; // Control descriptors

		g_rxDmaDescriptor[desc].desc1 = (ETHERNET_DMARX_DESC1_RCH | ETHERNET_BUFFER_SIZE);
		g_rxDmaDescriptor[desc].desc2 = (uint32_t)(g_rxBuffer + desc);
		if(desc == (ETHERNET_RX_BUFFER_COUNT - 1)){
			// Last descriptor, point to first buffer
			g_rxDmaDescriptor[desc].desc3 = (uint32_t)g_rxDmaDescriptor;
		}else{
			// Point to next buffer on chain
			g_rxDmaDescriptor[desc].desc3 = (uint32_t)(g_rxDmaDescriptor + desc + 1);
		}

		// Release descriptor
		g_rxDmaDescriptor[desc].desc0 = ETHERNET_DMARX_DESC0_OWN;
	}

	g_nextRxDesc = g_txDmaDescriptor;
	g_nextRxDesc = g_rxDmaDescriptor; // point to first descriptor

	ETH->DMATDLAR = (uint32_t)g_txDmaDescriptor;
	ETH->DMARDLAR = (uint32_t)g_rxDmaDescriptor;

	// Start the peripheral
	opResult = phyRead(ETHERNET_PHY_ADDRESS, ETHERNET_PHY_REG_SPECIAL, &phyValue);
	ASSERT_RESULT(opResult);

	// Initiate MAC and DMA

	// Configure our link accordingly
	// Check for 100 or 10BASE
	maccrValue = 0;
	if((phyValue & 0x0008))
		maccrValue |= ETH_MACCR_FES;

	if((phyValue & 0x0010))
	// Check for half or full duplex (on 10BASE)
		maccrValue |= ETH_MACCR_DM;

	maccrValue |= (0x2000000 | ETH_MACCR_IPCO);
	writeMACRegister(&(ETH->MACCR), maccrValue);

	ETH->MACHTHR = 0;
//	ETH->MACHTLR = 0x01; // Allow hash filtering to work only for ff:ff:ff:ff:ff:ff

//	maccrValue = (ETH_MACFFR_PAM | ETH_MACFFR_PM);
//	maccrValue = ETH_MACFFR_HM;
	maccrValue = 0; // avoid ALL boradcast
	writeMACRegister(&(ETH->MACFFR), maccrValue);

	writeMACRegister(&(ETH->MACFCR), ETH_MACFCR_ZQPD);
	writeMACRegister(&(ETH->MACVLANTR), 0);

	maccrValue = (ETH_DMAOMR_RSF | ETH_DMAOMR_TSF | ETH_DMAOMR_OSF);
	writeMACRegister(&(ETH->DMAOMR), maccrValue);

	maccrValue = (ETH_DMABMR_AAB | ETH_DMABMR_FB | (32 << 17U) | (32 << 8U));
	writeMACRegister(&(ETH->DMABMR), maccrValue);

	ETH->DMAIER = (ETH_DMAIER_NISE | ETH_DMAIER_RIE);
	ETH->MACA0HR = ((((uint32_t)rmii_interfaceMacAddr[5]) << 8) | ((uint32_t)rmii_interfaceMacAddr[4]));
	ETH->MACA0LR = ((((uint32_t)rmii_interfaceMacAddr[3]) << 24) | (((uint32_t)rmii_interfaceMacAddr[2]) << 16) |
			(((uint32_t)rmii_interfaceMacAddr[1]) << 8) | ((uint32_t)rmii_interfaceMacAddr[0]));

	maccrValue = ETH->MACCR;
	maccrValue |= (ETH_MACCR_RE | ETH_MACCR_TE);
	writeMACRegister(&(ETH->MACCR), maccrValue);

	maccrValue = ETH->DMAOMR;
	maccrValue |= ETH_DMAOMR_FTF;
	writeMACRegister(&(ETH->DMAOMR), maccrValue);

	ETH->DMAOMR |= (ETH_DMAOMR_ST | ETH_DMAOMR_SR);

	// enable the receive DMA
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	NVIC_SetPriority(DMA2_Stream0_IRQn, PRIORITY_IRQ_DMA_ETH);
	NVIC_EnableIRQ(DMA2_Stream0_IRQn);

	return RESULT_OK;
}

result_t rmii_transmit(uint8_t *buffer, uint32_t length){
	uint8_t *frame;
	uint32_t dataToCopy;

	while(length){
		if(length > ETHERNET_BUFFER_SIZE)
			dataToCopy = ETHERNET_BUFFER_SIZE;
		else
			dataToCopy = length;

		if(waitUntilFlag(&(g_nextTxDesc->desc0), 0x80000000U, RMII_TIMEOUT, 1) != RESULT_OK)
			return RESULT_ERROR_BUSY;

		frame = (uint8_t *)g_nextTxDesc->desc2;

		if(copyMemNonBlocking(frame, buffer, dataToCopy) != dataToCopy)
			return RESULT_ERROR_DMA;

		g_nextTxDesc->desc1  = dataToCopy;
		g_nextTxDesc->desc0 |= 0x30000000 | // LS+FS = single-buffer packet
							   0x80000000 ; // give ownership to eth DMA

		for(uint8_t delay = 0; delay < 54; delay++)
			__asm("nop");

		// see if DMA is stuck because it wasn't transmitting (which will almost
		// always be the case). if it's stuck, kick it into motion again
		const volatile uint32_t tps = ETH->DMASR & ETH_DMASR_TPS;
		if (tps == ETH_DMASR_TPS_Suspended)
			ETH->DMATPDR = 0; // transmit poll demand = kick it moving again

		g_nextTxDesc = (ethernet_dma_desc_t *)g_nextTxDesc->desc3;
		length -= dataToCopy;
	}

	return RESULT_OK;
}

result_t rmii_initRecvBuffer(uint8_t *buffers, uint32_t count, uint32_t *recvLength, uint32_t bufferLength){
	if(!buffers || !recvLength || (count == 0))
		return RESULT_ERROR_BAD_PARAMS;

	rmii_totalRecvBuffers = count;
	rmii_currentRecvBuffer = 0;
	rmii_currentRecvOffset = 0;
	rmii_recvBuffers = buffers;
	rmii_recvLength = recvLength;
	rmii_bufferLength = bufferLength;

	return RESULT_OK;
}

result_t rmii_getConnectionStatus(void){
	uint16_t phyValue;
	phyRead(ETHERNET_PHY_ADDRESS, ETHERNET_PHY_REG_STATUS, &phyValue);
	if((phyValue & 0x0004))
		return RESULT_OK;
	else
		return RESULT_ERROR_LINK_DOWN;
}

// User-defined callback for ethernet received packets
__WEAK void rmii_recvHandler(uint32_t bufferIdx){
	__asm("nop");
}
