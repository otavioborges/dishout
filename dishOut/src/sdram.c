/*
 * sdram.c
 *
 *  Created on: Jan. 5, 2021
 *      Author: otavio
 */

#define EXMEM_RAM_GPIOC_MASK						0x00000008U
#define EXMEM_RAM_GPIOD_MASK						0x0000C703U
#define EXMEM_RAM_GPIOE_MASK						0x0000FF83U
#define EXMEM_RAM_GPIOF_MASK						0x0000F83FU
#define EXMEM_RAM_GPIOG_MASK						0x00008133U
#define EXMEM_RAM_GPIOH_MASK						0x00000028U

#define SDRAM_MODEREG_BURST_LENGTH_1				((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_LENGTH_2				((uint16_t)0x0001)
#define SDRAM_MODEREG_BURST_LENGTH_4				((uint16_t)0x0002)
#define SDRAM_MODEREG_BURST_LENGTH_8				((uint16_t)0x0004)
#define SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL			((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_TYPE_INTERLEAVED		((uint16_t)0x0008)
#define SDRAM_MODEREG_CAS_LATENCY_2					((uint16_t)0x0020)
#define SDRAM_MODEREG_CAS_LATENCY_3					((uint16_t)0x0030)
#define SDRAM_MODEREG_OPERATING_MODE_STANDARD		((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_PROGRAMMED	((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_SINGLE		((uint16_t)0x0200)

#define SDRAM_INIT_ADDR                             0xC0000000U
#define SDRAM_BOUNDARY                              0xC0800000U

#include <stddef.h>

#include "sdram.h"
#include "port.h"
#include "stm32f7xx.h"

static void *nextPointer = (void *)SDRAM_INIT_ADDR;

static void RAMSendCommand(uint8_t command, uint16_t mrd, uint8_t nrfs){
	while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY)
		__asm("nop");

	FMC_Bank5_6->SDCMR = (((mrd & 0x1FFF) << 9U) | ((nrfs & 0x0F) << 5U) | FMC_SDCMR_CTB1 | command);
}

void sdram_init(void){
	uint32_t tmpCmd;

	// Activate RAM memory
	// RAM pins: SDKE0: PC3
	//           D2: PD0, D3: PD1, D13: PD8, D14: PD9, D15: PD10, D0: PD14, D1: PD15,
	//           NBL0: PE0, NBL1: PE1, D4~D12: PE7~PE15
	//           A0~A5: PF0~PF5, SDNRAS: PF11, A6~A9: PF12~PF15,
	//           A10: PG0, A11: PG1, BA0: PG4, BA1: PG5, SDCLK: PG8, SDGNCAS: PG15
	//           SDNE0: PH3, SDNWE: PH5
	RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOEEN |
			RCC_AHB1ENR_GPIOFEN | RCC_AHB1ENR_GPIOGEN | RCC_AHB1ENR_GPIOHEN);

	// Setup RAM Pins
	PORT_Reset(GPIOC, EXMEM_RAM_GPIOC_MASK);
	PORT_Reset(GPIOD, EXMEM_RAM_GPIOD_MASK);
	PORT_Reset(GPIOE, EXMEM_RAM_GPIOE_MASK);
	PORT_Reset(GPIOF, EXMEM_RAM_GPIOF_MASK);
	PORT_Reset(GPIOG, EXMEM_RAM_GPIOG_MASK);
	PORT_Reset(GPIOH, EXMEM_RAM_GPIOH_MASK);

	PORT_SetAF(GPIOC, EXMEM_RAM_GPIOC_MASK, 12, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	PORT_SetAF(GPIOD, EXMEM_RAM_GPIOD_MASK, 12, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	PORT_SetAF(GPIOE, EXMEM_RAM_GPIOE_MASK, 12, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	PORT_SetAF(GPIOF, EXMEM_RAM_GPIOF_MASK, 12, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	PORT_SetAF(GPIOG, EXMEM_RAM_GPIOG_MASK, 12, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);
	PORT_SetAF(GPIOH, EXMEM_RAM_GPIOH_MASK, 12, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);

	// Init and config the RAM
	RCC->AHB3ENR |= RCC_AHB3ENR_FMCEN;

	FMC_Bank5_6->SDCR[0] = (FMC_SDCR1_RBURST | (2 << 10U) | (2 << 7U) | FMC_SDCR1_NB | (1 << 4U) | (1 << 2U));
	FMC_Bank5_6->SDTR[0] = ((1 << 24u) | (1 << 20U) | (1 << 16U) | (5 << 12U) | (3 << 8U) | (5 << 4U) | 1);

	RAMSendCommand(1, 0,1);
	RAMSendCommand(2,0,1);
	RAMSendCommand(3,0,8);
	tmpCmd = (uint32_t)(SDRAM_MODEREG_BURST_LENGTH_1 | SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL | SDRAM_MODEREG_CAS_LATENCY_2 |
			SDRAM_MODEREG_OPERATING_MODE_STANDARD | SDRAM_MODEREG_WRITEBURST_MODE_SINGLE);
	RAMSendCommand(4, tmpCmd, 1);

	while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY)
		__asm("nop");

	FMC_Bank5_6->SDRTR |= ((uint32_t)((1292)<< 1));
}

result_t sdram_initArray(void **__p, uint32_t __l){
	if((nextPointer + __l) > (void *)SDRAM_BOUNDARY){
		*__p = NULL;
		return RESULT_ERROR_OUT_OF_BOUNDS;
	}

	*__p = nextPointer;
	nextPointer += __l;
	if((uint32_t)nextPointer & 0x00000003U) // keep RAM pointer memory aligned
		nextPointer = (void *)(((uint32_t)nextPointer & 0xFFFFFFFCU) + 0x00000004U);

	return RESULT_OK;
}

result_t sdram_initMultArray(void **__p, uint32_t __l, uint32_t __c){
	if((nextPointer + (__l * __c)) > (void *)SDRAM_BOUNDARY){
		*__p = NULL;
		return RESULT_ERROR_OUT_OF_BOUNDS;
	}

	for(uint32_t idx = 0; idx < __c; idx++){
		__p[idx] = nextPointer;
		nextPointer += __l;
	}

	if((uint32_t)nextPointer & 0x00000003U) // keep RAM pointer memory aligned
		nextPointer = (void *)(((uint32_t)nextPointer & 0xFFFFFFFCU) + 0x00000004U);

	return RESULT_OK;
}
