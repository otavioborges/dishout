/*
 * cardFormat.c
 *
 *  Created on: Jan. 18, 2021
 *      Author: otavio
 */

#define CARD_FORMAT_DEFAULT_DB_FILE            "cardFormats.cfd"
#define CARD_FORMAT_MAGIC_NUMBER               0xD0D0CACA
#define CARD_FORMAT_DEFAULT_MASTER_KEY         {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}
#define CARD_FORMAT_FIELD_MAX_COUNT            14U

#define CARD_FORMAT_FAT_ASSERT(error, x, file) if(x != FR_OK){ f_close(&file); return error; }
#define CARD_FORMAT_ASSERT_SIZE(x, y, file)    if(x != y){ f_close(&file); return RESULT_ERROR_SIZE_MISMATCH; }

#include "cardFormat.h"
#include "terminal.h"

#include "ff.h"

#include <string.h>

static struct card_file g_fileData = {0, 0, CARD_FORMAT_DEFAULT_MASTER_KEY};
static struct card_format g_formats[CARD_FORMAT_FIELD_MAX_COUNT];
static uint8_t g_isInit = 0;
static uint8_t g_defaultMasterKey[] = CARD_FORMAT_DEFAULT_MASTER_KEY;

static result_t cardFormat_saveStructToDisk(struct card_format *formats){
	FRESULT fatResult;
	FIL cardFile;
	uint32_t dataWritten = 0;
	struct card_field *fields;

	if(g_fileData.uiMagicNumber != CARD_FORMAT_MAGIC_NUMBER)
		return RESULT_ERROR_INVALID_STATE;

	if(!formats)
		return RESULT_ERROR_BAD_PARAMS;

	fatResult = f_open(&cardFile, CARD_FORMAT_DEFAULT_DB_FILE, (FA_WRITE | FA_CREATE_ALWAYS));
	if(fatResult != FR_OK)
		return RESULT_ERROR_EACCESS;

	fatResult = f_write(&cardFile, &g_fileData, sizeof(struct card_file), (UINT *)&dataWritten);
	CARD_FORMAT_FAT_ASSERT(RESULT_ERROR_SD_WRITE, fatResult, cardFile);
	CARD_FORMAT_ASSERT_SIZE(sizeof(struct card_file), dataWritten, cardFile);

	// write down the formats and fields
	for(uint32_t idx = 0; idx < g_fileData.uiNumberOfFormats; idx++){
		fatResult = f_write(&cardFile, (formats + idx), CARD_FORMAT_STRUCT_FORMAT_LENGTH, (UINT *)&dataWritten);
		CARD_FORMAT_FAT_ASSERT(RESULT_ERROR_SD_WRITE, fatResult, cardFile);
		CARD_FORMAT_ASSERT_SIZE(CARD_FORMAT_STRUCT_FORMAT_LENGTH, dataWritten, cardFile);

		// write down the fields
		if(formats[idx].uiNumberOfFields){
			fields = formats[idx].cfFields;
			for(uint32_t f = 0; f < formats[idx].uiNumberOfFields; f++){
				fatResult = f_write(&cardFile, (fields + f), CARD_FORMAT_STRUCT_FIELD_LENGTH, (UINT *)&dataWritten);
				CARD_FORMAT_FAT_ASSERT(RESULT_ERROR_SD_WRITE, fatResult, cardFile);
				CARD_FORMAT_ASSERT_SIZE(CARD_FORMAT_STRUCT_FIELD_LENGTH, dataWritten, cardFile);
			}
		}
	}

	// all data written with success, close and GO!
	f_sync(&cardFile);
	f_close(&cardFile);

	return RESULT_OK;
}

result_t cardFormat_init(void){
	FRESULT fatResult;
	FIL cardFile;
	FILINFO fno;
	uint32_t dataRead;

	// Try opening or creating the card formats save file
	fatResult = f_stat(CARD_FORMAT_DEFAULT_DB_FILE, &fno);
	if(fatResult == FR_NO_FILE){
		// card format doen't exist create new struct from the scratch
		g_fileData.uiMagicNumber = CARD_FORMAT_MAGIC_NUMBER;
		g_fileData.uiNumberOfFormats = 0;
	}else{
		fatResult = f_open(&cardFile, CARD_FORMAT_DEFAULT_DB_FILE, FA_READ);
		if(fatResult != FR_OK)
			return RESULT_ERROR_EACCESS;

		fatResult = f_read(&cardFile, &g_fileData, sizeof(struct card_file), (UINT *)&dataRead);
		CARD_FORMAT_FAT_ASSERT(RESULT_ERROR_SD_READ, fatResult, cardFile);
		CARD_FORMAT_ASSERT_SIZE(sizeof(struct card_file), dataRead, cardFile);

		// check if the file is a valid format
		if((g_fileData.uiMagicNumber != CARD_FORMAT_MAGIC_NUMBER) || (g_fileData.uiNumberOfFormats > CARD_FORMAT_FIELD_MAX_COUNT)){
			f_close(&cardFile);
			f_unlink(CARD_FORMAT_DEFAULT_DB_FILE);

			return RESULT_ERROR_INVALID_STATE;
		}

		// write down the formats and fields
		for(uint32_t idx = 0; idx < g_fileData.uiNumberOfFormats; idx++){
			fatResult = f_read(&cardFile, (g_formats + idx), CARD_FORMAT_STRUCT_FORMAT_LENGTH, (UINT *)&dataRead);
			CARD_FORMAT_FAT_ASSERT(RESULT_ERROR_SD_READ, fatResult, cardFile);
			CARD_FORMAT_ASSERT_SIZE(CARD_FORMAT_STRUCT_FORMAT_LENGTH, dataRead, cardFile);

			// write down the fields
			if(g_formats[idx].uiNumberOfFields){
				g_formats[idx].cfFields = (struct card_field *)pvPortMalloc((CARD_FORMAT_STRUCT_FIELD_LENGTH * g_formats[idx].uiNumberOfFields));
				if(!g_formats[idx].cfFields){
					f_close(&cardFile);
					return RESULT_ERROR_STACK_OVERFLOW;
				}

				for(uint32_t f = 0; f < g_formats[idx].uiNumberOfFields; f++){
					fatResult = f_read(&cardFile, (g_formats[idx].cfFields + f), CARD_FORMAT_STRUCT_FIELD_LENGTH, (UINT *)&dataRead);
					CARD_FORMAT_FAT_ASSERT(RESULT_ERROR_SD_READ, fatResult, cardFile);
					CARD_FORMAT_ASSERT_SIZE(CARD_FORMAT_STRUCT_FIELD_LENGTH, dataRead, cardFile);
				}
			}
		}

		f_close(&cardFile);
	}

	g_isInit = 1;
	return RESULT_OK;
}

int cardFormat_getCount(void){
	if(!g_isInit)
		return RESULT_ERROR_INVALID_STATE;

	return g_fileData.uiNumberOfFormats;
}

int cardFormat_addFormat(const char *name){
	if(!g_isInit)
		return RESULT_ERROR_INVALID_STATE;

	if(g_fileData.uiNumberOfFormats >= CARD_FORMAT_FIELD_MAX_COUNT)
		return RESULT_ERROR_OUT_OF_BOUNDS;

	for(uint32_t idx = 0; idx < g_fileData.uiNumberOfFormats; idx++){
		if(strcmp(g_formats[idx].sName, name) == 0)
			return RESULT_ERROR_EXISTS;
	}

	// good as gold for a new format
	g_formats[g_fileData.uiNumberOfFormats].cfFields = NULL;
	g_formats[g_fileData.uiNumberOfFormats].uiNumberOfFields = 0;
	strcpy(g_formats[g_fileData.uiNumberOfFormats].sName, name);

	return ++g_fileData.uiNumberOfFormats; // don't save without fields
}

int cardFormat_removeFormat(const char *name){
	uint8_t formatFound = 0;

	if(!g_isInit)
		return RESULT_ERROR_INVALID_STATE;

	for(uint32_t idx = 0; idx < g_fileData.uiNumberOfFormats; idx++){
		if(strcmp(g_formats[idx].sName, name) == 0){
			// it's a match
			formatFound = 1;

			if(g_formats[idx].cfFields)
				vPortFree(g_formats[idx].cfFields);

			if(idx == (g_fileData.uiNumberOfFormats - 1)){ // last format, don't bother
				g_fileData.uiNumberOfFormats--;
				return RESULT_OK;
			}
		}

		if(formatFound) // shift the next formats
			copyMemNonBlocking((g_formats + idx), (g_formats + idx + 1), sizeof(struct card_format));
	}

	if(formatFound){
		g_fileData.uiNumberOfFormats--;
		return RESULT_OK;
	}else{
		return RESULT_ERROR_OUT_OF_BOUNDS;
	}
}

int cardFormat_getFormat(const char *name, struct card_format **result){
	if(!g_isInit)
		return RESULT_ERROR_INVALID_STATE;

	for(uint32_t idx = 0; idx < g_fileData.uiNumberOfFormats; idx++){
		if(strcmp(g_formats[idx].sName, name) == 0){
			*result = (g_formats + idx);
			return 0;
		}
	}

	// If we reach this point no format was found
	*result = NULL;
	return RESULT_ERROR_OUT_OF_BOUNDS;
}

char **cardFormat_getFormatNames(uint32_t *count){
	char **result = NULL;

	if(!g_isInit)
		return NULL;

	*count = 0;
	result = (char **)pvPortMalloc((sizeof(char *) * g_fileData.uiNumberOfFormats));
	if(!result)
		return NULL;

	for(uint32_t idx = 0; idx < g_fileData.uiNumberOfFormats; idx++)
		result[idx] = g_formats[idx].sName;

	*count = g_fileData.uiNumberOfFormats;
	return result;
}

int cardFormat_saveFields(const char *formatName, struct card_field *fields, uint32_t count){
	if(!g_isInit)
		return RESULT_ERROR_INVALID_STATE;

	if(!count) // WELL, that was easy
		return RESULT_OK;

	for(uint32_t idx = 0; idx < g_fileData.uiNumberOfFormats; idx++){
		if(strcmp(g_formats[idx].sName, formatName) == 0){
			// desired format to alter
			if(g_formats[idx].cfFields)
				vPortFree(g_formats[idx].cfFields);

			g_formats[idx].cfFields = (struct card_field *)pvPortMalloc((CARD_FORMAT_STRUCT_FIELD_LENGTH * count));
			if(!g_formats[idx].cfFields)
				return RESULT_ERROR_STACK_OVERFLOW;

			copyMemNonBlocking(g_formats[idx].cfFields, fields, (CARD_FORMAT_STRUCT_FIELD_LENGTH * count));
			g_formats[idx].uiNumberOfFields = count;

			return RESULT_OK;
		}
	}

	// no match found
	return RESULT_ERROR_OUT_OF_BOUNDS;
}

int cardFormat_saveModifications(void){
	return cardFormat_saveStructToDisk(g_formats);
}

void cardFormat_deleteDatabase(void){
	// remove the file and clear the memory struct
	f_unlink(CARD_FORMAT_DEFAULT_DB_FILE);

	for(uint32_t idx = 0; idx < g_fileData.uiNumberOfFormats; idx++){
		if(g_formats[idx].cfFields){
			vPortFree(g_formats[idx].cfFields);
			g_formats[idx].cfFields = NULL;
		}
		g_formats[idx].uiNumberOfFields = 0;
		g_formats[idx].sName[0] = '\0';
	}

	g_fileData.uiMagicNumber = CARD_FORMAT_MAGIC_NUMBER;
	copyMemNonBlocking(g_fileData.uiMasterKey, g_defaultMasterKey, 6);
	g_fileData.uiNumberOfFormats = 0;
}

int cardFormat_isField(const struct card_format *format, const char *fieldName){
	if(!format || !fieldName)
		return RESULT_ERROR_BAD_PARAMS;

	// check if format has any field
	if(!format->cfFields || !format->uiNumberOfFields)
		return RESULT_ERROR_OUT_OF_BOUNDS;

	for(uint32_t idx = 0; idx < format->uiNumberOfFields; idx++){
		if(strcmp(fieldName, format->cfFields[idx].sName) == 0)
			return idx;
	}

	// was not found
	return RESULT_ERROR_OUT_OF_BOUNDS;
}

uint8_t *cardFormat_getMasterKey(void){
	return g_fileData.uiMasterKey;
}

int cardFormat_createTrailerConfig(const struct card_field field, rfid_trailer_config_t *config){
	rfid_access_t usedAccessWr, usedAccessRd, usedAccessOp;

	if(!config)
		return RESULT_ERROR_BAD_PARAMS;

	switch(field.caUserAccess){
	case CARD_ACCESS_NONE:
		usedAccessWr = RFID_ACCESS_KEY_A;
		usedAccessRd = RFID_ACCESS_KEY_A;
		usedAccessOp = RFID_ACCESS_KEY_A;
		break;
	case CARD_ACCESS_READ:
		usedAccessWr = RFID_ACCESS_KEY_A;
		usedAccessRd = RFID_ACCESS_BOTH;
		usedAccessOp = RFID_ACCESS_KEY_A;
		break;
	case CARD_ACCESS_WRITE:
		usedAccessWr = RFID_ACCESS_BOTH;
		usedAccessRd = RFID_ACCESS_KEY_A;
		usedAccessOp = RFID_ACCESS_KEY_A;
		break;
	case CARD_ACCESS_BOTH:
		usedAccessWr = RFID_ACCESS_BOTH;
		usedAccessRd = RFID_ACCESS_BOTH;
		usedAccessOp = RFID_ACCESS_KEY_A;
		break;
	case CARD_ACCESS_TRANSFER:
		usedAccessWr = RFID_ACCESS_KEY_A;
		usedAccessRd = RFID_ACCESS_KEY_A;
		usedAccessOp = RFID_ACCESS_BOTH;
		break;
	case CARD_ACCESS_PURSE_ALL:
		usedAccessWr = RFID_ACCESS_BOTH;
		usedAccessRd = RFID_ACCESS_BOTH;
		usedAccessOp = RFID_ACCESS_BOTH;
		break;
	default:
		// Bad access values
		return RESULT_ERROR_INVALID_STATE;
	}

	if(field.cfDataType != CARD_FIELD_TYPE_PURSE)
		usedAccessOp = RFID_ACCESS_NEVER;

	copyMemNonBlocking(config->keyB, g_fileData.uiMasterKey, 6);
	config->trailerWriteKeyA = RFID_ACCESS_KEY_A;
	config->trailerWriteKeyB = RFID_ACCESS_BOTH;
	config->trailerReadKeyB  = RFID_ACCESS_BOTH;

	// config access values
	for(uint8_t block = 0; block < 3; block++){
		config->blockRead[block]      = usedAccessRd;
		config->blockWrite[block]     = usedAccessWr;
		config->blockIncrement[block] = usedAccessOp;
		config->blockDecrement[block] = usedAccessOp;
	}

	return RESULT_OK;
}

int cardFormat_parseValue(const struct card_field field, const void *data, uint8_t *result){
	if(!data || !result)
		return RESULT_ERROR_BAD_PARAMS;

	switch(field.cfDataType){
	case CARD_FIELD_TYPE_UUID:
	case CARD_FIELD_TYPE_TEXT:
		copyMemNonBlocking(result, data, 48U);
		break;
	case CARD_FIELD_TYPE_NUMBER:
	case CARD_FIELD_TYPE_PURSE:
		copyMemNonBlocking(result, data, sizeof(int));
		break;
	default:
		return RESULT_ERROR_INVALID_KEY;
	}

	return RESULT_OK;
}
