/*
 * sd.c
 *
 *  Created on: Dec. 15, 2020
 *      Author: otavio
 */

#define SD_REVERT_32BIT(x)				(((x << 24) & 0xFF000000) | ((x << 8) & 0x00FF0000) | ((x >> 8) & 0x0000FF00) | ((x >> 24) & 0x000000FF))

#define SD_DEFAULT_TIMEOUT				30000
#define SD_BUS_CLK_MHZ					48

#define SD_DMA2_FLAGS_MASK				0x0F400000U
#define SD_GPIOC_MASK					0x00001F00U

//#define SD_ICR_CLEAR_MASK				0x004005FFU
#define SD_ICR_CLEAR_MASK				0x000005FFU
#define SD_ICR_CLEAR_COMMAND_MASK		0x000000C5U
#define SD_ICR_CLEAR_DATA_MASK			0x003FF53AU
#define SD_CMD_MASK						0x0000003FU
#define SD_CMD_FLAGS_MASK				0x00000BC0U
#define SD_CMD_FLAGS_SUSPEND			0x00000800U
#define SD_CMD_FLAGS_WAIT_PENDING		0x00000200U
#define SD_CMD_FLAGS_WAIT_IRQ			0x00000100U
#define SD_CMD_FLAGS_SHORT_RESP			0x00000040U
#define SD_CMD_FLAGS_LONG_RESP			0x000000C0U

#define SD_CMD_GO_IDLE_STATE            ((uint8_t)0U)
#define SD_CMD_SEND_OP_COND             ((uint8_t)1U)  // MMC only
#define SD_CMD_ALL_SEND_CID             ((uint8_t)2U)  // Not supported in SPI mode
#define SD_CMD_SEND_REL_ADDR            ((uint8_t)3U)  // Not supported in SPI mode
#define SD_CMD_SWITCH_FUNC              ((uint8_t)6U)
#define SD_CMD_SEL_DESEL_CARD           ((uint8_t)7U)  // Not supported in SPI mode
#define SD_CMD_HS_SEND_EXT_CSD          ((uint8_t)8U)
#define SD_CMD_SEND_CSD                 ((uint8_t)9U)
#define SD_CMD_SEND_CID                 ((uint8_t)10U)
#define SD_CMD_READ_DAT_UNTIL_STOP      ((uint8_t)11U) // Not supported in SPI mode
#define SD_CMD_STOP_TRANSMISSION        ((uint8_t)12U)
#define SD_CMD_SEND_STATUS              ((uint8_t)13U)
#define SD_CMD_GO_INACTIVE_STATE        ((uint8_t)15U) // Not supported in SPI mode
#define SD_CMD_SET_BLOCKLEN             ((uint8_t)16U)
#define SD_CMD_READ_SINGLE_BLOCK        ((uint8_t)17U)
#define SD_CMD_READ_MULT_BLOCK          ((uint8_t)18U)
#define SD_CMD_WRITE_DAT_UNTIL_STOP     ((uint8_t)20U) // Not supported in SPI mode
#define SD_CMD_WRITE_BLOCK              ((uint8_t)24U)
#define SD_CMD_WRITE_MULTIPLE_BLOCK     ((uint8_t)25U)
#define SD_CMD_PROG_CSD                 ((uint8_t)27U)
#define SD_CMD_SET_WRITE_PROT           ((uint8_t)28U) // Not supported in SPI mode
#define SD_CMD_CLR_WRITE_PROT           ((uint8_t)29U) // Not supported in SPI mode
#define SD_CMD_SEND_WRITE_PROT          ((uint8_t)30U) // Not supported in SPI mode
#define SD_CMD_ERASE                    ((uint8_t)38U)
#define SD_CMD_LOCK_UNLOCK              ((uint8_t)42U)
#define SD_CMD_APP_CMD                  ((uint8_t)55U)
#define SD_CMD_READ_OCR                 ((uint8_t)58U) // Read OCR register
#define SD_CMD_CRC_ON_OFF               ((uint8_t)59U) // On/Off CRC check by SD Card (in SPI mode)

// Following commands are SD Card Specific commands.
// SD_CMD_APP_CMD should be sent before sending these commands.
#define SD_CMD_SET_BUS_WIDTH            ((uint8_t)6U)  // ACMD6
#define SD_CMD_SD_SEND_OP_COND          ((uint8_t)41U) // ACMD41
#define SD_CMD_SET_CLR_CARD_DETECT      ((uint8_t)42U) // ACMD42
#define SD_CMD_SEND_SCR                 ((uint8_t)51U) // ACMD51

#define SD_CMD_R1_IDLE_STATE			0x01
#define SD_CMD_R1_ERASE_RESET			0x02
#define SD_CMD_R1_ILLEGAL_CMD			0x04
#define SD_CMD_R1_CRC_ERROR				0x08
#define SD_CMD_R1_ERASE_SEQ_ERROR		0x10
#define SD_CMD_R1_ADDR_ERROR			0x20
#define SD_CMD_R1_PARAM_ERROR			0x40

#define SD_R3_IS_IDLE_STATE				(uint32_t)(1 << 31U)
#define SD_HIGH_CAPACITY				(uint32_t)0x40000000U
#define SD_OCR_VOLTAGE					(uint32_t)0x80100000U

#define SD_RESP_ERROR_MASK				(uint32_t)0xFFF98008U
#define SD_RESP_OUT_OF_RANGE			(uint32_t)(1 << 31U)
#define SD_RESP_ADDRESS_ERROR			(uint32_t)(1 << 30U)
#define SD_RESP_BLOCK_LEN_ERROR			(uint32_t)(1 << 29U)
#define SD_RESP_ERASE_SEQ_ERROR			(uint32_t)(1 << 28U)
#define SD_RESP_ERASE_PARAM				(uint32_t)(1 << 27U)
#define SD_RESP_WP_VIOLATION			(uint32_t)(1 << 26U)
#define SD_RESP_CARD_LOCKED				(uint32_t)(1 << 25U)
#define SD_RESP_LOCK_UNL_FAILED			(uint32_t)(1 << 24U)
#define SD_RESP_COM_CRC_ERROR			(uint32_t)(1 << 23U)
#define SD_RESP_ILLEGAL_COMMAND			(uint32_t)(1 << 22U)
#define SD_RESP_CARD_ECC_ERROR			(uint32_t)(1 << 21U)
#define SD_RESP_CC_ERROR				(uint32_t)(1 << 20U)
#define SD_RESP_ERROR					(uint32_t)(1 << 19U)
#define SD_RESP_CSD_OVERWRITE			(uint32_t)(1 << 16U)
#define SD_RESP_WP_ERASE_SKIP			(uint32_t)(1 << 15U)
#define SD_RESP_CARD_ECC_DISABLED		(uint32_t)(1 << 14U)
#define SD_RESP_ERASE_RESET				(uint32_t)(1 << 13U)
#define SD_RESP_CURRENT_STATE			(uint32_t)(0x0F << 9U)
#define SD_RESP_READY_FOR_DATA			(uint32_t)(1 << 8U)
#define SD_RESP_APP_CMD					(uint32_t)(1 << 5U)
#define SD_RESP_AKE_SEQ_ERROR			(uint32_t)(1 << 3U)

// SCR Values
#define SDMMC_WIDE_BUS_SUPPORT			0x00040000U
#define SDMMC_SINGLE_BUS_SUPPORT		0x00010000U
#define SDMMC_CARD_LOCKED				0x02000000U

#include <stddef.h>
#include <string.h>

#include "stm32f7xx.h"
#include "port.h"
#include "gpio.h"

#include "sd.h"

#include "diskio.h"

#include "FreeRTOS.h"
#include "task.h"

static sd_card_t g_currentCard = {0,0,0,SD_TYPE_SDHC};

static result_t SendCommand(uint8_t cmd, uint32_t arg, uint32_t flags, uint32_t waitFlags){
	uint8_t retries = 100;
	// Wait for unit to be free
//	if(UTIL_WaitOrTimeout(SD_DEFAULT_TIMEOUT, &(SDMMC1->STA), (SDMMC_STA_TXACT | SDMMC_STA_RXACT | SDMMC_STA_CMDACT), 1) != DELAY_WAIT_OK)
//		return RESULT_BUSY;

	while(retries-- > 0){
		SDMMC1->ICR = SD_ICR_CLEAR_COMMAND_MASK; // clear flags
		SDMMC1->ARG = arg;
		SDMMC1->CMD = ((flags & SD_CMD_FLAGS_MASK) | (cmd & SD_CMD_MASK) | SDMMC_CMD_CPSMEN);

		if(waitFlags){
			if(waitUntilFlag(&(SDMMC1->STA), (waitFlags | SDMMC_STA_CTIMEOUT), SD_DEFAULT_TIMEOUT, 0) != RESULT_OK){
				return RESULT_ERROR_TIMEOUT;
			}else if((SDMMC1->STA & SDMMC_STA_CTIMEOUT) == 0){
				return RESULT_OK;
			}

			// Command timeout, retry
			delay(10);
			SDMMC1->CMD = 0;
			SDMMC1->ICR = SD_ICR_CLEAR_COMMAND_MASK;
		}else{
			return RESULT_OK;
		}
	}

	return RESULT_ERROR_INVALID_STATE;
}

static result_t getR1_7Response(uint8_t cmd, uint32_t *response){
	uint32_t flags = SDMMC1->STA;

	if(flags & (SDMMC_STA_DTIMEOUT | SDMMC_STA_CTIMEOUT))
		return RESULT_ERROR_TIMEOUT;

	if(flags & SDMMC_STA_CCRCFAIL)
		return RESULT_ERROR_CRC_FAIL;

	if(SDMMC1->RESPCMD != (cmd & SD_CMD_MASK))
		return RESULT_ERROR_INVALID_STATE;

	SDMMC1->ICR = SD_ICR_CLEAR_COMMAND_MASK;

	if(response)
		*response = SDMMC1->RESP1;

	return RESULT_OK;
}

static result_t getR2Response(uint32_t *response, uint8_t revertBits){
	uint32_t flags = SDMMC1->STA;

	if(flags & (SDMMC_STA_DTIMEOUT | SDMMC_STA_CTIMEOUT))
		return RESULT_ERROR_TIMEOUT;

	if(flags & SDMMC_STA_CCRCFAIL)
		return RESULT_ERROR_CRC_FAIL;

	SDMMC1->ICR = SD_ICR_CLEAR_COMMAND_MASK;

	if(revertBits){
		response[0] = SD_REVERT_32BIT(SDMMC1->RESP1);
		response[1] = SD_REVERT_32BIT(SDMMC1->RESP2);
		response[2] = SD_REVERT_32BIT(SDMMC1->RESP3);
		response[3] = SD_REVERT_32BIT(SDMMC1->RESP4);
	}else{
		response[0] = SDMMC1->RESP1;
		response[1] = SDMMC1->RESP2;
		response[2] = SDMMC1->RESP3;
		response[3] = SDMMC1->RESP4;
	}
	return RESULT_OK;
}



static result_t SendApplicationCommand(uint8_t cmd, uint32_t argAPP, uint32_t argCMD, uint32_t *result){
	uint32_t cmdFlags;
	result_t opResult;

	opResult = SendCommand(SD_CMD_APP_CMD, argAPP, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
	ASSERT_RESULT(opResult);

	opResult = getR1_7Response(SD_CMD_APP_CMD, &cmdFlags);
	ASSERT_RESULT(opResult);

	if(cmdFlags & SD_RESP_ERROR_MASK)
		return RESULT_ERROR_BAD_PARAMS;

	if((cmdFlags & SD_RESP_APP_CMD) == 0)
		return RESULT_ERROR_INVALID_STATE;

	// No error on APP mode
	if(result != NULL){
		if(cmd == SD_CMD_SD_SEND_OP_COND){ // If R3 is the response ignore CRC
			opResult = SendCommand(cmd, argCMD, SD_CMD_FLAGS_SHORT_RESP, (SDMMC_STA_CMDREND | SDMMC_STA_CCRCFAIL));

			ASSERT_RESULT(opResult);
			*result = SDMMC1->RESP1;
			SDMMC1->ICR = SD_ICR_CLEAR_MASK;
			return RESULT_OK;
		}else{
			opResult = SendCommand(cmd, argCMD, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);

			ASSERT_RESULT(opResult);
			return getR1_7Response(cmd, result);
		}
	}else{
		return SendCommand(cmd, argCMD, 0, SDMMC_STA_CMDSENT);
	}
}

static void SD_GetCardInfo(void) {
	uint32_t dev_size;
	uint32_t dev_size_mul;

	// Parse the CSD register
	g_currentCard.CSDVer = g_currentCard.CSD[0] >> 6; // CSD version
	if (g_currentCard.type != SD_TYPE_MMC) {
		// SD
		g_currentCard.MaxBusClkFreq = g_currentCard.CSD[3];
		if (g_currentCard.CSDVer == 0) {
			// CSD v1.00 (SDSCv1, SDSCv2)
			dev_size  = (uint32_t)(g_currentCard.CSD[6] & 0x03) << 10; // Device size
			dev_size |= (uint32_t)g_currentCard.CSD[7] << 2;
			dev_size |= (g_currentCard.CSD[8] & 0xc0) >> 6;
			dev_size_mul  = (g_currentCard.CSD[ 9] & 0x03) << 1; // Device size multiplier
			dev_size_mul |= (g_currentCard.CSD[10] & 0x80) >> 7;
			g_currentCard.BlockCount  = dev_size + 1;
			g_currentCard.BlockCount *= 1 << (dev_size_mul + 2);
			g_currentCard.BlockSize   = 1 << (g_currentCard.CSD[5] & 0x0f); // Maximum read data block length
		} else {
			// CSD v2.00 (SDHC, SDXC)
			dev_size  = (g_currentCard.CSD[7] & 0x3f) << 16;
			dev_size |=  g_currentCard.CSD[8] << 8;
			dev_size |=  g_currentCard.CSD[9]; // C_SIZE
			g_currentCard.BlockSize = SD_CARD_DEFAULT_SECTOR_SIZE;
			g_currentCard.BlockCount = dev_size + 1;
			// BlockCount >= 65535 means that this is SDXC card
		}
		g_currentCard.capacity = g_currentCard.BlockCount * g_currentCard.BlockSize;
	} else {
		// MMC
		g_currentCard.MaxBusClkFreq = g_currentCard.CSD[3];
		dev_size  = (uint32_t)(g_currentCard.CSD[6] & 0x03) << 8; // C_SIZE
		dev_size += (uint32_t)g_currentCard.CSD[7];
		dev_size <<= 2;
		dev_size += g_currentCard.CSD[8] >> 6;
		g_currentCard.BlockSize = 1 << (g_currentCard.CSD[5] & 0x0f); // MMC read block length
		dev_size_mul = ((g_currentCard.CSD[9] & 0x03) << 1) + ((g_currentCard.CSD[10] & 0x80) >> 7);
		g_currentCard.BlockCount = (dev_size + 1) * (1 << (dev_size_mul + 2));
		g_currentCard.capacity = g_currentCard.BlockCount * g_currentCard.BlockSize;
	}

	// Parse the CID register
	if (g_currentCard.type != SD_TYPE_MMC) {
		// SD card
		g_currentCard.MID = g_currentCard.CID[0];
		g_currentCard.OID = (g_currentCard.CID[1] << 8) | g_currentCard.CID[2];
		g_currentCard.PNM[0] = g_currentCard.CID[3];
		g_currentCard.PNM[1] = g_currentCard.CID[4];
		g_currentCard.PNM[2] = g_currentCard.CID[5];
		g_currentCard.PNM[3] = g_currentCard.CID[6];
		g_currentCard.PNM[4] = g_currentCard.CID[7];
		g_currentCard.PRV = g_currentCard.CID[8];
		g_currentCard.PSN = (g_currentCard.CID[9] << 24) | (g_currentCard.CID[10] << 16) | (g_currentCard.CID[11] << 8) | g_currentCard.CID[12];
		g_currentCard.MDT = ((g_currentCard.CID[13] << 8) | g_currentCard.CID[14]) & 0x0fff;
	} else {
		// MMC
		g_currentCard.MID = 0x00;
		g_currentCard.OID = 0x0000;
		g_currentCard.PNM[0] = '*';
		g_currentCard.PNM[1] = 'M';
		g_currentCard.PNM[2] = 'M';
		g_currentCard.PNM[3] = 'C';
		g_currentCard.PNM[4] = '*';
		g_currentCard.PRV = 0;
		g_currentCard.PSN = 0x00000000;
		g_currentCard.MDT = 0x0000;
	}
}

static result_t GetSCR(uint32_t *SCR){
	result_t opResult;
	uint32_t cmdResponse;
	uint32_t tempValues[2];
	uint8_t tempIdx = 0;

	opResult = SendCommand(SD_CMD_SET_BLOCKLEN, 8, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
	ASSERT_RESULT(opResult);
	opResult = getR1_7Response(SD_CMD_SET_BLOCKLEN, &cmdResponse);
	ASSERT_RESULT(opResult);

	if(cmdResponse & SD_RESP_BLOCK_LEN_ERROR)
		return RESULT_ERROR_INVALID_STATE;

	opResult = SendCommand(SD_CMD_APP_CMD, (((uint32_t)g_currentCard.RCA) << 16), SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
	ASSERT_RESULT(opResult);

	opResult = getR1_7Response(SD_CMD_APP_CMD, &cmdResponse);
	ASSERT_RESULT(opResult);

	if(cmdResponse & SD_RESP_ERROR_MASK)
		return RESULT_ERROR_BAD_PARAMS;

	if((cmdResponse & SD_RESP_APP_CMD) == 0)
		return RESULT_ERROR_BAD_PARAMS;

	SDMMC1->ICR = SD_ICR_CLEAR_MASK;

	SDMMC1->DTIMER = 0xFFFFFFFFU;//24000000U; // Set timeout for 500ms in 48MHz
	SDMMC1->DLEN   = 8; // Data length in bytes
	SDMMC1->DCTRL  = ((3 << SDMMC_DCTRL_DBLOCKSIZE_Pos) | SDMMC_DCTRL_DTDIR | SDMMC_DCTRL_DTEN);

	opResult = SendCommand(SD_CMD_SEND_SCR, 0, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
	ASSERT_RESULT(opResult);

	opResult = getR1_7Response(SD_CMD_SEND_SCR, &cmdResponse);
	ASSERT_RESULT(opResult);

	if(cmdResponse & SD_RESP_ERROR_MASK)
		return RESULT_ERROR_BAD_PARAMS;

	while ((SDMMC1->STA & (SDMMC_STA_RXOVERR | SDMMC_STA_DCRCFAIL | SDMMC_STA_DTIMEOUT | SDMMC_STA_DBCKEND | SDMMC_STA_DATAEND)) == 0) {
		if(SDMMC1->STA & SDMMC_STA_RXDAVL)
			tempValues[tempIdx++] = SDMMC1->FIFO;
	}

	if(SDMMC1->STA & (SDMMC_STA_RXOVERR | SDMMC_STA_DCRCFAIL | SDMMC_STA_DTIMEOUT)){
		SDMMC1->ICR = SD_ICR_CLEAR_MASK;
		return RESULT_ERROR_BAD_PARAMS;
	}

	SCR[0] = SD_REVERT_32BIT(tempValues[1]);
	SCR[1] = SD_REVERT_32BIT(tempValues[0]);

	SDMMC1->ICR = 0xFFFFFFFF;
	return RESULT_OK;
}

static result_t SendStopCommand(void){
	result_t opResult = SendCommand(SD_CMD_STOP_TRANSMISSION, 0, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
	ASSERT_RESULT(opResult);
	return getR1_7Response(SD_CMD_STOP_TRANSMISSION, NULL);
}

static result_t SendSwitchCommand(uint32_t arg, uint8_t *resp){
	uint32_t tempData, dataLeft = 64;
	result_t opResult;

	if(resp){
		SDMMC1->ICR = 0xFFFFFFFFU;
		SDMMC1->DTIMER = 0xFFFFFFFFU;
		SDMMC1->DLEN = 64;
		SDMMC1->DCTRL = ((6 << SDMMC_DCTRL_DBLOCKSIZE_Pos) | SDMMC_DCTRL_DTDIR | SDMMC_DCTRL_DTEN);
	}

	opResult = SendCommand(SD_CMD_SWITCH_FUNC, arg, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
	ASSERT_RESULT(opResult);
	opResult = getR1_7Response(SD_CMD_SWITCH_FUNC, &tempData);
	ASSERT_RESULT(opResult);
	if(tempData & SD_RESP_ERROR_MASK)
		return RESULT_ERROR_BAD_PARAMS;

	if(resp){
		while((SDMMC1->STA & (SDMMC_STA_RXOVERR | SDMMC_STA_DCRCFAIL | SDMMC_STA_DTIMEOUT | SDMMC_STA_DATAEND)) == 0){
			if((SDMMC1->STA & SDMMC_STA_RXFIFOHF) && (dataLeft > 0U)){
				tempData = SDMMC1->FIFO;

				*resp++ = (uint8_t)tempData;
				*resp++ = (uint8_t)(tempData >> 8);
				*resp++ = (uint8_t)(tempData >> 16);
				*resp++ = (uint8_t)(tempData >> 24);

				dataLeft -= 4;
			}
		}
	}

	tempData = SDMMC1->STA;
	SDMMC1->ICR = 0xFFFFFFFFU;
	if(tempData & (SDMMC_STA_RXOVERR | SDMMC_STA_DCRCFAIL | SDMMC_STA_DTIMEOUT))
		return RESULT_ERROR_BAD_PARAMS;
	else
		return RESULT_OK;
}

result_t SD_Init(void){
	// MMC Pins: D0: PC8, D1: PC9, D2: PC10, D3: PC11, CLK: PC12, DETECT: PC13, CMD: PD2
	RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN);

	PORT_Clear(GPIOC, SD_GPIOC_MASK);
	GPIO_Clear(GPIOD, 2);

	PORT_SetAF(GPIOC, SD_GPIOC_MASK, 12, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_PULL_UP);
	GPIO_SetInput(GPIOC, 13, GPIO_SPEED_MEDIUM, GPIO_PULL_NO_PULL);	// Detect is HIGH if no card is detected
	GPIO_SetAF(GPIOD, 2, 12, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_PULL_UP);

	RCC->APB2ENR |= RCC_APB2ENR_SDMMC1EN;

	RCC->APB2RSTR |= RCC_APB2RSTR_SDMMC1RST;
	__asm("nop");
	RCC->APB2RSTR &= ~RCC_APB2RSTR_SDMMC1RST;

	SDMMC1->CLKCR = (SDMMC_CLKCR_CLKEN | 118U); // initiate at 400kHz
	SDMMC1->POWER = 0x3;

	// Init DMA bus clock
	// DMA2, Stream 3, Channel 4
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	DMA2->LIFCR = SD_DMA2_FLAGS_MASK;

	return RESULT_OK;
}

result_t SD_Config(void){
	result_t opResult;
	uint32_t cmdResponse[4];
	uint8_t switchResponse[64];
	uint8_t busDiv;

	if((SDMMC1->POWER & SDMMC_POWER_PWRCTRL) != 0x03)
		return RESULT_ERROR_INVALID_STATE;

	opResult = SendCommand(SD_CMD_GO_IDLE_STATE, 0, 0, SDMMC_STA_CMDSENT);
	ASSERT_RESULT(opResult);

	opResult = SendCommand(SD_CMD_HS_SEND_EXT_CSD, 0x1AA, SD_CMD_FLAGS_SHORT_RESP, (SDMMC_STA_CCRCFAIL | SDMMC_STA_CMDREND));
	if(opResult == RESULT_OK){
		// SD >=v2.0

		opResult = getR1_7Response(SD_CMD_HS_SEND_EXT_CSD, cmdResponse);
		ASSERT_RESULT(opResult);

		if((cmdResponse[0] & 0x1FF) != 0x1AA)
			return RESULT_ERROR_BAD_PARAMS;

		do{
			opResult = SendApplicationCommand(SD_CMD_SD_SEND_OP_COND, 0, (SD_OCR_VOLTAGE | SD_HIGH_CAPACITY), cmdResponse); // decode this argument later
			ASSERT_RESULT(opResult);
		}while((cmdResponse[0] & SD_R3_IS_IDLE_STATE) == 0);

		if(cmdResponse[0] & SD_HIGH_CAPACITY)
			g_currentCard.type = SD_TYPE_SDHC;
		else
			g_currentCard.type = SD_TYPE_SD_V2;
	}else{
		// Do codes for V1/MMC cards
		return RESULT_ERROR_INVALID_STATE;
	}

	opResult = SendCommand(SD_CMD_ALL_SEND_CID, 0, SD_CMD_FLAGS_LONG_RESP, SDMMC_STA_CMDREND);
	ASSERT_RESULT(opResult);

	opResult = getR2Response(g_currentCard.CID, 0);
	ASSERT_RESULT(opResult);

	if(g_currentCard.type != SD_TYPE_MMC){
		opResult = SendCommand(SD_CMD_SEND_REL_ADDR, 0, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
		ASSERT_RESULT(opResult);

		opResult = getR1_7Response(SD_CMD_SEND_REL_ADDR, cmdResponse);
		ASSERT_RESULT(opResult);

		g_currentCard.RCA = (uint16_t)(cmdResponse[0] >> 16);
	}

	opResult = SendCommand(SD_CMD_SEND_CSD, (((uint32_t)g_currentCard.RCA) << 16), SD_CMD_FLAGS_LONG_RESP, SDMMC_STA_CMDREND);
	ASSERT_RESULT(opResult);

	opResult = getR2Response((uint32_t *)&(g_currentCard.CSD), 1);
	ASSERT_RESULT(opResult);

	SD_GetCardInfo();

	opResult = SendCommand(SD_CMD_SEL_DESEL_CARD, (((uint32_t)g_currentCard.RCA) << 16), SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
	ASSERT_RESULT(opResult);
	opResult = getR1_7Response(SD_CMD_SEL_DESEL_CARD, cmdResponse);
	ASSERT_RESULT(opResult);

	if(((cmdResponse[0] & SD_RESP_CURRENT_STATE) >> 9) != 0x03)
		return RESULT_ERROR_INVALID_STATE;

	//	opResult = SendApplicationCommand(SD_CMD_SET_CLR_CARD_DETECT, (((uint32_t)g_currentCard.RCA) << 16), 0, cmdResponse);
	//	ASSERT_RESULT(opResult);
	//
	//	// Check if in Transmit mode
	//	if(((cmdResponse[0] & SD_RESP_CURRENT_STATE) >> 9) != 0x04)
	//		return RESULT_INVALID_STATE;

	if(g_currentCard.type != SD_TYPE_MMC){
		opResult = GetSCR((uint32_t *)&(g_currentCard.SCR));
	}

	if(g_currentCard.SCR[1] & SDMMC_WIDE_BUS_SUPPORT){
		// Set up 4-bit bus
		opResult = SendApplicationCommand(SD_CMD_SET_BUS_WIDTH, (((uint32_t)g_currentCard.RCA) << 16), 2, cmdResponse);
		ASSERT_RESULT(opResult);
		if(cmdResponse[0] & SD_RESP_ERROR_MASK)
			return RESULT_ERROR_INVALID_STATE;

		SDMMC1->CLKCR |= (1 << SDMMC_CLKCR_WIDBUS_Pos);
	}

	if((g_currentCard.type != SD_TYPE_SD_V1) | (g_currentCard.type != SD_TYPE_SD_V2) | (g_currentCard.type != SD_TYPE_MMC)){
		opResult = SendCommand(SD_CMD_SET_BLOCKLEN, SD_CARD_DEFAULT_SECTOR_SIZE, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
		ASSERT_RESULT(opResult);
		opResult = getR1_7Response(SD_CMD_SET_BLOCKLEN, NULL);
	}

	// Try to set the SDMMC in high speed
	//	memset(switchResponse, 0xAA, 64);
	//	opResult = SendSwitchCommand(0xFFFFF1, switchResponse);
	//	ASSERT_RESULT(opResult);
	//
	//	if(switchResponse[13] & 0x01){
	//		opResult = SendSwitchCommand(0x80FFFFF0, NULL);
	//		ASSERT_RESULT(opResult);
	//
	//		Delay(1);
	//	}

	// Not using max bus speed to avoid bad bits, divide supported bus speed by 2
	if(g_currentCard.MaxBusClkFreq >= 48){
		cmdResponse[0] = SDMMC1->CLKCR;
		cmdResponse[0] &= ~SDMMC_CLKCR_CLKDIV;
		cmdResponse[0] |= 50;
		SDMMC1->CLKCR = cmdResponse[0];
	}else{
		cmdResponse[0] = SDMMC1->CLKCR;
		cmdResponse[0] &= ~SDMMC_CLKCR_CLKDIV;
		cmdResponse[0] |= (((SD_BUS_CLK_MHZ / g_currentCard.MaxBusClkFreq) - 1) & 0xFF); // avoid remainder giving higher bus freq
		SDMMC1->CLKCR = cmdResponse[0];
	}

	g_currentCard.cardFlags = SD_CARD_FLAGS_INIT;
	return opResult;
}

result_t SD_IsCardPresent(void){
	if(GPIOC->IDR & 0x2000){	// HIGH means no card
		g_currentCard.cardFlags = 0;
		return RESULT_ERROR_INVALID_STATE;
	}else{
		g_currentCard.cardFlags |= SD_CARD_FLAGS_PRESENT;
		return RESULT_OK;
	}
}

result_t SD_Read(uint32_t address, uint8_t *buffer, uint32_t size){
	uint32_t blockCount = size >> 9;
	uint32_t cmdResponse, dataResult, dataLeft = size;
	result_t opResult;

	if(waitUntilFlag(&(SDMMC1->STA), (SDMMC_STA_TXACT | SDMMC_STA_RXACT | SDMMC_STA_CMDACT), SD_DEFAULT_TIMEOUT, 1) != RESULT_OK)
		return RESULT_ERROR_BUSY;

	if((g_currentCard.cardFlags & SD_CARD_FLAGS_INIT) == 0)
		return RESULT_ERROR_INVALID_STATE;

	if(address + size > g_currentCard.capacity)
		return RESULT_ERROR_OUT_OF_BOUNDS;

	if(!buffer)
		return RESULT_ERROR_INVALID_STATE;

	SDMMC1->DCTRL = 0;
	SDMMC1->ICR = SD_ICR_CLEAR_MASK;
	if(g_currentCard.type == SD_TYPE_SDHC)
		address >>= 9;

	opResult = SendCommand(SD_CMD_SET_BLOCKLEN, SD_CARD_DEFAULT_SECTOR_SIZE, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
	ASSERT_RESULT(opResult);
	opResult = getR1_7Response(SD_CMD_SET_BLOCKLEN, &cmdResponse);
	ASSERT_RESULT(opResult);

	if(cmdResponse & SD_RESP_BLOCK_LEN_ERROR)
		return RESULT_ERROR_INVALID_STATE;

	// Config DMA
	DMA2_Stream3->CR = ((4 << DMA_SxCR_CHSEL_Pos) | DMA_SxCR_MBURST_0 | DMA_SxCR_PBURST_0 | DMA_SxCR_PL | (0x02 << DMA_SxCR_MSIZE_Pos) | (0x02 << DMA_SxCR_PSIZE_Pos) | DMA_SxCR_MINC | DMA_SxCR_PFCTRL);
	DMA2_Stream3->FCR = (DMA_SxFCR_DMDIS | 0x03);
	DMA2_Stream3->NDTR = size >> 2;
	DMA2_Stream3->PAR = (uint32_t)&(SDMMC1->FIFO);
	DMA2_Stream3->M0AR = (uint32_t)buffer;

	DMA2->LIFCR = SD_DMA2_FLAGS_MASK;
	DMA2_Stream3->CR |= DMA_SxCR_EN;

	// We are GOOD....TO....GO!
	SDMMC1->DTIMER = 0xFFFFFFFFU;
	SDMMC1->DLEN = size;
	SDMMC1->DCTRL = ((9 << SDMMC_DCTRL_DBLOCKSIZE_Pos) | SDMMC_DCTRL_DTDIR | SDMMC_DCTRL_DMAEN | SDMMC_DCTRL_DTEN);

	if(blockCount > 1){
		opResult = SendCommand(SD_CMD_READ_MULT_BLOCK, address, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
		ASSERT_RESULT(opResult);
		opResult = getR1_7Response(SD_CMD_READ_MULT_BLOCK, &cmdResponse);
		ASSERT_RESULT(opResult);
	}else{
		opResult = SendCommand(SD_CMD_READ_SINGLE_BLOCK, address, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
		ASSERT_RESULT(opResult);
		opResult = getR1_7Response(SD_CMD_READ_SINGLE_BLOCK, &cmdResponse);
		ASSERT_RESULT(opResult);
	}

	if(((cmdResponse & SD_RESP_READY_FOR_DATA) == 0) || (cmdResponse & SD_RESP_ERROR_MASK))
		return RESULT_ERROR_INVALID_STATE;

	if(((cmdResponse & SD_RESP_CURRENT_STATE) >> 9) != 0x04)
		return RESULT_ERROR_INVALID_STATE;	// not READY!

	while((SDMMC1->STA & (SDMMC_STA_RXOVERR | SDMMC_STA_DCRCFAIL | SDMMC_STA_DTIMEOUT | SDMMC_STA_DATAEND)) == 0){
		if(DMA2->LISR & (DMA_LISR_TCIF3 | DMA_LISR_TEIF3 | DMA_LISR_DMEIF3 | DMA_LISR_FEIF3))
			break;
	}

	cmdResponse = SDMMC1->STA;

	if(blockCount > 1){
		opResult = SendStopCommand();
		ASSERT_RESULT(opResult);
	}

	if(DMA2->LISR & (DMA_LISR_TEIF3 | DMA_LISR_DMEIF3 | DMA_LISR_FEIF3))
		return RESULT_ERROR_INVALID_STATE;

	SDMMC1->ICR = 0xFFFFFFFFU;
	if(cmdResponse & (SDMMC_STA_RXOVERR | SDMMC_STA_DCRCFAIL | SDMMC_STA_DTIMEOUT))
		return RESULT_ERROR_INVALID_STATE;
	else
		return RESULT_OK;
}

result_t SD_Write(uint32_t address, const uint8_t *buffer, uint32_t size){
	uint32_t blockCount = size >> 9;
	uint32_t cmdResponse, dataResult, dataLeft = size;
	result_t opResult;

	if(waitUntilFlag(&(SDMMC1->STA), (SDMMC_STA_TXACT | SDMMC_STA_RXACT | SDMMC_STA_CMDACT), SD_DEFAULT_TIMEOUT, 1) != RESULT_OK)
		return RESULT_ERROR_BUSY;

	if((g_currentCard.cardFlags & SD_CARD_FLAGS_INIT) == 0)
		return RESULT_ERROR_INVALID_STATE;

	if(address + size > g_currentCard.capacity)
		return RESULT_ERROR_OUT_OF_BOUNDS;

	if(size & 0x1FF)
		return RESULT_ERROR_INVALID_STATE;

	if(!buffer)
		return RESULT_ERROR_INVALID_STATE;

	SDMMC1->DCTRL = 0;
	SDMMC1->ICR = (SD_ICR_CLEAR_DATA_MASK | SD_ICR_CLEAR_COMMAND_MASK);
	if(g_currentCard.type == SD_TYPE_SDHC)
		address >>= 9;

	opResult = SendCommand(SD_CMD_SET_BLOCKLEN, SD_CARD_DEFAULT_SECTOR_SIZE, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
	ASSERT_RESULT(opResult);
	opResult = getR1_7Response(SD_CMD_SET_BLOCKLEN, &cmdResponse);
	ASSERT_RESULT(opResult);

	if(cmdResponse & SD_RESP_BLOCK_LEN_ERROR)
		return RESULT_ERROR_INVALID_STATE;

	// Config DMA
	//	__DSB();
	//	UTIL_IRQPause();
	//	DMA2_Stream3->CR = ((4 << DMA_SxCR_CHSEL_Pos) | DMA_SxCR_MBURST_0 | DMA_SxCR_PBURST_0 | (0x02 << DMA_SxCR_MSIZE_Pos) | (0x02 << DMA_SxCR_PSIZE_Pos) | DMA_SxCR_MINC | (1 << DMA_SxCR_DIR_Pos) | DMA_SxCR_PL | DMA_SxCR_PFCTRL);
	//	DMA2_Stream3->FCR = (DMA_SxFCR_DMDIS | 0x03);
	//	DMA2_Stream3->NDTR = size >> 2;
	//	DMA2_Stream3->PAR = (uint32_t)&(SDMMC1->FIFO);
	//	DMA2_Stream3->M0AR = (uint32_t)buffer;
	//
	//	DMA2->LIFCR = SD_DMA2_FLAGS_MASK;
	//	DMA2_Stream3->CR |= DMA_SxCR_EN;

	// We are GOOD....TO....GO!
	taskENTER_CRITICAL();
	SDMMC1->DTIMER = 0xFFFFFFFFU;
	SDMMC1->DLEN = size;
	//	SDMMC1->DCTRL = ((9 << SDMMC_DCTRL_DBLOCKSIZE_Pos) | SDMMC_DCTRL_DMAEN | SDMMC_DCTRL_DTEN);
	SDMMC1->DCTRL = ((9 << SDMMC_DCTRL_DBLOCKSIZE_Pos) | SDMMC_DCTRL_DTEN);

	if(blockCount > 1){
		opResult = SendCommand(SD_CMD_WRITE_MULTIPLE_BLOCK, address, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
		ASSERT_RESULT(opResult);
		opResult = getR1_7Response(SD_CMD_WRITE_MULTIPLE_BLOCK, &cmdResponse);
		ASSERT_RESULT(opResult);
	}else{
		opResult = SendCommand(SD_CMD_WRITE_BLOCK, address, SD_CMD_FLAGS_SHORT_RESP, SDMMC_STA_CMDREND);
		ASSERT_RESULT(opResult);
		opResult = getR1_7Response(SD_CMD_WRITE_BLOCK, &cmdResponse);
		ASSERT_RESULT(opResult);
	}

	if(((cmdResponse & SD_RESP_READY_FOR_DATA) == 0) || (cmdResponse & SD_RESP_ERROR_MASK))
		return RESULT_ERROR_INVALID_STATE;

	if(((cmdResponse & SD_RESP_CURRENT_STATE) >> 9) != 0x04)
		return RESULT_ERROR_INVALID_STATE;	// not READY!

	uint32_t bIdx = 0;
	while((SDMMC1->STA & (SDMMC_STA_TXUNDERR | SDMMC_STA_DCRCFAIL | SDMMC_STA_DTIMEOUT | SDMMC_STA_DATAEND)) == 0){
		if((SDMMC1->STA & SDMMC_STA_TXFIFOHE) && (dataLeft > 0)){
			for(uint8_t i = 0; i < 8; i++){
				dataResult = buffer[bIdx++];
				dataResult |= (((uint32_t)buffer[bIdx++]) << 8);
				dataResult |= (((uint32_t)buffer[bIdx++]) << 16);
				dataResult |= (((uint32_t)buffer[bIdx++]) << 24);

				SDMMC1->FIFO = dataResult;
				dataLeft -= 4;
			}
		}
		//		if(DMA2->LISR & (DMA_LISR_TCIF3 | DMA_LISR_TEIF3 | DMA_LISR_DMEIF3 | DMA_LISR_FEIF3))
		//			break;
	}

	cmdResponse = SDMMC1->STA;
	if(blockCount > 1){
		opResult = SendStopCommand();
		ASSERT_RESULT(opResult);
	}

	//	if(DMA2->LISR & (DMA_LISR_TEIF3 | DMA_LISR_DMEIF3 | DMA_LISR_FEIF3))
	//		return RESULT_ERROR;

	taskEXIT_CRITICAL();
	SDMMC1->ICR = 0xFFFFFFFFU;
	if(cmdResponse & (SDMMC_STA_TXUNDERR | SDMMC_STA_DCRCFAIL | SDMMC_STA_DTIMEOUT))
		return RESULT_ERROR_INVALID_STATE;
	else
		return RESULT_OK;
}

DSTATUS disk_initialize (BYTE pdrv){
	result_t res;

	if(g_currentCard.cardFlags & SD_CARD_FLAGS_INIT) // already init, what you doing here?
		return 0;

	res = SD_Init();
	if(res != RESULT_OK)
		return STA_NOINIT;

	res = SD_Config();
	if(res != RESULT_OK)
		return STA_NOINIT;
	else
		return 0;
}

DSTATUS disk_status (BYTE pdrv){
	if(g_currentCard.cardFlags & SD_CARD_FLAGS_INIT)
		return 0;
	else
		return STA_NOINIT;
}

DRESULT disk_read (BYTE pdrv, BYTE* buff, LBA_t sector, UINT count){
	result_t res;
	res = SD_Read((sector << 9), buff, (count << 9));
	if(res != RESULT_OK)
		return RES_ERROR;
	else
		return RES_OK;
}

DRESULT disk_write (BYTE pdrv, const BYTE* buff, LBA_t sector, UINT count){
	result_t res;
	res = SD_Write((sector << 9), buff, (count << 9));
	if(res != RESULT_OK)
		return RES_ERROR;
	else
		return RES_OK;
}

DRESULT disk_ioctl (BYTE pdrv, BYTE cmd, void* buff){
	DRESULT res;

	if((g_currentCard.cardFlags & SD_CARD_FLAGS_INIT) == 0)
		return RES_NOTRDY;

	switch(cmd){
	case CTRL_SYNC:
		res = RES_OK;
		break;

		/* Get number of sectors on the disk (DWORD) */
	case GET_SECTOR_COUNT:
		*((DWORD *)buff) = g_currentCard.BlockCount;
		res = RES_OK;
		break;

		/* Get R/W sector size (WORD) */
	case GET_SECTOR_SIZE:
		*((DWORD *)buff) = g_currentCard.BlockSize;
		res = RES_OK;
		break;

		/* Get erase block size in unit of sector (DWORD) */
	case GET_BLOCK_SIZE:
		*(DWORD*)buff = g_currentCard.BlockSize / SD_CARD_DEFAULT_SECTOR_SIZE;
		res = RES_OK;
		break;

	default:
		res = RES_PARERR;
	}

	return res;
}

DWORD get_fattime(void){
	return getUptime();
}
