/*
 * rfid.c
 *
 *  Created on: Nov. 15, 2020
 *      Author: otavio
 */

#include <stdlib.h>
#include <string.h>

// List of Registers from MFRC522
#define CommandReg			0x01
#define ComlEnReg			0x02
#define DivlEnReg			0x03
#define ComIrqReg			0x04
#define DivIrqReg			0x05
#define ErrorReg			0x06
#define Status1Reg			0x07
#define Status2Reg			0x08
#define FIFODataReg			0x09
#define FIFOLevelReg		0x0A
#define WaterLevelReg		0x0B
#define ControlReg			0x0C
#define BitFramingReg		0x0D
#define CollReg				0x0E
#define ModeReg				0x11
#define TxModeReg			0x12
#define RxModeReg			0x13
#define TxControlReg		0x14
#define TxASKReg			0x15
#define TxSelReg			0x16
#define RxSelReg			0x17
#define RxThresholdReg		0x18
#define DemodReg			0x19
#define MfTxReg				0x1C
#define MfRxReg				0x1D
#define SerialSpeedReg		0x1F
#define CRCResultRegH		0x21
#define CRCResultRegL		0x22
#define ModWidthReg			0x24
#define RFCfgReg			0x26
#define GsNReg				0x27
#define CWGsPReg			0x28
#define ModGsPReg			0x29
#define TModeReg			0x2A
#define TPrescalerReg		0x2B
#define TreloadRegH			0x2C
#define TreloadRegL			0x2D
#define TcounterValRegH		0x2E
#define TcounterValRegL		0x2F
#define TestSel1Reg			0x31
#define TestSel2Reg			0x32
#define TestPinEnReg		0x33
#define TestPinValueReg		0x34
#define TestBusReg			0x35
#define AutoTestReg			0x36
#define VersionReg			0x37
#define AnalogTestReg		0x38
#define TestDAC1Reg			0x39
#define TestDAC2Reg			0x3A
#define TestADCReg			0x3B

#define GET_BIT_TO_POSITION(byte, bit, pos)		(uint8_t)(((byte >> bit) & 0x01) << pos)

#include "rfid.h"
#include "gpio.h"
#include <util.h>

#include "FreeRTOS.h"

static uint8_t g_intiated = 0;
static uint8_t g_fifoBuffer[64];

static void SpiTransfer(uint8_t *outBuffer, uint8_t *inBuffer, uint32_t length){
	uint16_t outIdx = 0;
	uint16_t discart;

	GPIO_Reset(GPIOI, 0);
	delay(1);

	for(uint32_t i = 0; i < length; i+=2){
		if((length & 1) && (i == length-1)){
			*(__IO uint8_t *)&SPI2->DR = outBuffer[i];

			while((SPI2->SR & SPI_SR_FRLVL) == 0)
				__asm("nop");
		}else{
			SPI2->DR = outBuffer[i] + (outBuffer[i+1] << 8);

			while((SPI2->SR & SPI_SR_FRLVL) < 0x400)
				__asm("nop");
		}

		while(SPI2->SR & SPI_SR_RXNE){
			if(inBuffer == NULL)
				discart = SPI2->DR;
			else
				inBuffer[outIdx] = SPI2->DR;

			outIdx++;
		}
	}

	GPIO_Set(GPIOI, 0);
}

static void RC522_ReadQueue(uint8_t reg, uint8_t *response, uint32_t length){
	uint32_t idx;
	uint8_t regs[32];

	for(idx = 0; idx < length; idx++)
		regs[idx] = ((reg + idx) << 1) | 0x80;

	regs[length] = 0; // last byte send must be zero to get last register value

	SpiTransfer(regs, response, length+1);
}

static uint8_t RC522_ReadOne(uint8_t reg){
	uint8_t rtnValue[4];

	rtnValue[0] = (reg << 1) | 0x80;
	rtnValue[1] = 0;

	SpiTransfer(rtnValue, rtnValue, 2);
	return rtnValue[1];
}

static void RC522_WriteOne(uint8_t reg, uint8_t value){
	uint8_t rtnValue[2];

	rtnValue[0] = (reg << 1) & ~0x80;
	rtnValue[1] = value;

	SpiTransfer(rtnValue, rtnValue, 2);
}

static void RC522_ClearMask(uint8_t reg, uint8_t mask){
	uint8_t writeValue = RC522_ReadOne(reg);
	writeValue &= ~(mask);
	RC522_WriteOne(reg, writeValue);
}

static void RC522_SetMask(uint8_t reg, uint8_t mask){
	uint8_t writeValue = RC522_ReadOne(reg);
	writeValue |= mask;
	RC522_WriteOne(reg, writeValue);
}

static void RC522_WriteQueue(uint8_t reg, uint8_t *values, uint32_t length){
	uint8_t regs[32];

	regs[0] = (reg << 1) & ~0x80;
	for(uint16_t idx = 1; idx <= length; idx++)
		regs[idx] = values[idx - 1];

	SpiTransfer(regs, NULL, length + 1);
}

static uint8_t RC522_ReadFIFO(uint8_t *data){
	uint8_t regs[65];
	uint8_t idx;
	uint8_t length = RC522_ReadOne(FIFOLevelReg);

	if(length == 0)
		return 0;

	for(idx = 0; idx < length; idx++)
		regs[idx] = (FIFODataReg << 1) | 0x80;

	regs[length] = 0;
	SpiTransfer(regs, data, length + 1);

	return length;
}

static void RC522_CalculateCRC(	uint8_t *data, uint8_t length, uint8_t *result){
	uint8_t readValue;

	RC522_WriteOne(CommandReg, RFID_Comm_Idle);		// Stop any active command.
	RC522_WriteOne(DivIrqReg, 0x04);					// Clear the CRCIRq interrupt request bit
	RC522_WriteOne(FIFOLevelReg, 0x80);					// FlushBuffer = 1, FIFO initialization
	RC522_WriteQueue(FIFODataReg, data, length);		// Write data to the FIFO
	RC522_WriteOne(CommandReg, RFID_Comm_CalcCRC);		// Start the calculation

	do{
		delay(1);
		readValue = RC522_ReadOne(DivIrqReg);
	}while((readValue & 0x04) == 0);

	RC522_WriteOne(CommandReg, RFID_Comm_Idle);
	result[0] = RC522_ReadOne(CRCResultRegL);
	result[1] = RC522_ReadOne(CRCResultRegH);
}

void RFID_Init(void){
	uint8_t data;

	if(g_intiated)
		return;

	// Pins used: PA8-RST, PB15-MOSI, PB14-MISO, PI1-SCK, PI0-CS
	RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOIEN);

	GPIO_Clear(GPIOA, 8);
	GPIO_Clear(GPIOB, 15);
	GPIO_Clear(GPIOB, 14);
	GPIO_Clear(GPIOI, 1);
	GPIO_Clear(GPIOI, 0);

	GPIO_SetOutput(GPIOA, 8, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_LOW, GPIO_PULL_PULL_UP);
	GPIO_SetOutput(GPIOI, 0, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_LOW, GPIO_PULL_PULL_UP);
	GPIO_Reset(GPIOA, 8);
	GPIO_Set(GPIOI, 0);

	GPIO_SetAF(GPIOB, 15, 5, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_PULL_UP);	// MOSI
	GPIO_SetAF(GPIOB, 14, 5, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_PULL_UP);	// MISO
	GPIO_SetAF(GPIOI, 1, 5, GPIO_TYPE_PUSH_PULL, GPIO_SPEED_VERY_HIGH, GPIO_PULL_NO_PULL);	// SCK

	delay(5);
	GPIO_Set(GPIOA, 8);
	delay(1);

	RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;

	if(SPI2->CR1 & SPI_CR1_SPE){
		// proper shutoff SPI
		while(SPI2->SR & SPI_SR_FTLVL)
			__asm("nop");

		while(SPI2->SR & SPI_SR_BSY)
			__asm("nop");
	}

	SPI2->CR1 = 0;
	while(SPI2->SR & SPI_SR_FRLVL)
		data = SPI2->DR;

	// Max CLK is PCLK1/8 = 54MHz/8 = 6.75MHz, For RC522 CPHA=0, CPOL=0
	SPI2->CR1 = ((0x04 << SPI_CR1_BR_Pos) | SPI_CR1_MSTR); // | SPI_CR1_CPHA | SPI_CR1_CPOL);

	SPI2->CR2 = 0;
	SPI2->CR2 = ((0x07 << SPI_CR2_DS_Pos) | SPI_CR2_SSOE | SPI_CR2_FRXTH);

	SPI2->CR1 |= SPI_CR1_SPE;

	g_intiated = 1;
}

void RFID_DeInit(void){
	if(g_intiated == 0)
		return;

	if(SPI2->CR1 & SPI_CR1_SPE){
		// proper shutoff SPI
		while(SPI2->SR & SPI_SR_FTLVL)
			__asm("nop");

		while(SPI2->SR & SPI_SR_BSY)
			__asm("nop");
	}

	SPI2->CR1 &= ~SPI_CR1_SPE;
	g_intiated = 0;
}

void RFID_Reset(void){
	GPIO_Reset(GPIOA, 8);
	delay(100);

	GPIO_Set(GPIOA, 8);
	delay(50);
}

void RFID_PowerUp(void){
	uint8_t readValue;

	if(g_intiated == 0)
		return;

	RC522_WriteOne(CommandReg, 0x0F);

	// Wait for soft-reset to be cleared
	delay(50);
	do{
		readValue = RC522_ReadOne(CommandReg);
	}while(readValue & 0x0F);

	RC522_WriteOne(TxModeReg, 0x00);
	RC522_WriteOne(RxModeReg, 0x00);
	RC522_WriteOne(ModWidthReg, 0x26);

	RC522_WriteOne(TModeReg, 0x80);			// TAuto=1; timer starts automatically at the end of the transmission in all communication modes at all speeds
	RC522_WriteOne(TPrescalerReg, 0xA9);		// TPreScaler = TModeReg[3..0]:TPrescalerReg, ie 0x0A9 = 169 => f_timer=40kHz, ie a timer period of 25μs.
	RC522_WriteOne(TreloadRegH, 0x03);		// Reload timer with 0x3E8 = 1000, ie 25ms before timeout.
	RC522_WriteOne(TreloadRegL, 0xE8);

	RC522_WriteOne(TxASKReg, 0x40);		// Default 0x00. Force a 100 % ASK modulation independent of the ModGsPReg register setting
	RC522_WriteOne(ModeReg, 0x3D);		// Default 0x3F. Set the preset value for the CRC coprocessor for the CalcCRC command to 0x6363 (ISO 14443-3 part 6.2.4)

	readValue = RC522_ReadOne(TxControlReg);
	if ((readValue & 0x03) != 0x03) {
		RC522_WriteOne(TxControlReg, (readValue | 0x03));
	}
}

uint8_t RFID_Transceive(rfid_comm_t command, uint8_t *dataToSend, uint8_t sendLength, uint8_t *recv, uint8_t *recvLength, uint8_t framing, uint8_t irqMask, uint8_t useCRC){
	uint8_t readValue;
	uint8_t crcResult[2];

	if(framing == 8)
		framing = 0;

	vPortEnterCritical();
	RC522_WriteOne(TxModeReg, 0x00);
	RC522_WriteOne(RxModeReg, 0x00);
	RC522_WriteOne(ModWidthReg, 0x26);

	if(useCRC){
		RC522_CalculateCRC(dataToSend, sendLength, crcResult);
	}

	RC522_WriteOne(CommandReg, RFID_Comm_Idle);				// Go to Idle
	RC522_WriteOne(ComIrqReg, 0x7F);						// Clear all seven interrupt request bits
	RC522_WriteOne(FIFOLevelReg, 0x80);						// FlushBuffer = 1, FIFO initialization
	RC522_WriteQueue(FIFODataReg, dataToSend, sendLength);	// Send data to FIFO

	if(useCRC){
		RC522_WriteQueue(FIFODataReg, crcResult, 2);
	}

	RC522_WriteOne(BitFramingReg, framing);					// Set up framing scheme
	RC522_WriteOne(CommandReg, (uint8_t)command);			// Execute the command

	if(command == RFID_Comm_Transceive)
		RC522_SetMask(BitFramingReg, 0x80);

	do{
		readValue = RC522_ReadOne(ComIrqReg);
	}while((readValue & irqMask) == 0);

	if((recvLength != NULL) && (recv != NULL))
		*recvLength = RC522_ReadFIFO(recv);

	vPortExitCritical();
	return readValue;
}

rfid_result_t RFID_IsCardPresent(uint16_t *atqa){
	uint8_t response;
	uint8_t responseData[2];

	g_fifoBuffer[0] = 0x26;

	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 1, NULL, NULL, 7, 0x60, 0);

	if(response & 0x20){
		response = RC522_ReadFIFO(responseData);
		if(response == 2){
			*atqa = (((uint16_t)responseData[0]) << 8) + responseData[1];
			return RFID_Result_Found;
		}else{
			return RFID_Result_Unknown;
		}
	}else{
		return RFID_Result_None;
	}
}

rfid_result_t RFID_GetUID(uint16_t atqa, uint8_t *UID){
	uint8_t response, fifoLength;

	if((atqa & 0x00C0))
		return RFID_Result_NotImplemented;

	g_fifoBuffer[0] = 0x93;
	g_fifoBuffer[1] = 0x20;

	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 2, g_fifoBuffer + 1, &fifoLength, 8, 0x60, 0);

	// TODO: treat collisions
	if(fifoLength == 5){
//		copyReversedUID(UID, g_fifoBuffer+2);
		memcpy(UID, g_fifoBuffer+2, 4);
		g_fifoBuffer[0] = 0x93;
		g_fifoBuffer[1] = 0x70;

		response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 7, g_fifoBuffer, &fifoLength, 8, 0x60, 1);


		// Is that a SAK? Who is a good SAK? WHO's a good'SAK
		if(g_fifoBuffer[1] == 0x08)
			return RFID_Result_Success;
		else
			return RFID_Result_NotImplemented;

	}

	return RFID_Result_NotImplemented;
}

rfid_result_t RFID_Authenticate(uint8_t *UID, uint8_t *key, uint8_t addr, rfid_auth_key_t keyType){
	uint8_t response, fifoLength;

	if(keyType == RFID_AUTH_KEY_A)
		g_fifoBuffer[0] = 0x60;
	else
		g_fifoBuffer[0] = 0x61;

	g_fifoBuffer[1] = addr;
	memcpy(g_fifoBuffer+2, key, 6);
	memcpy(g_fifoBuffer+8, UID, 4);

	response = RFID_Transceive(RFID_Comm_MFAuthent, g_fifoBuffer, 12, g_fifoBuffer, &fifoLength, 8, 0x11, 0);

	if(response & 0x01)
		return RFID_Result_Timeout;
	else
		return RFID_Result_Success;
}

rfid_result_t RFID_Read(uint8_t *UID, uint8_t addr, uint8_t *result){
	uint8_t response, fifoLength;

	g_fifoBuffer[0] = 0x30;
	g_fifoBuffer[1] = addr;
	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 2, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if((fifoLength == 1) && (g_fifoBuffer[1] == 0)){
			return RFID_Result_NACK;
		}else if((fifoLength == 1) && (g_fifoBuffer[1] == 0x04)){
			return RFID_Result_NotAllowed;
		}else if(fifoLength > 1){
			// read was success send the 16byte result
			if(fifoLength != 17)
				return RFID_Result_Unknown;

			memcpy(result, g_fifoBuffer+1, 16);
			return RFID_Result_Success;
		}else{
			return RFID_Result_Unknown;
		}
}

rfid_result_t RFID_ValueRead(uint8_t addr, int32_t *value, uint8_t *ID){
	uint8_t response, fifoLength;

	g_fifoBuffer[0] = 0x30;
	g_fifoBuffer[1] = addr;
	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 2, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if((fifoLength == 1) && (g_fifoBuffer[1] == 0)){
		return RFID_Result_NACK;
	}else if((fifoLength == 1) && (g_fifoBuffer[1] == 0x04)){
		return RFID_Result_NotAllowed;
	}else if(fifoLength > 1){
		// read was success send the 16byte result
		if(fifoLength != 17)
			return RFID_Result_Unknown;

		if((g_fifoBuffer[1] != (uint8_t)~g_fifoBuffer[5]) || (g_fifoBuffer[2] != (uint8_t)~g_fifoBuffer[6]) ||
				(g_fifoBuffer[3] != (uint8_t)~g_fifoBuffer[7]) || (g_fifoBuffer[4] != (uint8_t)~g_fifoBuffer[8]))
			return RFID_Result_NotValueBlock;

		*value = (int32_t)((((uint32_t)g_fifoBuffer[1])) | (((uint32_t)g_fifoBuffer[2]) << 8) |
				(((uint32_t)g_fifoBuffer[3]) << 16) | ((uint32_t)g_fifoBuffer[4] << 24));
		*ID = g_fifoBuffer[13];

		return RFID_Result_Success;
	}else{
		return RFID_Result_Unknown;
	}
}

rfid_result_t RFID_Write(uint8_t *data, uint8_t length, uint8_t addr){
	uint8_t response, fifoLength;

	if((addr & 0x03) == 0x03)
		return RFID_Result_BadParameter;

	if(length > 16)
		return RFID_Result_BadParameter;

	g_fifoBuffer[0] = 0xA0;
	g_fifoBuffer[1] = addr;
	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 2, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if(fifoLength != 1)
		return RFID_Result_Unknown;

	if(g_fifoBuffer[1] != 0x0A)
		return RFID_Result_NACK;

	memcpy(g_fifoBuffer, data, length);
	for(uint8_t idx = length; idx < 16; idx++)
		g_fifoBuffer[idx] = 0; // fill with trailing zeros if necessary

	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 16, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if((fifoLength == 0) && ((response & 0x03) == 0))
		return RFID_Result_Success;

	if((fifoLength == 1) && (g_fifoBuffer[1] == 0x0A))
		return RFID_Result_NACK;

	return RFID_Result_Unknown;
}

rfid_result_t RFID_Increment(uint8_t addr, int32_t delta){
	uint8_t response, fifoLength;

	if((addr & 0x03) == 0x03)
		return RFID_Result_BadParameter;

	g_fifoBuffer[0] = 0xC1;
	g_fifoBuffer[1] = addr;
	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 2, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if(fifoLength != 1)
		return RFID_Result_Unknown;

	if(g_fifoBuffer[1] != 0x0A)
		return RFID_Result_NACK;

	g_fifoBuffer[0] = (uint8_t)(delta & 0x000000FF);
	g_fifoBuffer[1] = (uint8_t)((delta & 0x0000FF00) >> 8);
	g_fifoBuffer[2] = (uint8_t)((delta & 0x00FF0000) >> 16);
	g_fifoBuffer[3] = (uint8_t)((delta & 0xFF000000) >> 24);

	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 4, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if((fifoLength == 0) && ((response & 0x03) == 0))
		return RFID_Result_Success;

	if((fifoLength == 1) && (g_fifoBuffer[1] == 0x0A))
		return RFID_Result_NACK;

	return RFID_Result_Unknown;
}

rfid_result_t RFID_Decrement(uint8_t addr, int32_t delta){
	uint8_t response, fifoLength;

	if((addr & 0x03) == 0x03)
		return RFID_Result_BadParameter;

	g_fifoBuffer[0] = 0xC0;
	g_fifoBuffer[1] = addr;
	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 2, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if(fifoLength != 1)
		return RFID_Result_Unknown;

	if(g_fifoBuffer[1] != 0x0A)
		return RFID_Result_NACK;

	g_fifoBuffer[0] = (uint8_t)(delta & 0x000000FF);
	g_fifoBuffer[1] = (uint8_t)((delta & 0x0000FF00) >> 8);
	g_fifoBuffer[2] = (uint8_t)((delta & 0x00FF0000) >> 16);
	g_fifoBuffer[3] = (uint8_t)((delta & 0xFF000000) >> 24);

	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 4, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if((fifoLength == 0) && ((response & 0x03) == 0))
		return RFID_Result_Success;

	if((fifoLength == 1) && (g_fifoBuffer[1] == 0x0A))
		return RFID_Result_NACK;

	return RFID_Result_Unknown;
}

rfid_result_t RFID_Transfer(uint8_t addr){
	uint8_t response, fifoLength;

	if((addr & 0x03) == 0x03)
		return RFID_Result_BadParameter;

	g_fifoBuffer[0] = 0xB0;
	g_fifoBuffer[1] = addr;
	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 2, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if((fifoLength == 0) && ((response & 0x03) == 0))
		return RFID_Result_Success;

	return RFID_Result_NACK;
}

rfid_result_t RFID_SetTrailerBlock(uint8_t trailerAddr, uint8_t userData, rfid_trailer_config_t config){
	uint8_t response, fifoLength, C[4], trailerB6, trailerB7, trailerB8;

	if((trailerAddr & 0x03) != 0x03)
		return RFID_Result_BadParameter;

	// Config Access to Trailer Block
	if((config.trailerWriteKeyA == RFID_ACCESS_KEY_A) && (config.trailerReadAccess == RFID_ACCESS_KEY_A) &&
			(config.trailerWriteAccess == RFID_ACCESS_NEVER) && (config.trailerReadKeyB == RFID_ACCESS_KEY_A) &&
			(config.trailerWriteKeyB == RFID_ACCESS_KEY_A)){
		C[3] = 0;
	}else if((config.trailerWriteKeyA == RFID_ACCESS_NEVER) && (config.trailerReadAccess == RFID_ACCESS_KEY_A) &&
			(config.trailerWriteAccess == RFID_ACCESS_NEVER) && (config.trailerReadKeyB == RFID_ACCESS_KEY_A) &&
			(config.trailerWriteKeyB == RFID_ACCESS_NEVER)){
		C[3] = 0b010;
	}else if((config.trailerWriteKeyA == RFID_ACCESS_KEY_B) && (config.trailerReadAccess == RFID_ACCESS_BOTH) &&
			(config.trailerWriteAccess == RFID_ACCESS_NEVER) && (config.trailerReadKeyB == RFID_ACCESS_NEVER) &&
			(config.trailerWriteKeyB == RFID_ACCESS_KEY_B)){
		C[3] = 0b100;
	}else if((config.trailerWriteKeyA == RFID_ACCESS_NEVER) && (config.trailerReadAccess == RFID_ACCESS_BOTH) &&
			(config.trailerWriteAccess == RFID_ACCESS_NEVER) && (config.trailerReadKeyB == RFID_ACCESS_NEVER) &&
			(config.trailerWriteKeyB == RFID_ACCESS_NEVER)){
		C[3] = 0b110;
	}else if((config.trailerWriteKeyA == RFID_ACCESS_KEY_A) && (config.trailerReadAccess == RFID_ACCESS_KEY_A) &&
			(config.trailerWriteAccess == RFID_ACCESS_KEY_A) && (config.trailerReadKeyB == RFID_ACCESS_KEY_A) &&
			(config.trailerWriteKeyB == RFID_ACCESS_KEY_A)){
		C[3] = 0b001;
	}else if((config.trailerWriteKeyA == RFID_ACCESS_NEVER) && (config.trailerReadAccess == RFID_ACCESS_BOTH) &&
			(config.trailerWriteAccess == RFID_ACCESS_KEY_B) && (config.trailerReadKeyB == RFID_ACCESS_NEVER) &&
			(config.trailerWriteKeyB == RFID_ACCESS_NEVER)){
		C[3] = 0b101;
	}else if((config.trailerWriteKeyA == RFID_ACCESS_NEVER) && (config.trailerReadAccess == RFID_ACCESS_BOTH) &&
			(config.trailerWriteAccess == RFID_ACCESS_NEVER) && (config.trailerReadKeyB == RFID_ACCESS_NEVER) &&
			(config.trailerWriteKeyB == RFID_ACCESS_NEVER)){
		C[3] = 0b111;
	}else{ // FALLS DEFAULT TO NEVER/KEY B/KEY A|B/KEY B/NEVER/KEY B
		C[3] = 0b011;
	}

	// Config Access to Data Blocks
	for(uint8_t block = 0; block < 3; block++){
		if((config.blockRead[block] == RFID_ACCESS_BOTH) && (config.blockWrite[block] == RFID_ACCESS_BOTH) &&
				(config.blockIncrement[block] == RFID_ACCESS_BOTH) && (config.blockDecrement[block] == RFID_ACCESS_BOTH)){
			C[block] = 0;
		}else if((config.blockRead[block] == RFID_ACCESS_BOTH) && (config.blockWrite[block] == RFID_ACCESS_NEVER) &&
				(config.blockIncrement[block] == RFID_ACCESS_NEVER) && (config.blockDecrement[block] == RFID_ACCESS_NEVER)){
			C[block] = 0b010;
		}else if((config.blockRead[block] == RFID_ACCESS_BOTH) && (config.blockWrite[block] == RFID_ACCESS_KEY_B) &&
				(config.blockIncrement[block] == RFID_ACCESS_NEVER) && (config.blockDecrement[block] == RFID_ACCESS_NEVER)){
			C[block] = 0b100;
		}else if((config.blockRead[block] == RFID_ACCESS_BOTH) && (config.blockWrite[block] == RFID_ACCESS_NEVER) &&
				(config.blockIncrement[block] == RFID_ACCESS_NEVER) && (config.blockDecrement[block] == RFID_ACCESS_BOTH)){
			C[block] = 0b001;
		}else if((config.blockRead[block] == RFID_ACCESS_KEY_B) && (config.blockWrite[block] == RFID_ACCESS_KEY_B) &&
				(config.blockIncrement[block] == RFID_ACCESS_NEVER) && (config.blockDecrement[block] == RFID_ACCESS_NEVER)){
			C[block] = 0b011;
		}else if((config.blockRead[block] == RFID_ACCESS_KEY_B) && (config.blockWrite[block] == RFID_ACCESS_NEVER) &&
				(config.blockIncrement[block] == RFID_ACCESS_NEVER) && (config.blockDecrement[block] == RFID_ACCESS_NEVER)){
			C[block] = 0b101;
		}else if((config.blockRead[block] == RFID_ACCESS_NEVER) && (config.blockWrite[block] == RFID_ACCESS_NEVER) &&
				(config.blockIncrement[block] == RFID_ACCESS_NEVER) && (config.blockDecrement[block] == RFID_ACCESS_NEVER)){
			C[block] = 0b111;
		}else{ // Defaults to KEY A|B/KEY B/KEY B/KEY A|B
			C[block] = 0b110;
		}
	}

	g_fifoBuffer[0] = 0xA0;
	g_fifoBuffer[1] = trailerAddr;
	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 2, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if(fifoLength != 1)
		return RFID_Result_Unknown;

	if(g_fifoBuffer[1] != 0x0A)
		return RFID_Result_NACK;

	memcpy(g_fifoBuffer, config.keyA, 6);
	g_fifoBuffer[6] = GET_BIT_TO_POSITION(~C[3], 1, 7) | GET_BIT_TO_POSITION(~C[2], 1, 6) | GET_BIT_TO_POSITION(~C[1], 1, 5) |
				GET_BIT_TO_POSITION(~C[0], 1, 4) | GET_BIT_TO_POSITION(~C[3], 2, 3) | GET_BIT_TO_POSITION(~C[2], 2, 2) |
				GET_BIT_TO_POSITION(~C[1], 2, 1) | GET_BIT_TO_POSITION(~C[0], 2, 0);

	g_fifoBuffer[7] = GET_BIT_TO_POSITION(C[3], 2, 7) | GET_BIT_TO_POSITION(C[2], 2, 6) | GET_BIT_TO_POSITION(C[1], 2, 5) |
			GET_BIT_TO_POSITION(C[0], 2, 4) | GET_BIT_TO_POSITION(~C[3], 0, 3) | GET_BIT_TO_POSITION(~C[2], 0, 2) |
			GET_BIT_TO_POSITION(~C[1], 0, 1) | GET_BIT_TO_POSITION(~C[0], 0, 0);

	g_fifoBuffer[8] = GET_BIT_TO_POSITION(C[3], 0, 7) | GET_BIT_TO_POSITION(C[2], 0, 6) | GET_BIT_TO_POSITION(C[1], 0, 5) |
				GET_BIT_TO_POSITION(C[0], 0, 4) | GET_BIT_TO_POSITION(C[3], 1, 3) | GET_BIT_TO_POSITION(C[2], 1, 2) |
				GET_BIT_TO_POSITION(C[1], 1, 1) | GET_BIT_TO_POSITION(C[0], 1, 0);

	g_fifoBuffer[9] = userData;

	memcpy(g_fifoBuffer + 10, config.keyB, 6);

	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 16, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if((fifoLength == 0) && ((response & 0x03) == 0))
		return RFID_Result_Success;

	if((fifoLength == 1) && (g_fifoBuffer[1] == 0x0A))
		return RFID_Result_NACK;

	return RFID_Result_Unknown;
}

rfid_result_t RFID_ValueMode(uint8_t addr, int32_t value, uint8_t ID){
	uint8_t response, fifoLength;

	if((addr & 0x03) == 0x03)
		return RFID_Result_BadParameter;

	g_fifoBuffer[0] = 0xA0;
	g_fifoBuffer[1] = addr;
	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 2, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if(fifoLength != 1)
		return RFID_Result_Unknown;

	if(g_fifoBuffer[1] != 0x0A)
		return RFID_Result_NACK;

	g_fifoBuffer[0] = (uint8_t)(value & 0x000000FF);
	g_fifoBuffer[1] = (uint8_t)((value & 0x0000FF00) >> 8);
	g_fifoBuffer[2] = (uint8_t)((value & 0x00FF0000) >> 16);
	g_fifoBuffer[3] = (uint8_t)((value & 0xFF000000) >> 24);

	g_fifoBuffer[4] = ~g_fifoBuffer[0];
	g_fifoBuffer[5] = ~g_fifoBuffer[1];
	g_fifoBuffer[6] = ~g_fifoBuffer[2];
	g_fifoBuffer[7] = ~g_fifoBuffer[3];

	memcpy(g_fifoBuffer + 8, g_fifoBuffer, 4);

	g_fifoBuffer[12] = ID;
	g_fifoBuffer[13] = ~ID;
	g_fifoBuffer[14] = ID;
	g_fifoBuffer[15] = ~ID;

	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 16, g_fifoBuffer, &fifoLength, 8, 0x60, 1);

	if((fifoLength == 0) && ((response & 0x03) == 0))
		return RFID_Result_Success;

	if((fifoLength == 1) && (g_fifoBuffer[1] == 0x0A))
		return RFID_Result_NACK;

	return RFID_Result_Unknown;
}

void RFID_Disconnect(void){
	// Clear the MFCrypto1On flag to stop encrypted transmition
	RC522_ClearMask(Status2Reg, 0x08);
}

rfid_result_t RFID_Halt(void){
	uint8_t response, fifoLength;

	g_fifoBuffer[0] = 0x50;
	g_fifoBuffer[1] = 0;

	response = RFID_Transceive(RFID_Comm_Transceive, g_fifoBuffer, 2, g_fifoBuffer, &fifoLength, 8, 0x01, 1);
	if(response & 0x01) // good Halt responds with timeout
		return RFID_Result_Success;
	else
		return RFID_Result_Timeout;
}
