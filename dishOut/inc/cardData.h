/*
 * cardData.h
 *
 *	An abstraction of rfid library to Read and Write
 *	Card using the card format format
 *
 *  Created on: Jan. 20, 2021
 *      Author: otavio
 */

#ifndef DISHOUT_INC_CARDDATA_H_
#define DISHOUT_INC_CARDDATA_H_

#define CARD_DATA_TIMEOUT_INFINITE     0xFFFFFFFFU
#define CARD_DATA_SECTOR_SIZE          48U
#define CARD_DATA_BLOCK_SIZE           16U
#define CARD_DATA_SECTOR_COUNT         15U // sector 0 defines the card format name
#define CARD_DATA_FORMAT_TRAILER       3U
#define CARD_DATA_FORMAT_SECTOR        0
#define CARD_DATA_THREAD_STACK_SIZE    512U
#define CARD_DATA_DEFAULT_CONN_TIMEOUT 60000U

#include "cardFormat.h"
#include "util.h"

typedef enum{
	CARD_OPERATION_BEGIN_FORMAT,
	CARD_OPERATION_WRITE,
	CARD_OPERATION_READ
}card_operation_t;

typedef enum{
	CARD_FIELD_OP_ALTER     = (uint8_t)0,
	CARD_FIELD_OP_INCREMENT = (uint8_t)1U,
	CARD_FIELD_OP_DECREMENT = (uint8_t)2U,
	CARD_FIELD_OP_NONE      = (uint8_t)3U, // skip this field
//	CARD_FIELD_OP_TRANSFER,
//	CARD_FIELD_OP_RESTORE
}card_purse_op_t;

struct card_sector_data{
	uint8_t update;
	rfid_trailer_config_t trailerConfig;
	card_purse_op_t purseOp;
	uint8_t sectorData[CARD_DATA_SECTOR_SIZE];
};

typedef void (*card_operation_result_t)(card_operation_t, result_t, struct card_sector_data *, struct card_format *);

int cardData_Connect(uint32_t timeout);
int cardData_defineKey(uint8_t *key, rfid_auth_key_t keyType);
int cardData_TryCardClean(void);
int cardData_DefineFormat(struct card_format format);
int cardData_WriteSector(uint8_t *data, uint8_t sec);
int cardData_ReadPurseValue(int32_t *data, uint8_t sec, uint8_t *ID);
int cardData_ReadSector(uint8_t *data, uint8_t sec, uint8_t skipFirst);
int cardData_PurseOperation(uint32_t delta, uint8_t sec, card_purse_op_t op);
struct card_sector_data *cardData_getSectorsData(void);
void cardData_releaseSectorData(void);
int cardData_initiateCardOperation(card_operation_t op, struct card_format *targetFormat, card_operation_result_t callback);

#endif /* DISHOUT_INC_CARDDATA_H_ */
