/*
 * rmii.h
 *
 *  Created on: Jan. 5, 2021
 *      Author: otavio
 */

#ifndef DISHOUT_INC_RMII_H_
#define DISHOUT_INC_RMII_H_

#define ETHERNET_TX_BUFFER_COUNT    5U
#define ETHERNET_RX_BUFFER_COUNT    5U

#define ETHERNET_BUFFER_SIZE        (uint32_t)1524U

#include "util.h"

typedef struct{
	uint32_t desc0;
	uint32_t desc1;
	uint32_t desc2;
	uint32_t desc3;
}ethernet_dma_desc_t;

extern const uint8_t rmii_interfaceMacAddr[6];

result_t rmii_init(void);
result_t rmii_start(void);
result_t rmii_transmit(uint8_t *buffer, uint32_t length);
result_t rmii_initRecvBuffer(uint8_t *buffers, uint32_t count, uint32_t *recvLength, uint32_t bufferLength);
result_t rmii_getConnectionStatus(void);

void rmii_recvHandler(uint32_t bufferIdx);

#endif /* DISHOUT_INC_RMII_H_ */
