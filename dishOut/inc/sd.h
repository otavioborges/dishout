/*
 * sd.h
 *
 *  Created on: Dec. 15, 2020
 *      Author: otavio
 */

#ifndef INC_SD_H_
#define INC_SD_H_

#define SD_CARD_FLAGS_INIT			0x01
#define SD_CARD_FLAGS_PRESENT		0x02

#define SD_CARD_DEFAULT_SECTOR_SIZE	512
#define SD_CARD_VOLUME_NAME			"0:"
#define SD_CARD_ROOT     			"0:/"

#include "util.h"
#include "FreeRTOS.h"
#include "semphr.h"

typedef enum{
	SD_TYPE_SDHC				= 0x00,
	SD_TYPE_SD_V2				= 0x01,
	SD_TYPE_SD_V1				= 0x02,
	SD_TYPE_MMC					= 0x03
}sd_type_t;

typedef struct{
	uint8_t		cardFlags;
	uint32_t	ocr;
	uint32_t	capacity;
	sd_type_t	type;

	uint32_t    BlockCount;      // SD card blocks count
	uint32_t    BlockSize;       // SD card block size (bytes), determined in SD_ReadCSD()
	uint32_t    MaxBusClkFreq;   // Maximum card bus frequency (MHz)
	uint8_t     CSDVer;          // SD card CSD register version
	uint16_t    RCA;             // SD card RCA address (only for SDIO)
	uint8_t     MID;             // SD card manufacturer ID
	uint16_t    OID;             // SD card OEM/Application ID
	uint8_t     PNM[5];          // SD card product name (5-character ASCII string)
	uint8_t     PRV;             // SD card product revision (two BCD digits: '6.2' will be 01100010b)
	uint32_t    PSN;             // SD card serial number
	uint16_t    MDT;             // SD card manufacturing date
	uint8_t     CSD[16];         // SD card CSD register (card structure data)
	uint32_t    CID[4];          // SD card CID register (card identification number)
	uint32_t    SCR[2];          // SD card SCR register (SD card configuration)
}sd_card_t;

typedef enum{
	DISK_OP_IDLE	= 0x00,
	DISK_OP_WRITE	= 0x01,
	DISK_OP_READ	= 0x02
}disk_op_t;

typedef struct{
	SemaphoreHandle_t diskSemaphore;
	uint8_t diskInit;
	uint8_t diskOp;
	uint8_t containsHttp;
	uint8_t fannypackPackage;
	char path[128];
	uint32_t TotalSize;
	uint8_t *result;
	uint32_t bytesInResult;
}disk_share_t;

result_t SD_Init(void);

result_t SD_IsCardPresent(void);
result_t SD_Config(void);

result_t SD_Read(uint32_t address, uint8_t *buffer, uint32_t size);
result_t SD_Write(uint32_t address, const uint8_t *buffer, uint32_t size);

#endif /* INC_SD_H_ */
