/*
 * httpData.h
 *
 *  Created on: Jan. 8, 2021
 *      Author: otavio
 */

#ifndef DISHOUT_INC_HTTPDATA_H_
#define DISHOUT_INC_HTTPDATA_H_

#define HTTPDATA_DEFAULT_FOLDER             "wwwData"
#define HTTPDATA_DEFAULT_INDEX              "wwwData/index.htm"
#define HTTPDATA_DEFAULT_404                "wwwData/404.htm"
#define HTTPDATA_DEFAULT_401                "wwwData/401.htm"

#define HTTPDATA_INFINITE_RETRIES           0xFFU

#define HTTPDATA_CLIENT_THREAD_STACK        4096U
#define HTTPDATA_RECEIVE_DEFAULT_TIMEOUT    10000
#define HTTPDATA_DEFAULT_HTTP_PORT          80U
#define HTTPDATA_MAX_CONNECTIONS            3U
#define HTTPDATA_BUFFER_LENGTH              1024U
#define HTTPDATA_LARGE_FILE_BUFFER_LENGTH   1048576U // 1MB
#define HTTPDATA_LARGE_FILE_DATA_OFFSET     128U

#include "util.h"
#include "FreeRTOS.h"
#include "semphr.h"

#include "lwip/sockets.h"

struct http_client_data{
	uint8_t isConnected;
	uint8_t clientIdx;
	int clientSock;
	result_t procResult;
	struct sockaddr_in clientAddr;
	SemaphoreHandle_t clientMutex;
	uint32_t clientPriority;
};

struct http_client_buffer{
	uint8_t *rxData;
	uint8_t *txData;

	int32_t rxLength;
	int32_t txLength;
};

struct http_response_code{
	uint16_t code;
	char name[64];
};

struct __attribute__((__packed__)) http_large_file_header{
	char fileName[124];
	uint32_t dataInBuffer;
};

result_t httpdata_init(struct http_client_data *clients);
void httpdata_safeSocketClose(int *s);
result_t httpData_isDownloadServerOn(uint8_t retries, uint8_t *serverStatus, uint32_t *serverIP, uint32_t ourIP);
result_t httpData_downloadServer(uint32_t serverIP, uint8_t retries, uint8_t *dataBuffer, uint32_t bufferLength);
result_t httpData_initiateServer(int *server);
void httpdata_clientThread(void *argument);


#endif /* DISHOUT_INC_HTTPDATA_H_ */
