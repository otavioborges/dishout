/*
 * priorities.h
 *
 *  Created on: Jan. 5, 2021
 *      Author: otavio
 */

#ifndef DISHOUT_INC_PRIORITIES_H_
#define DISHOUT_INC_PRIORITIES_H_

#define PRIORITY_IRQ_ETH                    8U
#define PRIORITY_IRQ_DMA_ETH                9U
#define PRIORITY_IRQ_DELAY                  10U
#define PRIORITY_IRQ_RANDOM                 11U
#define PRIORITY_IRQ_MEM_COPY               12U

#define PRIORITY_TASK_CLIENTS               7U
#define PRIORITY_TASK_TCP_IP                6U
#define PRIORITY_TASK_RECV                  5U
#define PRIORITY_TASK_MAIN                  4U
#define PRIORITY_TASK_RFID                  3U
#define PRIORITY_TASK_GUI                   2U

#endif /* DISHOUT_INC_PRIORITIES_H_ */
