/*
 * terminal.h
 *
 *  Created on: Jan. 18, 2021
 *      Author: otavio
 */

#ifndef DISHOUT_INC_TERMINAL_H_
#define DISHOUT_INC_TERMINAL_H_

//#define TERMINAL_ON_LCD    FALSE
//#ifndef TERMINAL_ON_LCD
//#define TERMINAL_ON_LCD    TRUE
//#endif

#include "util.h"

result_t terminal_init(void);
result_t terminal_send(const uint8_t *buffer, uint32_t length);
int terminal_print(const char *__b, ...);

#endif /* DISHOUT_INC_TERMINAL_H_ */
