/*
 * tcpIpDriver.h
 *
 *  Created on: Jan. 15, 2021
 *      Author: otavio
 */

#include "lwip/netif.h"

err_t tcpIpDriver_init(struct netif *netif);
void *pvPortCalloc(size_t xElements, size_t xSizeOfElement);
