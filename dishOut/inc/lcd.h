/*
 * lcd.h
 *
 *  Created on: Jan. 28, 2021
 *      Author: otavio
 */

#ifndef DISHOUT_INC_LCD_H_
#define DISHOUT_INC_LCD_H_

#define LCD_WIDTH  480
#define LCD_HEIGHT 272

#include "util.h"

typedef uint16_t color_t;

#define COLOR_WHITE   (color_t)0xFFFFU
#define COLOR_BLACK   (color_t)0
#define COLOR_BLUE    (color_t)0x001FU
#define COLOR_GREEN   (color_t)0x07E0U
#define COLOR_RED     (color_t)0xF800U
#define COLOR_YELLOW  (color_t)0xFFE0U
#define COLOR_CYAN    (color_t)0x07FFU
#define COLOR_MAGENTA (color_t)0xF81FU

color_t lcd_colorFromARGB(uint32_t argb);
result_t lcd_init(void);
void lcd_backlight(uint8_t bclt);
void lcd_rectangle(color_t color, uint16_t x, uint16_t y, uint16_t width, uint16_t height);
void lcd_printText(char *text, color_t color, color_t backColor, uint16_t x, uint16_t y);
void lcd_paint(color_t color);

#endif /* DISHOUT_INC_LCD_H_ */
