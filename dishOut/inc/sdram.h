/*
 * sdram.h
 *
 *  Created on: Jan. 5, 2021
 *      Author: otavio
 */

#ifndef DISHOUT_INC_SDRAM_H_
#define DISHOUT_INC_SDRAM_H_

#include "util.h"

void sdram_init(void);
result_t sdram_initArray(void **__p, uint32_t __l);
result_t sdram_initMultArray(void **__p, uint32_t __l, uint32_t __c);

#endif /* DISHOUT_INC_SDRAM_H_ */
