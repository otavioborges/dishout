/*
 * cardFormat.h
 *
 *  Created on: Jan. 18, 2021
 *      Author: otavio
 */

#ifndef DISHOUT_INC_CARDFORMAT_H_
#define DISHOUT_INC_CARDFORMAT_H_

#define CARD_FORMAT_STRUCT_FORMAT_LENGTH       64U
#define CARD_FORMAT_STRUCT_FIELD_LENGTH        64U
#define CARD_FORMAT_UUID_LENGTH                16U

#include "util.h"
#include "rfid.h"

#include <stdint.h>

typedef enum{
	CARD_FIELD_TYPE_UUID      = (uint8_t)0U,
	CARD_FIELD_TYPE_TEXT      = (uint8_t)1U,
	CARD_FIELD_TYPE_NUMBER    = (uint8_t)2U,
	CARD_FIELD_TYPE_PURSE     = (uint8_t)3U,
	CARD_FIELD_TYPE_INVALID   = (uint8_t)0xFFU
}card_field_t;

typedef enum{
	CARD_ACCESS_NONE          = (uint8_t)0U,
	CARD_ACCESS_READ          = (uint8_t)1U,
	CARD_ACCESS_WRITE         = (uint8_t)2U,
	CARD_ACCESS_BOTH          = (uint8_t)3U,
	CARD_ACCESS_TRANSFER      = (uint8_t)4U,
	CARD_ACCESS_PURSE_ALL     = (uint8_t)5U,
}card_access_t;

struct __attribute__((__packed__)) card_file{
	uint32_t uiMagicNumber;
	uint32_t uiNumberOfFormats;
	uint8_t uiMasterKey[6];
};

struct __attribute__((__packed__)) card_field{
	char sName[60U];
	uint8_t cfDataType;
	uint8_t caUserAccess;
	uint8_t caAdminAccess;
	uint8_t reserved;
};


struct __attribute__((__packed__)) card_format{
	char sName[60U];
	uint32_t uiNumberOfFields;
	struct card_field *cfFields;
};

// TODO: field names must be unique, they can be removed
// TODO: encrypt the database file on card
result_t cardFormat_init(void);
int cardFormat_getCount(void);
int cardFormat_addFormat(const char *name);
int cardFormat_removeFormat(const char *name);
int cardFormat_getFormat(const char *name, struct card_format **result);
char **cardFormat_getFormatNames(uint32_t *count);
int cardFormat_saveFields(const char *formatName, struct card_field *fields, uint32_t count);
int cardFormat_saveModifications(void);
void cardFormat_deleteDatabase(void);
int cardFormat_isField(const struct card_format *format, const char *fieldName);
uint8_t *cardFormat_getMasterKey(void);
int cardFormat_createTrailerConfig(const struct card_field field, rfid_trailer_config_t *config);
int cardFormat_parseValue(const struct card_field field, const void *data, uint8_t *result);

#endif /* DISHOUT_INC_CARDFORMAT_H_ */
