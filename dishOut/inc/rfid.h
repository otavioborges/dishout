/*
 * rfid.h
 *
 *  Created on: Jan. 12, 2021
 *      Author: otavio
 */

#ifndef DISHOUT_INC_RFID_H_
#define DISHOUT_INC_RFID_H_

#include <stdint.h>

typedef enum{
	RFID_Comm_Idle			= 0x00,	// no action, cancels current command execution
	RFID_Comm_Mem			= 0x01,	// stores 25 bytes into the internal buffer
	RFID_Comm_GenRandomID	= 0x02,	// generates a 10-byte random ID number
	RFID_Comm_CalcCRC		= 0x03,	// activates the CRC coprocessor or performs a self test
	RFID_Comm_Transmit		= 0x04,	// transmits data from the FIFO buffer
	RFID_Comm_NoCmdChange	= 0x07,	// no command change, can be used to modify the CommandReg register bits without affecting the command, for example, the PowerDown bit
	RFID_Comm_Receive		= 0x08,	// activates the receiver circuits
	RFID_Comm_Transceive	= 0x0C,	// transmits data from FIFO buffer to antenna and automatically activates the receiver after transmission
	RFID_Comm_MFAuthent		= 0x0E,	// performs the MIFARE standard authentication as a reader
	RFID_Comm_SoftReset		= 0x0F	// you know!
}rfid_comm_t;

typedef enum{
	RFID_Result_Success			= 0x00,
	RFID_Result_NotImplemented	= 0x01,
	RFID_Result_None			= 0x02,
	RFID_Result_Found			= 0x03,
	RFID_Result_Unknown			= 0x04,
	RFID_Result_NACK			= 0x05,
	RFID_Result_Timeout			= 0x06,
	RFID_Result_BadParameter	= 0x07,
	RFID_Result_NotValueBlock	= 0x08,
	RFID_Result_NotAllowed		= 0x09
}rfid_result_t;

typedef enum{
	RFID_AUTH_KEY_A				= 0x00,
	RFID_AUTH_KEY_B				= 0x01
}rfid_auth_key_t;

typedef enum{
	RFID_ACCESS_NEVER			= 0x00,
	RFID_ACCESS_KEY_A			= 0x01,
	RFID_ACCESS_KEY_B			= 0x02,
	RFID_ACCESS_BOTH			= 0x03
}rfid_access_t;

typedef struct{
	uint8_t keyA[6];
	uint8_t keyB[6];

	rfid_access_t trailerWriteKeyA;
	rfid_access_t trailerReadAccess;
	rfid_access_t trailerWriteAccess;
	rfid_access_t trailerReadKeyB;
	rfid_access_t trailerWriteKeyB;

	rfid_access_t blockRead[3];
	rfid_access_t blockWrite[3];
	rfid_access_t blockIncrement[3];
	rfid_access_t blockDecrement[3];
}rfid_trailer_config_t;

#include <stdint.h>

void RFID_Init(void);
void RFID_DeInit(void);
void RFID_Reset(void);
void RFID_PowerUp(void);
uint8_t RFID_Transceive(rfid_comm_t command, uint8_t *dataToSend, uint8_t sendLength, uint8_t *recv, uint8_t *recvLength, uint8_t framing, uint8_t irqMask, uint8_t useCRC);
rfid_result_t RFID_IsCardPresent(uint16_t *atqa);
rfid_result_t RFID_GetUID(uint16_t atqa, uint8_t *UID);
rfid_result_t RFID_Authenticate(uint8_t *UID, uint8_t *key, uint8_t addr, rfid_auth_key_t keyType);
rfid_result_t RFID_Read(uint8_t *UID, uint8_t addr, uint8_t *result);
rfid_result_t RFID_ValueRead(uint8_t addr, int32_t *value, uint8_t *ID);
rfid_result_t RFID_Write(uint8_t *data, uint8_t length, uint8_t addr);
rfid_result_t RFID_Increment(uint8_t addr, int32_t delta);
rfid_result_t RFID_Decrement(uint8_t addr, int32_t delta);
rfid_result_t RFID_Transfer(uint8_t addr);
rfid_result_t RFID_SetTrailerBlock(uint8_t trailerAddr, uint8_t userData, rfid_trailer_config_t config);
rfid_result_t RFID_ValueMode(uint8_t addr, int32_t value, uint8_t ID);
void RFID_Disconnect(void);
rfid_result_t RFID_Halt(void);


#endif /* DISHOUT_INC_RFID_H_ */
