/*
 * gpio.h
 *
 *  Created on: Nov. 15, 2020
 *      Author: otavio
 */

#ifndef INC_PORT_H_
#define INC_PORT_H_

#include "stm32f7xx.h"
#include "gpio.h"
#include <stdint.h>

void PORT_Clear(GPIO_TypeDef *port, uint16_t mask);
void PORT_SetOutput(GPIO_TypeDef *port, uint16_t mask, gpio_type_t type, gpio_speed_t speed, gpio_pull_t pull);
void PORT_SetInput(GPIO_TypeDef *port, uint16_t mask, gpio_speed_t speed, gpio_pull_t pull);
void PORT_SetAF(GPIO_TypeDef *port, uint16_t mask, uint8_t alternate, gpio_type_t type, gpio_speed_t speed, gpio_pull_t pull);

void PORT_Reset(GPIO_TypeDef *port, uint16_t mask);
void PORT_Set(GPIO_TypeDef *port, uint16_t mask);

uint16_t PORT_Read(GPIO_TypeDef *port, uint16_t mask);

#endif /* INC_PORT_H_ */
