/*
 * util.h
 *
 *  Created on: Jan. 5, 2021
 *      Author: otavio
 */

#ifndef DISHOUT_INC_UTIL_H_
#define DISHOUT_INC_UTIL_H_

#define ASSERT_RESULT(x)            if(x != RESULT_OK) return x

#define copyMemNonBlocking          copyMem

#include <stdint.h>

typedef enum{
	RESULT_OK                       =(int8_t)0,
	RESULT_ERROR_BUSY               =(int8_t)-1,
	RESULT_ERROR_TIMEOUT            =(int8_t)-2,
	RESULT_ERROR_LINK_DOWN          =(int8_t)-3,
	RESULT_ERROR_PHY                =(int8_t)-4,
	RESULT_ERROR_DMA                =(int8_t)-5,
	RESULT_ERROR_BAD_PARAMS         =(int8_t)-6,
	RESULT_ERROR_NMI                =(int8_t)-7,
	RESULT_ERROR_HARDFAULT          =(int8_t)-8,
	RESULT_ERROR_MEMMANG            =(int8_t)-9,
	RESULT_ERROR_BUSFAULT           =(int8_t)-10,
	RESULT_ERROR_USAGEFAULT         =(int8_t)-11,
	RESULT_ERROR_TASK_CREATE        =(int8_t)-12,
	RESULT_ERROR_RTOS_ASSERT        =(int8_t)-13,
	RESULT_ERROR_SOCKET_CREATION    =(int8_t)-14,
	RESULT_ERROR_SOCKET_CONNECTION  =(int8_t)-15,
	RESULT_ERROR_SOCKET_RECV        =(int8_t)-16,
	RESULT_ERROR_SOCKET_SEND        =(int8_t)-17,
	RESULT_ERROR_INVALID_STATE      =(int8_t)-18,
	RESULT_ERROR_CRC_FAIL           =(int8_t)-19,
	RESULT_ERROR_OUT_OF_BOUNDS      =(int8_t)-20,
	RESULT_ERROR_SD_WRITE           =(int8_t)-21,
	RESULT_ERROR_SD_READ            =(int8_t)-22,
	RESULT_ERROR_FANNYPACK          =(int8_t)-23,
	RESULT_ERROR_EACCESS            =(int8_t)-24,
	RESULT_ERROR_INVALID_KEY        =(int8_t)-25,
	RESULT_ERROR_SIZE_MISMATCH      =(int8_t)-26,
	RESULT_ERROR_STACK_OVERFLOW     =(int8_t)-27,
	RESULT_ERROR_EXISTS             =(int8_t)-28,
	RESULT_ERROR_NO_OPERATION       =(int8_t)-29,
	RESULT_ERROR_FORMAT_MISMATCH    =(int8_t)-30
}result_t;

struct hardFaultDetails{
	uint32_t r0;
	uint32_t r1;
	uint32_t r2;
	uint32_t r3;
	uint32_t r12;
	uint32_t lr;
	uint32_t pc;
	uint32_t psr;
};

result_t CoreInit(void);

uint32_t getRandom(void);

void delay(uint32_t ms);
result_t waitUntilFlag(const volatile uint32_t *reg, uint32_t flag, uint16_t maxWait, uint8_t exitWhenZero);
uint32_t getUptime(void);
uint64_t getLongUptime(void);

uint32_t copyMem(void *dest, const void *src, uint32_t length);
//uint32_t copyMemNonBlocking(void *dest, const void *src, uint32_t length);
void ErrorHandler(result_t errorCode);
char *fitSize(uint32_t size);

#endif /* DISHOUT_INC_UTIL_H_ */
