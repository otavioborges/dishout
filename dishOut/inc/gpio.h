/*
 * gpio.h
 *
 *  Created on: Nov. 15, 2020
 *      Author: otavio
 */

#ifndef INC_GPIO_H_
#define INC_GPIO_H_

#include "stm32f746xx.h"
#include <stdint.h>

typedef enum{
	GPIO_TYPE_PUSH_PULL		= 0,
	GPIO_TYPE_OPEN_DRAIN	= 0x01
}gpio_type_t;

typedef enum{
	GPIO_SPEED_LOW			= 0,
	GPIO_SPEED_MEDIUM		= 0x01,
	GPIO_SPEED_HIGH			= 0x02,
	GPIO_SPEED_VERY_HIGH	= 0x03
}gpio_speed_t;

typedef enum{
	GPIO_PULL_NO_PULL		= 0,
	GPIO_PULL_PULL_UP		= 0x01,
	GPIO_PULL_PULL_DOWN		= 0x02
}gpio_pull_t;

void GPIO_Clear(GPIO_TypeDef *port, uint8_t pin);
void GPIO_SetOutput(GPIO_TypeDef *port, uint8_t pin, gpio_type_t type, gpio_speed_t speed, gpio_pull_t pull);
void GPIO_SetInput(GPIO_TypeDef *port, uint8_t pin, gpio_speed_t speed, gpio_pull_t pull);
void GPIO_SetAF(GPIO_TypeDef *port, uint8_t pin, uint8_t alternate, gpio_type_t type, gpio_speed_t speed, gpio_pull_t pull);
void GPIO_SetAnalog(GPIO_TypeDef *port, uint8_t pin, gpio_speed_t speed);

void GPIO_Reset(GPIO_TypeDef *port, uint8_t pin);
void GPIO_Set(GPIO_TypeDef *port, uint8_t pin);

uint8_t GPIO_Read(GPIO_TypeDef *port, uint8_t pin);

#endif /* INC_GPIO_H_ */
