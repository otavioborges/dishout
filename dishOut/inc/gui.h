/*
 * gui.h
 *
 *  Created on: Jan. 29, 2021
 *      Author: otavio
 */

#ifndef DISHOUT_INC_GUI_H_
#define DISHOUT_INC_GUI_H_

#define GUI_THREAD_STACK_SIZE       512U

#include "util.h"

result_t gui_init(void);

#endif /* DISHOUT_INC_GUI_H_ */
